package com.usalamatechnology.application.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.usalamatechnology.application.R;
import com.usalamatechnology.application.adapter.DirectionsJSONParser;
import com.usalamatechnology.application.adapter.PlaceJSONParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class MapsActivity extends AppCompatActivity implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {

    private String senderNum = "";
    private Double vicLongitude;
    private Double vicLatitude;
    private float vicAccuracy;

    private Double policeOneLong;
    private Double policeOneLat;
    private Double policeTwoLong;
    private Double policeTwoLat;
    private Double policeThreeLong;
    private Double policeThreeLat;

    private static final String TAG = "LocationActivity";
    private static final long INTERVAL = 1000 * 60 * 1; //1 minute
    private static final long FASTEST_INTERVAL = 1000 * 60 * 1; // 1 minute


    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;

    LocationRequest mLocationRequest;
    Location mCurrentLocation;
    private PlacesTask placesTask;

    String mLastUpdateTime;

    GoogleApiClient mGoogleApiClient;
    GoogleMap googleMap;
    MapView mMapView;

    public static final String LOCATIONPREFERENCES = "LocationPrefs";
    public static SharedPreferences locationSharedPreferences;
    SharedPreferences.Editor locationEditor;

    public static final String lastKnownLatitude = "lastLatitude";
    public static final String lastKnownLongitude = "lastLongitude";
    public static final String lastKnownAccuracy = "lastAccuracy";
    private Marker myMarker;

    ///////////////////////////////////////////////////////////////////////////
    // Internet connection variables

    ConnectionDetector cd;
    // flag for Internet connection status
    private InternetConnection internetConnection;
    Boolean isInternetPresent = false;
    Boolean isInternetActive = false;
    private CameraPosition cameraPosition;
    private Context context;
    private String textContent;
    private String address = null;
    private float distance;
    private String policeOneName;
    private String policeOneVicinity;
    private String policeTwoName;
    private String policeTwoVicinity;
    private String policeThreeName;
    private String policeThreeVicinity;
    // Connection detector class

    //emergency contacts
    public static final String phoneKey = "phoneKey";
    public static final String phoneOneKey = "phoneKey1";
    public static final String phoneTwoKey = "phoneKey2";

    //emergency contacts
    public static String phoneNumber = "";
    public static String phoneNumberOne = "";
    public static String phoneNumberTwo = "";

    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;

    //////////////////////////////////////////////////////////////////////////


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        cd = new ConnectionDetector(this.getApplicationContext());
        locationSharedPreferences = getSharedPreferences(LOCATIONPREFERENCES, Context.MODE_PRIVATE);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        locationEditor = locationSharedPreferences.edit();

        //When user presses distress call, first thing is to check for internet
        internetConnection = new InternetConnection();
        internetConnection.execute("");
        //show error dialog if GoolglePlayServices not available


       setContentView(R.layout.activity_maps2);

        phoneNumber = sharedpreferences.getString(phoneKey, null);
        phoneNumberOne = sharedpreferences.getString(phoneOneKey, null);
        phoneNumberTwo = sharedpreferences.getString(phoneTwoKey, null);

        String substring1="",substring2="",substring3="";

        if(phoneNumber!=null)
        {
            substring1 = phoneNumber.length() > 6 ? phoneNumber.substring(phoneNumber.length() - 6) : phoneNumber;
        }

        if(phoneNumberOne!=null) {
             substring2 = phoneNumberOne.length() > 6 ? phoneNumberOne.substring(phoneNumberOne.length() - 6) : phoneNumberOne;
        }

        if(phoneNumberTwo!=null)
        {
             substring3 = phoneNumberTwo.length() > 6 ? phoneNumberTwo.substring(phoneNumberTwo.length() - 6) : phoneNumberTwo;
        }



        TextView msg = (TextView) findViewById(R.id.textViewMessage);
        msg.setText(String.format("%s: %s", getIntent().getStringExtra("Name"), stripper(getIntent().getStringExtra("Message"))));

        //addMarker(14.0f);


        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        if (mToolbar != null) {
            mToolbar.setTitle("");

            setSupportActionBar(mToolbar);
        }
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SmsManager smsManager = SmsManager.getDefault();
        System.out.println("Seeeeeeeeeender num " + senderNum);
        String senderNumString = senderNum.length() > 6 ? senderNum.substring(senderNum.length() - 6) : senderNum;
        if (!senderNum.equals("") && (senderNumString.equals(substring1) || senderNumString.equals(substring2) || senderNumString.equals(substring3))) {
//I don't know why the hell i did this////////////////////////////////////////////

            if ("true".equals(getIntent().getStringExtra("fromRec"))) {
                if (!"dontsend".equals(getIntent().getStringExtra("dontsend"))) {
                    if (getIntent().getStringExtra("send") == null) {

                        System.out.println("Waaaaaaaaaaaas sennnnnnnnnnnnnd");
                        smsManager.sendTextMessage(
                                senderNum, null,
                                "Usalama Text: I have seen your distress call.", null,
                                null);
                    }
                    System.out.println("Waaaaaaaaaaaasnt1 sennnnnnnnnnnnnd");
                }
                System.out.println("Waaaaaaaaaaaasnt2 sennnnnnnnnnnnnd");
            }

/////////////////////////////////////////////////////////////////////////
        }

        findViewById(R.id.directions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("cccccccccccccccccccccccccccccclicked");
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                System.out.println("Animate");
                if (googleMap != null) {
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    addMarker(14.0f);
                }
            }
        });

        findViewById(R.id.more).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent resultIntent = new Intent(context, MoreActivity.class);
                if (mCurrentLocation != null) {
                    resultIntent.putExtra("myLongitude", String.valueOf(mCurrentLocation.getLongitude()));
                    resultIntent.putExtra("myLatitude", String.valueOf(mCurrentLocation.getLatitude()));
                }
                resultIntent.putExtra("name", getIntent().getStringExtra("Name"));
                resultIntent.putExtra("distancetovic", String.valueOf(distance));
                resultIntent.putExtra("vicLongitude", String.valueOf(vicLongitude));
                resultIntent.putExtra("vicLatitude", String.valueOf(vicLatitude));
                resultIntent.putExtra("policeOneLong", String.valueOf(policeOneLong));
                resultIntent.putExtra("policeOneLat", String.valueOf(policeOneLat));
                resultIntent.putExtra("policeTwoLong", String.valueOf(policeTwoLong));
                resultIntent.putExtra("policeTwoLat", String.valueOf(policeTwoLat));
                resultIntent.putExtra("policeThreeLong", String.valueOf(policeThreeLong));
                resultIntent.putExtra("policeThreeLat", String.valueOf(policeThreeLat));
                startActivity(resultIntent);

            }
        });

    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        address = null;
        Log.d(TAG, "onCreate ...............................");
        context = this.getApplicationContext();

        if (!isGooglePlayServicesAvailable()) {
            finish();
        }
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                this.googleMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            this.googleMap.setMyLocationEnabled(true);
        }

        System.out.println("++++++++++++++++++++++++++++++" + getIntent().getStringExtra("policeOneLong"));
        getAccuracy(getIntent().getStringExtra("Number"));
        placesTask = new PlacesTask();
        System.out.println("fromRec " + getIntent().getStringExtra("fromRec"));

        System.out.println("vicLongitude" + getIntent().getStringExtra("long"));
        System.out.println("vicLatitude" + getIntent().getStringExtra("lat"));

        if (!getIntent().getStringExtra("long").equals("") && !getIntent().getStringExtra("lat").equals("")) {
            String lon = getIntent().getStringExtra("long");
            String lat = getIntent().getStringExtra("lat");
            if (lon.length() > 1 && lat.length() > 1) {
                vicLongitude = Double.parseDouble(getIntent().getStringExtra("long"));
                vicLatitude = Double.parseDouble(getIntent().getStringExtra("lat"));

                System.out.println("vicLongitude" + getIntent().getStringExtra("long"));
                System.out.println("vicLatitude" + getIntent().getStringExtra("lat"));
            }
        }

        if (!getIntent().getStringExtra("policeOneLong").equals("") && !getIntent().getStringExtra("policeOneLat").equals("")) {
            String lon = getIntent().getStringExtra("policeOneLong");
            String lat = getIntent().getStringExtra("policeOneLat");
            if (lon.length() > 1 && lat.length() > 1) {
                policeOneLong = Double.parseDouble(getIntent().getStringExtra("policeOneLong"));
                policeOneLat = Double.parseDouble(getIntent().getStringExtra("policeOneLat"));
            } else {
                if (mCurrentLocation != null) {
                   /* If there is internet connection and googleLocation client is NOT null
                   Execute the code nearest police station called PlacesTask
                     */
                    placesTask.execute("https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                            "location=" + mCurrentLocation.getLatitude() + "," + mCurrentLocation.getLongitude() + "&radius=5000" + "&types=" + "police" +
                            "&sensor=true" + "&key=AIzaSyBBq3V_OY_A2RRG1zRe3XUx6iU6i7bHQ_Y");
                } else {
               /* If there is internet connection and googleLocation client is null
                   --Utilize values stored in shared preferences
                   --Execute the code nearest police station called PlacesTask
                     */
                    if (locationSharedPreferences.getString(lastKnownLatitude, null) != null && !"".equals(locationSharedPreferences.getString(lastKnownLatitude, null)) && locationSharedPreferences.getString(lastKnownLongitude, null) != null && !"".equals(locationSharedPreferences.getString(lastKnownLatitude, null)) && locationSharedPreferences.getString(lastKnownAccuracy, null) != null && !"".equals(locationSharedPreferences.getString(lastKnownAccuracy, null))) {
                        placesTask.execute("https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                                "location=" + locationSharedPreferences.getString(lastKnownLatitude, null) + "," + locationSharedPreferences.getString(lastKnownLongitude, null) + "&radius=5000" + "&types=" + "police" +
                                "&sensor=true" + "&key=AIzaSyBBq3V_OY_A2RRG1zRe3XUx6iU6i7bHQ_Y");
                    }

                }
            }
        } else {
            placesTask.execute("https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                    "location=-1.28336,36.8163&radius=5000" + "&types=" + "police" +
                    "&sensor=true" + "&key=AIzaSyBBq3V_OY_A2RRG1zRe3XUx6iU6i7bHQ_Y");
        }

        if (!getIntent().getStringExtra("policeTwoLong").equals("") && !getIntent().getStringExtra("policeTwoLat").equals("")) {
            String lon = getIntent().getStringExtra("policeTwoLong");
            String lat = getIntent().getStringExtra("policeTwoLat");
            if (lon.length() > 1 && lat.length() > 1) {
                policeTwoLong = Double.parseDouble(getIntent().getStringExtra("policeTwoLong"));
                policeTwoLat = Double.parseDouble(getIntent().getStringExtra("policeTwoLat"));
            }
        }

        if (!getIntent().getStringExtra("policeThreeLong").equals("") && !getIntent().getStringExtra("policeThreeLat").equals("")) {
            String lon = getIntent().getStringExtra("policeThreeLong");
            String lat = getIntent().getStringExtra("policeThreeLat");
            if (lon.length() > 1 && lat.length() > 1) {
                policeThreeLong = Double.parseDouble(getIntent().getStringExtra("policeThreeLong"));
                policeThreeLat = Double.parseDouble(getIntent().getStringExtra("policeThreeLat"));
            }

        }
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * A class, to download Google Places
     */
    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... url) {
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
                System.out.println("Background Task ********************************* " + e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            PlacesParserTask parserTask = new PlacesParserTask();
            parserTask.execute(result);
        }

    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class PlacesParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;


        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                /** Getting the parsed data as a List construct */
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> list) {
            if (list != null) {
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {

                        MarkerOptions markerOptions = new MarkerOptions();

                        HashMap<String, String> hmPlace = list.get(i);

                        double lat = Double.parseDouble(hmPlace.get("lat"));

                        double lng = Double.parseDouble(hmPlace.get("lng"));

                        String name = hmPlace.get("place_name");

                        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& name " + name);

                        String vicinity = hmPlace.get("vicinity");

                        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& vicinity " + vicinity);

                        LatLng latLng = new LatLng(lat, lng);

                        markerOptions.position(latLng);

                        markerOptions.title(name + " : " + vicinity);
                        if (i == 0) {
                            policeOneLong = lng;
                            policeOneLat = lat;

                            policeOneName = name;
                            policeOneVicinity = vicinity;
                        } else if (i == 1) {
                            policeTwoLong = lng;
                            policeTwoLat = lat;

                            policeTwoName = name;
                            policeTwoVicinity = vicinity;
                        } else if (i == 2) {
                            policeThreeLong = lng;
                            policeThreeLat = lat;

                            policeThreeName = name;
                            policeThreeVicinity = vicinity;
                        }

                    }
                    addMarker(16.0f);
                }

            }

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent resultIntent = new Intent(this, MainActivity.class);
        startActivity(resultIntent);
    }

    private void getAccuracy(String value) {
        String[] newArray = value.split("'");
        vicAccuracy = Float.parseFloat(newArray[1]);
        senderNum = newArray[0];
    }

    private String getDirectionsUrl() {
        if (mCurrentLocation != null) {
            System.out.println("Am real directions");
            String str_origin = "origin=" + mCurrentLocation.getLatitude() + "," + mCurrentLocation.getLongitude();
            String str_dest = "destination=" + vicLatitude + "," + vicLongitude;
            String sensor = "sensor=false";


            String parameters = str_origin + "&" + str_dest + "&" + sensor;


            String output = "json";


            return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        } else {
            System.out.println("Am directions");
            String str_origin = "origin=" + locationSharedPreferences.getString(lastKnownLatitude, null) + "," + locationSharedPreferences.getString(lastKnownLongitude, null);
            String str_dest = "destination=" + vicLatitude + "," + vicLongitude;
            String sensor = "sensor=false";


            String parameters = str_origin + "&" + str_dest + "&" + sensor;


            String output = "json";


            return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        }


    }

    private class DownloadTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... url) {


            String data = "";

            try {

                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();


            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();


                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            if (result != null) {
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<>();
                    lineOptions = new PolylineOptions();


                    List<HashMap<String, String>> path = result.get(i);

                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    lineOptions.addAll(points);
                    lineOptions.width(4);
                    lineOptions.color(context.getResources().getColor(R.color.colorPrimaryDark));
                }

                if (googleMap != null) {
                    googleMap.addPolyline(lineOptions);
                }
            }
        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);


            urlConnection = (HttpURLConnection) url.openConnection();


            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception w", e.toString());
        } finally {
            assert iStream != null;
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    public Boolean isOnline() {

        try {
            Process p1 = Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    private class InternetConnection extends AsyncTask<String, Integer, Boolean> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Boolean doInBackground(String... url) {
            //calls helper method to determine if their is an internet connection
            boolean status = checkForActiveConnection();
            return status;

        }


        @Override
        protected void onPostExecute(Boolean result) {

        }
    }

    private boolean checkForActiveConnection() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            if (isOnline()) {
                isInternetActive = true;
                // Toast.makeText(getActivity(), "You are connected to the internet", Toast.LENGTH_LONG).show();
                return true;
            } else {
                isInternetActive = false;
                return false;
            }
        } else {
            //Toast.makeText(getActivity(), "You are NOT connected to the internet", Toast.LENGTH_LONG).show();
            isInternetActive = false;
            return false;
        }
    }

    private String stripper(String text) {
        String newValue = text.replace(" Location:http://maps.google.com/?q=", "*");

        char[] valueChar;
        valueChar = newValue.toCharArray();
        for (int i = 0; i < newValue.length(); i++) {
            if (valueChar[i] == '*') {
                newValue = newValue.substring(13, i);
                break;
            }
        }
        return newValue;

    }

    /**
     * Initialises the mapview
     */
    private void createMapView() {

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if(mapFragment==null)
        {
            System.out.println("map is null");
        }
        else
        {
            mapFragment.getMapAsync(this);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart fired ..............");
       // mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop fired ..............");
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
            Log.d(TAG, "isConnected ...............: " + mGoogleApiClient.isConnected());
        }

    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }


    @Override
    public void onConnected(Bundle bundle){
        Log.d(TAG, "onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if(mGoogleApiClient.isConnected())
            {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }

        }
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        if (mCurrentLocation == null) {

            if (isInternetActive) {
                if (vicLatitude != null && vicLongitude != null) {
                    String url = getDirectionsUrl();
                    if (!"".equals(url)) {
                        System.out.println("url " + url);
                        DownloadTask downloadTask = new DownloadTask();

                        downloadTask.execute(url);
                    }
                } else {
                    Toast.makeText(this, "Unfortunately, the victim did not send their location", Toast.LENGTH_LONG).show();
                }
            }

        } else {

            handleNewLocation(mCurrentLocation);

            if (isInternetActive) {
                if (vicLatitude != null && vicLongitude != null) {
                    String url = getDirectionsUrl();
                    if (!"".equals(url)) {
                        System.out.println("url " + url);
                        DownloadTask downloadTask = new DownloadTask();

                        downloadTask.execute(url);
                    }
                }
            }


        }
    }

    private void handleNewLocation(Location location) {
        if (location != null) {
            Log.d(TAG, "Firing onLocationChanged..............................................");
            mCurrentLocation = location;
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            addMarker(14.0f);
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, R.string.alert_for_connection_suspended, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        googleMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            // Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
            Toast.makeText(this, R.string.alert_for_connectionfailed, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
       // mCurrLocationMarker = googleMap.addMarker(markerOptions);
        //move map camera
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(14));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

        if (mCurrentLocation == null || location.getAccuracy() < mCurrentLocation.getAccuracy()) {
            System.out.println("We have a more accurate location ---------->");
            handleNewLocation(location);
        }
    }

    class GetAddress extends AsyncTask<String, Date, String> {


        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            getMyLocationAddress(vicLatitude,vicLongitude);
            return "";
        }

        @Override
        protected void onPostExecute(String msg) {
            //pDialog.setMessage("Calling Upload");
        }

    }

    public void getMyLocationAddress(Double latitude, Double longitude) {

        Geocoder geocoder= new Geocoder(this, Locale.ENGLISH);

        try {

            //Place your latitude and longitude
            List<Address> addresses = geocoder.getFromLocation(latitude,longitude, 1);

            if(addresses != null) {

                Address fetchedAddress = addresses.get(0);
                StringBuilder strAddress = new StringBuilder();

                for(int i=0; i<fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(fetchedAddress.getAddressLine(i)).append("\n");
                }

                address= strAddress.toString();

            }

            else
            {
                address="No location found!";
                //System.out.println("addressaddress"+address);
            }


        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //Toast.makeText(getApplicationContext(),"Could not get address..!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Adds a marker to the map
     */

    //overridden code
//Handles the response from googleAPIClient
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_RECOVER_PLAY_SERVICES) {
            if (resultCode == Activity.RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this.getApplicationContext(), R.string.text_to_install_googleplay_services,
                        Toast.LENGTH_SHORT).show();
                //Activity.finish();
            }
        }
    }

    private void addMarker(Float zoom) {


        if (mCurrentLocation != null) {
            if(googleMap!=null) {
                googleMap.clear();
                System.out.println("Was executed");
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), zoom));
            }
            /** Make sure that the map has been initialised **/
            LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            cameraPosition = new CameraPosition.Builder()
                    .target(currentLatLng)      // Sets the center of the map to Mountain View
                    .zoom(zoom)
                    .build();                   //
            if (null != googleMap){
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                System.out.println("user "+mCurrentLocation.getLongitude());
                 /*myMarker = googleMap.addMarker(new MarkerOptions()
                                .position(currentLatLng)
                                .title("Me")
                                .draggable(true)
                                 .snippet("My position")
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.markerfinal))
                );
                myMarker.showInfoWindow();
                drawCircle(mCurrentLocation.getLongitude(), mCurrentLocation.getLongitude(), mCurrentLocation.getAccuracy());*/
                System.out.println("passed here " + vicLongitude);


                    if (vicLatitude != null && vicLongitude != null) {


                        new GetAddress().execute();


                    Location locationA = new Location("point A");
                    locationA.setLatitude(mCurrentLocation.getLatitude());
                    locationA.setLongitude(mCurrentLocation.getLongitude());
                    Location locationB = new Location("point B");
                    locationB.setLatitude(vicLatitude);
                    locationB.setLongitude(vicLongitude);
                     distance = locationA.distanceTo(locationB);
                    DecimalFormat df = new DecimalFormat("#.#");
                    df.setRoundingMode(RoundingMode.CEILING);
                    if(address!=null)
                    {
                        System.out.println("addressaddress NOT NULL"+address);
                        textContent="Am at "+address+"("+df.format(distance)+" m). I need your help.";
                    }
                    else
                    {
                        System.out.println("addressaddress NULL"+address);
                        textContent="Am "+df.format(distance)+" m away.";
                    }

                    Marker vicMarker = googleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(vicLatitude, vicLongitude))
                                    .title("Victim")
                                    .draggable(true)
                                    .snippet(textContent)
                                    .icon(BitmapDescriptorFactory
                                            .fromResource(R.drawable.markerfinal))
                    );


                    vicMarker.showInfoWindow();
                    drawCircle(vicLongitude, vicLatitude, vicAccuracy);
                    System.out.println("passed through");
                }


                if (policeOneLat != null && policeOneLong != null) {
                    if(policeOneName != null || policeOneVicinity !=null)
                    {
                        googleMap.addMarker(new MarkerOptions()
                                .title(policeOneName+":"+policeOneVicinity)
                                .position(
                                        new LatLng(policeOneLat, policeOneLong))
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.pin)));
                        //policeOneMarker.showInfoWindow();
                    }
                    else
                    {
                        Marker policeOneMarker = googleMap.addMarker(new MarkerOptions()
                                .title("Police Stn:1")
                                .position(
                                        new LatLng(policeOneLat, policeOneLong))
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.pin)));
                        //policeOneMarker.showInfoWindow();
                    }

                }

                System.out.println("policeOneLat " + policeOneLat);
                if (policeTwoLat != null && policeTwoLong != null) {
                    if(policeTwoName != null || policeTwoVicinity !=null) {
                        googleMap.addMarker(new MarkerOptions()
                                .title(policeTwoName + ":" + policeTwoVicinity)
                                .position(
                                        new LatLng(policeTwoLat, policeTwoLong))
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.pin)));
                        //  policeTwoMarker.showInfoWindow();
                    }else
                    {
                        googleMap.addMarker(new MarkerOptions()
                                .title("Police Stn:2")
                                .position(
                                        new LatLng(policeTwoLat, policeTwoLong))
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.pin)));
                        //  policeTwoMarker.showInfoWindow();
                    }
                }
                System.out.println("policeTwoLat "+policeTwoLat);

                if (policeThreeLat != null && policeThreeLong != null) {
                    if(policeThreeName != null || policeThreeVicinity !=null) {
                        googleMap.addMarker(new MarkerOptions()
                                .title(policeThreeName + ":" + policeThreeVicinity)
                                .position(
                                        new LatLng(policeThreeLat, policeThreeLong))
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.pin)));
                        //  policeThreeMarker.showInfoWindow();
                    }else
                    {
                        googleMap.addMarker(new MarkerOptions()
                                .title("Police Stn:3")
                                .position(
                                        new LatLng(policeThreeLat, policeThreeLong))
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.pin)));
                        //  policeThreeMarker.showInfoWindow();
                    }
                }

            }
        } else if (locationSharedPreferences.getString(lastKnownLatitude, null) != null && locationSharedPreferences.getString(lastKnownLongitude, null) != null && locationSharedPreferences.getString(lastKnownAccuracy, null) != null) {
            Toast.makeText(this, "Usalama has used your last known location.Turn on GPS and WIFI for better accuracy", Toast.LENGTH_LONG).show();
            if(googleMap!=null) {
                googleMap.clear();

                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        Double.parseDouble(locationSharedPreferences.getString(lastKnownLatitude, null)), Double.parseDouble(locationSharedPreferences.getString(lastKnownLongitude, null))), 16.0f));
            }

            /** Make sure that the map has been initialised **/
            LatLng currentLatLng = new LatLng(Double.parseDouble(locationSharedPreferences.getString(lastKnownLatitude, null)), Double.parseDouble(locationSharedPreferences.getString(lastKnownLongitude, null)));

            cameraPosition = new CameraPosition.Builder()
                    .target(currentLatLng)      // Sets the center of the map to Mountain View
                    .zoom(zoom)                   // Sets the zoom// Sets the tilt of the camera to 30 degrees
                    .build();                   //
            if (null != googleMap) {

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                myMarker = googleMap.addMarker(new MarkerOptions()
                                .position(currentLatLng)
                                .title("Me")
                                .draggable(true)
                                .snippet("My position")
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.markerfinal))

                );
                myMarker.showInfoWindow();

                drawCircle(Double.parseDouble(locationSharedPreferences.getString(lastKnownLongitude, null)), Double.parseDouble(locationSharedPreferences.getString(lastKnownLatitude, null)), Float.parseFloat(locationSharedPreferences.getString(lastKnownAccuracy, null)));

                System.out.println("passed here " + vicLongitude);
                if (vicLatitude != null && vicLongitude != null)
                {

                        new GetAddress().execute();

                    Location locationA = new Location("point A");
                   locationA.setLatitude(Double.parseDouble(locationSharedPreferences.getString(lastKnownLatitude, null)));
                   locationA.setLongitude(Double.parseDouble(locationSharedPreferences.getString(lastKnownLongitude, null)));

                    Location locationB = new Location("point B");
                    locationB.setLatitude(vicLatitude);
                    locationB.setLongitude(vicLongitude);
                    float distance = locationA.distanceTo(locationB);
                    DecimalFormat df = new DecimalFormat("#.#");
                    df.setRoundingMode(RoundingMode.CEILING);

                    if(address!=null)
                    {
                        System.out.println("addressaddress NOT NULL"+address);
                        textContent="Am at "+address+"("+df.format(distance)+" m). I need your help.";
                    }
                    else
                    {
                        System.out.println("addressaddress  NULL"+address);
                        textContent="Am "+df.format(distance)+" m away.";
                    }


                    Marker vicMarker = googleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(vicLatitude, vicLongitude))
                                    .title("Victim")
                                    .draggable(true)
                                    .snippet(textContent)
                                    .icon(BitmapDescriptorFactory
                                            .fromResource(R.drawable.markerfinal))
                    );
                    vicMarker.showInfoWindow();
                    drawCircle(vicLongitude, vicLatitude, vicAccuracy);
                }

                System.out.println("passed through");

                if (policeOneLat != null && policeOneLong != null) {
                    Marker policeOneMarker = googleMap.addMarker(new MarkerOptions()
                            .title(policeOneName+":"+policeOneVicinity)
                            .position(
                                    new LatLng(policeOneLat, policeOneLong))
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.pin)));
                    //policeOneMarker.showInfoWindow();
                }

                System.out.println("policeOneLat " + policeOneLat);
                if (policeTwoLat != null && policeTwoLong != null) {
                    Marker policeTwoMarker =googleMap.addMarker(new MarkerOptions()
                            .title(policeTwoName+":"+policeTwoVicinity)
                            .position(
                                    new LatLng(policeTwoLat, policeTwoLong))
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.pin)));
                    //  policeTwoMarker.showInfoWindow();
                }
                System.out.println("policeTwoLat "+policeTwoLat);

                if (policeThreeLat != null && policeThreeLong != null) {
                    Marker policeThreeMarker =googleMap.addMarker(new MarkerOptions()
                            .title(policeThreeName+":"+policeThreeVicinity)
                            .position(
                                    new LatLng(policeThreeLat, policeThreeLong))
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.pin)));
                    //  policeThreeMarker.showInfoWindow();
                }
            }
        } else if (vicLatitude!=null && vicLongitude !=null){
            Toast.makeText(this, "Wait while we try get your location.Turn on GPS and WIFI for better accuracy", Toast.LENGTH_LONG).show();

            if(googleMap!=null) {
                googleMap.clear();
                System.out.println("Was executed");
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        vicLatitude, vicLongitude), zoom));
            }
            /** Make sure that the map has been initialised **/
            LatLng currentLatLng = new LatLng(vicLatitude, vicLongitude);
            cameraPosition = new CameraPosition.Builder()
                    .target(currentLatLng)      // Sets the center of the map to Mountain View
                    .zoom(zoom)
                    .build();                   //
            if (null != googleMap) {

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                if (vicLatitude != null && vicLongitude != null) {


                        new GetAddress().execute();

                    System.out.println("addressaddress" + address);
                    DecimalFormat df = new DecimalFormat("#.#");
                    df.setRoundingMode(RoundingMode.CEILING);
                    if(address!=null)
                    {
                        System.out.println("addressaddress NOT NULL"+address);
                        textContent="Am at "+address+"("+df.format(distance)+" m). I need your help.";
                    }
                    else
                    {
                        System.out.println("addressaddress NULL"+address);
                        textContent="Am "+df.format(distance)+" m away.";
                    }
                    Marker vicMarker = googleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(vicLatitude, vicLongitude))
                                    .title("Victim")
                                    .draggable(true)
                                    .snippet(textContent)
                                    .icon(BitmapDescriptorFactory
                                            .fromResource(R.drawable.markerfinal))
                    );

                    vicMarker.showInfoWindow();
                    drawCircle(vicLongitude, vicLatitude, vicAccuracy);
                    System.out.println("passed through");
                }

                if (policeOneLat != null && policeOneLong != null) {
                    Marker policeOneMarker = googleMap.addMarker(new MarkerOptions()
                            .title(policeOneName+":"+policeOneVicinity)
                            .position(
                                    new LatLng(policeOneLat, policeOneLong))
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.pin)));
                    //policeOneMarker.showInfoWindow();
                }

                System.out.println("policeOneLat " + policeOneLat);
                if (policeTwoLat != null && policeTwoLong != null) {
                    Marker policeTwoMarker =googleMap.addMarker(new MarkerOptions()
                            .title(policeTwoName+":"+policeTwoVicinity)
                            .position(
                                    new LatLng(policeTwoLat, policeTwoLong))
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.pin)));
                    //  policeTwoMarker.showInfoWindow();
                }
                System.out.println("policeTwoLat "+policeTwoLat);

                if (policeThreeLat != null && policeThreeLong != null) {
                    Marker policeThreeMarker =googleMap.addMarker(new MarkerOptions()
                            .title(policeThreeName+":"+policeThreeVicinity)
                            .position(
                                    new LatLng(policeThreeLat, policeThreeLong))
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.pin)));
                    //  policeThreeMarker.showInfoWindow();
                }

            }
        }

    }

    private void drawCircle(double lon, double lat, float accuracy) {
        Log.d("Circle", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcuracy " + accuracy);
        LatLng latLng = new LatLng(lat, lon);

        CircleOptions circleOptions = new CircleOptions()
                .center(latLng)   //set center
                .radius(accuracy)   //set radius in meters
                .fillColor(0x110000FF)  //default
                .strokeColor(0x110000FF)
                .strokeWidth(1);
        if(googleMap!=null) {
            googleMap.addCircle(circleOptions);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            stopLocationUpdates();
        }
        SmsManager smsManager = SmsManager.getDefault();
        if (!senderNum.equals("")) {
//I don't know why the hell i did this////////////////////////////////////////////

            if("true".equals(getIntent().getStringExtra("fromRec")))
            {
                if (getIntent().getStringExtra("send") == null) {
                    smsManager.sendTextMessage(
                            senderNum, null,
                            "I have seen your distress call.", null,
                            null);
                }
            }

/////////////////////////////////////////////////////////////////////////
        }
    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
        Log.d(TAG, "Location update stopped .......................");
    }

    @Override
    public void onResume() {
        super.onResume();
createMapView();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
            if (mGoogleApiClient.isConnected()) {
                startLocationUpdates();
                Log.d(TAG, "Location update resumed .....................");
            }
        }


        if (!getIntent().getStringExtra("long").equals("") && !getIntent().getStringExtra("lat").equals("")) {
            String lon = getIntent().getStringExtra("long");
            String lat = getIntent().getStringExtra("lat");
            if (lon.length() > 1 && lat.length() > 1) {
                vicLongitude = Double.parseDouble(getIntent().getStringExtra("long"));
                vicLatitude = Double.parseDouble(getIntent().getStringExtra("lat"));

                System.out.println("vicLongitude" + getIntent().getStringExtra("long"));
                System.out.println("vicLatitude" + getIntent().getStringExtra("lat"));
            }
        }

        if (!getIntent().getStringExtra("policeOneLong").equals("") && !getIntent().getStringExtra("policeOneLat").equals("")) {
            String lon = getIntent().getStringExtra("policeOneLong");
            String lat = getIntent().getStringExtra("policeOneLat");
            if (lon.length() > 1 && lat.length() > 1) {
                policeOneLong = Double.parseDouble(getIntent().getStringExtra("policeOneLong"));
                policeOneLat = Double.parseDouble(getIntent().getStringExtra("policeOneLat"));
            }
        }

        if (!getIntent().getStringExtra("policeTwoLong").equals("") && !getIntent().getStringExtra("policeTwoLat").equals("")) {
            String lon = getIntent().getStringExtra("policeTwoLong");
            String lat = getIntent().getStringExtra("policeTwoLat");
            if (lon.length() > 1 && lat.length() > 1) {
                policeTwoLong = Double.parseDouble(getIntent().getStringExtra("policeTwoLong"));
                policeTwoLat = Double.parseDouble(getIntent().getStringExtra("policeTwoLat"));
            }
        }

        if (!getIntent().getStringExtra("policeThreeLong").equals("") && !getIntent().getStringExtra("policeThreeLat").equals("")) {
            String lon = getIntent().getStringExtra("policeThreeLong");
            String lat = getIntent().getStringExtra("policeThreeLat");
            if (lon.length() > 1 && lat.length() > 1) {
                policeThreeLong = Double.parseDouble(getIntent().getStringExtra("policeThreeLong"));
                policeThreeLat = Double.parseDouble(getIntent().getStringExtra("policeThreeLat"));
            }

        }

    }
}
