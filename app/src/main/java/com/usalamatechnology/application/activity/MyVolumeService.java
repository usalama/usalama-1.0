package com.usalamatechnology.application.activity;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class MyVolumeService extends Service {


    public static final String USERLOCATIONPREFERENCES = "userlocationPrefs";
    public static SharedPreferences userLocationSharedPreferences;
    SharedPreferences.Editor userLocationEditor;

    //emergency contacts
    public static final String phoneKey = "phoneKey";
    public static final String phoneOneKey = "phoneKey1";
    public static final String phoneTwoKey = "phoneKey2";

    //messages
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;

    public static final String userLatitude = "userLatitude";
    public static final String userLongitude = "userLongitude";
    public static final String userAccuracy = "userAccuracy";

    public static final String LOCATIONPREFERENCES = "LocationPrefs";
    public static SharedPreferences locationSharedPreferences;
    SharedPreferences.Editor locationEditor;


    private ArrayList<String> allPhoneNumbers = new ArrayList<>();
    String message;
    boolean sent = false;
    boolean stop=false;
    private int mMessageSentCount;
    private String txtBody;
    private int mMessageSentTotalParts;
    private int mMessageSentParts;
    private int count;
    private BroadcastReceiver receiver;
    private BroadcastReceiver receiverTwo;

    String phoneNumber;
    String phoneNumberOne ;
    String phoneNumberTwo;


    private void setAllPhoneNumbers() {
        allPhoneNumbers.clear();
        if (phoneNumber != null && !"".equals(phoneNumber)) {
            allPhoneNumbers.add(phoneNumber);
        }

        if (phoneNumberOne != null && !"".equals(phoneNumberOne)) {
            allPhoneNumbers.add(phoneNumberOne);
        }

        if (phoneNumberTwo != null && !"".equals(phoneNumberTwo)) {
            allPhoneNumbers.add(phoneNumberTwo);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {


        System.out.println("BBOOOOOOOOOOOOOOOOOOUNd");
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void startSendMessages() {
        registerBroadCastReceivers();
        message="I need your help";
        if (locationSharedPreferences.getString(userLatitude, null) != null) {

            txtBody = "Usalama text: " + message + " Location:http://maps.google.com/?q=" + locationSharedPreferences.getString(userLatitude, null) + "," + locationSharedPreferences.getString(userLongitude, null) + "&z=14 .Accuracy level is " + locationSharedPreferences.getString(userAccuracy, null)+" meters ";

        } else {
            txtBody = "Usalama text: " + message;

        }
        mMessageSentCount = 0;


        if (message != null) {
            sendSMS(allPhoneNumbers.get(mMessageSentCount), txtBody);
        } else
        {
            if(getApplication()!=null) {
                Toast.makeText(getApplication(), "Distress not sent. Message field is not available", Toast.LENGTH_LONG).show();
            }
        }
    }

    public static boolean checkSMSPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedpreferences = getApplication().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        userLocationSharedPreferences = getApplication().getSharedPreferences(USERLOCATIONPREFERENCES, Context.MODE_PRIVATE);
        userLocationEditor = userLocationSharedPreferences.edit();

        locationSharedPreferences = getApplication().getSharedPreferences(LOCATIONPREFERENCES, Context.MODE_PRIVATE);
        locationEditor = locationSharedPreferences.edit();

        System.out.println("MyVolumeService has started ..............**********==============>>>>>>>>>>>");

        phoneNumber = sharedpreferences.getString(phoneKey, null);
        phoneNumberOne = sharedpreferences.getString(phoneOneKey, null);
        phoneNumberTwo = sharedpreferences.getString(phoneTwoKey, null);
        System.out.println("phoneNumberMy Timer Service " + phoneNumber);
        setAllPhoneNumbers();
        if(phoneNumber!=null)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(checkSMSPermission(getApplicationContext()))
                {
                    startSendMessages();
                }
            }
            else
            {
                startSendMessages();
            }
        }
        System.out.println("Done sending "+phoneNumber);

    }

    private boolean thereAreSmsToSend() {

        return mMessageSentCount < allPhoneNumbers.size();
    }

    private void sendNextMessage() {
        if (thereAreSmsToSend()) {
            if (message != null)
                sendSMS(allPhoneNumbers.get(mMessageSentCount), txtBody);
        } else {
            if(getApplication()!=null) {
                // Get instance of Vibrator from current Context
                Vibrator v = (Vibrator) getApplication().getSystemService(Context.VIBRATOR_SERVICE);

// Vibrate for 300 milliseconds
                v.vibrate(1000);
                Toast.makeText(getApplication().getBaseContext(), "All distress have been sent",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendSMS(final String phoneNumber, String message) {
        if(!stop)
        {
            String SENT = "SMS_SENT";
            String DELIVERED = "SMS_DELIVERED";

            SmsManager sms = SmsManager.getDefault();
            ArrayList<String> parts = sms.divideMessage(message);
            mMessageSentTotalParts = parts.size();

            Log.i("Message Count", "Message Count: " + mMessageSentTotalParts);

            ArrayList<PendingIntent> deliveryIntents = new ArrayList<>();
            ArrayList<PendingIntent> sentIntents = new ArrayList<>();

            if(getApplication()!=null) {
                PendingIntent sentPI = PendingIntent.getBroadcast(getApplication(), 0, new Intent(SENT), 0);
                PendingIntent deliveredPI = PendingIntent.getBroadcast(getApplication(), 0, new Intent(DELIVERED), 0);

                for (int j = 0; j < mMessageSentTotalParts; j++) {
                    sentIntents.add(sentPI);
                    deliveryIntents.add(deliveredPI);
                }
            }
            mMessageSentParts = 0;
            if (!phoneNumber.equals("") && count <= allPhoneNumbers.size()) {
                System.out.println("phoneNumber "+phoneNumber);
                sms.sendMultipartTextMessage(phoneNumber, null, parts, sentIntents, deliveryIntents);
                ++count;
            }

        }


    }

    private void registerBroadCastReceivers() {

        String SENT = "SMS_SENT";

        class myReceiverTwo extends BroadcastReceiver
        {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                if (count <= allPhoneNumbers.size()) {
                    switch (getResultCode()) {

                        case Activity.RESULT_OK:

                            mMessageSentParts++;
                            if (mMessageSentParts == mMessageSentTotalParts) {

                                mMessageSentCount++;
                                sendNextMessage();
                            }

                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:

                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:

                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:

                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:

                            break;
                    }
                }
            }
        }
        if(receiverTwo!=null)
            if(getApplicationContext()!=null) {
                getApplicationContext().unregisterReceiver(receiverTwo);
            }
        receiverTwo=null;
        receiverTwo=new myReceiverTwo();
        if(getApplicationContext()!=null)
        {
            getApplicationContext().registerReceiver(receiverTwo,new IntentFilter(SENT));
        }


        String DELIVERED = "SMS_DELIVERED";


        class myReceiver extends BroadcastReceiver
        {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {

                    case Activity.RESULT_OK:

                        break;
                    case Activity.RESULT_CANCELED:

                        break;
                }
            }
        }
        if(receiver!=null)
            if(getApplicationContext()!=null) {
                getApplicationContext().unregisterReceiver(receiver);
                receiver = null;
            }

        receiver=new myReceiver();
        if(getApplicationContext()!=null) {
            Intent intent = getApplicationContext().registerReceiver(receiver, new IntentFilter(DELIVERED));
        }
    }



}
