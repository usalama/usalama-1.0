package com.usalamatechnology.application.activity;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Looper;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.usalamatechnology.application.R;
import com.usalamatechnology.application.adapter.ImageAdapter;
import com.usalamatechnology.application.adapter.PlaceJSONParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by hp1 on 21-01-2015.
 */
public class HomeFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener,ResultCallback<LocationSettingsResult> {

    private ProgressDialog progress;
    boolean isCanceled=false;

    ///////////////////////////////////////////////////////////////////////////
    // Prefrence variables
    //location
    public static final String LOCATIONPREFERENCES = "LocationPrefs";
    public static SharedPreferences locationSharedPreferences;
    SharedPreferences.Editor locationEditor;
    Context ctx;

    public static final String lastKnownLatitude = "lastLatitude";
    public static final String lastKnownLongitude = "lastLongitude";
    public static final String lastKnownAccuracy = "lastAccuracy";

    //emergency contacts
    public static final String phoneKey = "phoneKey";
    public static final String phoneOneKey = "phoneKey1";
    public static final String phoneTwoKey = "phoneKey2";

    //messages
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;
    public static final String robberMessagesKey = "robber";
    public static final String burglarMessagesKey = "burglar";
    public static final String muggerMessagesKey = "mugger";
    public static final String carjackerMessagesKey = "carjacker";
    public static final String showhelp = "showhelp";
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // Store variables
int trialTest=0;
    //emergency contacts
    public static String phoneNumber = "";
    public static String phoneNumberOne = "";
    public static String phoneNumberTwo = "";

    //messages
    public static String messageRobber = "";
    public static String messageBurglar = "";
    public static String messageMugger = "";
    public static String messageCarjacker = "";

    //police location variables
    private String policeOneLong = " ";
    private String policeOneLat = " ";
    private String policeTwoLong = " ";
    private String policeTwoLat = " ";
    private String policeThreeLong = " ";
    private String policeThreeLat = " ";
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // Google api constants and variables
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;
    int REQUEST_CHECK_SETTINGS = 100;

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    //Timer constants and variables
    Timer timer = new Timer(true);
    private CountDownTimer timerLimit; //sets the maximum time a distress call can last while processing data
    private boolean isRunning = false; //flag to help determine if periodic updates task has been called

    final int milliseconds = 1000;
    final int seconds = 60;// in your case as per question one minute
    final int oneMinute = 60 * milliseconds;// in your case as per question one minute
    final int minute = 5; // Time in munutes between distress call updates
    final int periodicUpdate = milliseconds * seconds * minute;
    ///////////////////////////////////////////////////////////////////////////
    //specify the time interval in seconds after which task should run periodically

    ///////////////////////////////////////////////////////////////////////////
    // SMS sending variables
    SharedPreferences.Editor credentialsEditor;
    private PlacesTask placesTask;
    private ArrayList<String> allPhoneNumbers = new ArrayList<>();
    String message;
    boolean sent = false;
    boolean stop = false;
    private int mMessageSentCount;
    private String txtBody;
    private int mMessageSentTotalParts;
    private int mMessageSentParts;
    private int count;
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // Internet connection variables
    private InternetConnection internetConnection;
    ConnectionDetector cd;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    Boolean isInternetActive = false;
    // Connection detector class
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    int success;

    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // UI variables
    private GridView gridview;
    private MaterialDialog pd; //Varaible for all material dialogs
    private int isCancelled = 0;
    boolean isCheckContacts = false;
    SharedPreferences.Editor editor;
    private BroadcastReceiver receiver;
    private BroadcastReceiver receiverTwo;

    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";
    public static SharedPreferences credentialsSharedPreferences;

    public static final String savedphone = "phonenumber";
    private static final int PERMISSIONS_REQUEST_SEND_SMS = 100;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private LocationManager locationManager;
    private boolean GpsStatus;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LinearLayout wrapper = null;
        if (getActivity() != null) {
            wrapper = new LinearLayout(getActivity()); // for example


            inflater.inflate(R.layout.home_fragment, wrapper, true);
            credentialsSharedPreferences = getActivity().getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
            credentialsEditor = credentialsSharedPreferences.edit();
            System.out.println("+++++++++++++++++++++++++++++++++++++>>>>>>>>>> " + credentialsSharedPreferences.getString(savedphone, "Not Available"));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkLocationPermission();
            }

            Toolbar mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            if (mToolbar != null) {
                mToolbar.setTitle(R.string.app_name);
            }

            setHasOptionsMenu(true);


            sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            locationSharedPreferences = getActivity().getSharedPreferences(LOCATIONPREFERENCES, Context.MODE_PRIVATE);
            locationEditor = locationSharedPreferences.edit();
            editor = sharedpreferences.edit();

            buildGoogleApiClient();
            createLocationRequest();

            phoneNumber = sharedpreferences.getString(phoneKey, null);
            phoneNumberOne = sharedpreferences.getString(phoneOneKey, null);
            phoneNumberTwo = sharedpreferences.getString(phoneTwoKey, null);


            messageRobber = sharedpreferences.getString(robberMessagesKey, null);
            messageBurglar = sharedpreferences.getString(burglarMessagesKey, null);
            messageMugger = sharedpreferences.getString(muggerMessagesKey, null);
            messageCarjacker = sharedpreferences.getString(carjackerMessagesKey, null);

            cd = new ConnectionDetector(getActivity().getApplicationContext());
            gridview = (GridView) wrapper.findViewById(R.id.gridview);
            ImageAdapter imgAdapter = new ImageAdapter(getActivity().getApplicationContext());

            gridview.setAdapter(imgAdapter);


        }
        //Request to get current user location




        //sets emergency contacts in an array list
        setAllPhoneNumbers();

        //checks to see if user has set up emergency contacts
        checkContacts();

        return wrapper;
    }


    public void showLoader(){

    }


    public void explainLocation()
    {
        new MaterialDialog.Builder(getActivity())
                .title("Info")
                .titleColorRes(R.color.colorPrimary)
                .dividerColorRes(R.color.colorPrimaryDark)
                .content("The app needs to know your location so that it may accurately send your distress during an emergency.")
                .positiveText("GOT IT")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }
                })
                .show();
    }

    public boolean checkLocationPermission(){

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                explainLocation();
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }



    public void CheckGpsStatus(){

        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);

        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_cancel) {

            System.out.println("sharedpreferences.getString(showhelp, null) " + sharedpreferences.getString(showhelp, null));
            if ("yes".equals(sharedpreferences.getString(showhelp, null))) {
                System.out.println("sharedpreferences.getString(showhelp, null) ");
                if (getActivity() != null) {
                    new MaterialDialog.Builder(getActivity())
                            .title("Info")
                            .titleColorRes(R.color.colorPrimary)
                            .dividerColorRes(R.color.colorPrimaryDark)
                            .content("This button allows you to cancel a distress or stops the app from sending updates. Are you sure you want to cancel the distress call")
                            .positiveText("YES")
                            .negativeText("No")
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog) {
                                    editor.putString(showhelp, "no");
                                    editor.apply();
                                    stop = true;

                                }
                                @Override
                                public void onNegative(MaterialDialog dialog) {
                                    editor.putString(showhelp, "no");
                                    editor.apply();
                                    stop = false;
                                }
                            })
                            .show();
                }
            }
            else
            {
                new MaterialDialog.Builder(getActivity())
                        .title("Confirmation")
                        .titleColorRes(R.color.colorPrimary)
                        .dividerColorRes(R.color.colorPrimaryDark)
                        .content("Are you sure you want to cancel the distress call.")
                        .positiveText("YES")
                        .negativeText("No")
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                editor.putString(showhelp, "no");
                                editor.apply();
                                stop = true;
                            }
                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                editor.putString(showhelp, "no");
                                editor.apply();
                                stop = false;
                            }
                        })
                        .show();
            }

            if (timer != null) {
                timer.purge();
            }

            if (timerLimit != null) {
                timerLimit.cancel();
            }

            if (phoneNumber != null) {
                if (timerLimit != null || timer != null) {
                    if (isRunning) {
                        SmsManager smsManager = SmsManager.getDefault();
                        if (!phoneNumber.equals("")) {
                            smsManager.sendTextMessage(
                                    phoneNumber, null,
                                    "Sorry, the distress call was a mistake. I am not in any danger.", null,
                                    null);

                        }
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), "Your call for help has been cancelled", Toast.LENGTH_LONG).show();
                        }
                    }

                }

            }
            if (phoneNumberOne != null) {
                if (timerLimit != null || timer != null) {
                    SmsManager smsManager = SmsManager.getDefault();
                    if (!phoneNumberOne.equals("")) {
                        smsManager.sendTextMessage(
                                phoneNumber, null,
                                "Sorry, the distress call was a mistake. I am not in any danger.", null,
                                null);

                    }
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), "Your call for help has been cancelled", Toast.LENGTH_LONG).show();
                    }
                }
            }
            if (phoneNumberTwo != null) {
                if (timerLimit != null || timer != null) {
                    SmsManager smsManager = SmsManager.getDefault();
                    if (!phoneNumberTwo.equals("")) {
                        smsManager.sendTextMessage(
                                phoneNumber, null,
                                "Sorry, the distress call was a mistake. I am not in any danger.", null,
                                null);

                    }
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), "Your call for help has been cancelled", Toast.LENGTH_LONG).show();
                    }
                }
            }
            isCancelled = 1;

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {

            // show your own AlertDialog for example:
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            // set the message
            builder.setMessage("This app use google play services only for optional features")
                    .setTitle("Do you want to update?"); // set a title

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button

                    final String appPackageName = "com.google.android.gms";
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    }catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                }
            });
            AlertDialog dialog = builder.create();

        }


        if(GpsStatus)
        {
            System.out.println("YES GPS");
        }else {
            System.out.println("NO GPS");
        }

        //reconnects google api client
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

        setAllPhoneNumbers();
        checkContacts();

        //listen for clicks on grid view items
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, final int position, long id) {
                trialTest=0;
                isCanceled=false;
                progress = new ProgressDialog(getActivity());
                progress.setMessage("This distress will be sent in 5 seconds. Press the cancel button to stop.");
                progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progress.setIndeterminate(false);
                progress.setProgress(0);
                progress.setMax(5);
                // Put a cancel button in progress dialog
                progress.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    // Set a click listener for progress dialog cancel button
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // dismiss the progress dialog
                        progress.dismiss();
                        // Tell the system about cancellation
                        isCanceled = true;
                    }
                });
                progress.show();

                final int totalProgressTime = 5;
                final Thread t = new Thread() {
                    @Override
                    public void run() {
                        int jumpTime = 0;

                        while (jumpTime <= totalProgressTime) {
                            try {
                                sleep(1000);
                                jumpTime += 1;
                                progress.setProgress(jumpTime);
                                if(jumpTime == totalProgressTime)
                                {
                                    trialTest=1;
                                    theMessenger(position);
                                }
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        progress.dismiss();
                    }
                };
                t.start();




            }
        });
    }

    public void theMessenger(int position)
    {
        if(!isCanceled)
        {
            stop = false;
            //automatically set phone to silent mode to prevent rescuer from creating attention
            if (getActivity() != null) {
                AudioManager audio_mngr = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);

                if (audio_mngr != null)
                    audio_mngr.setRingerMode(AudioManager.RINGER_MODE_SILENT);
            }
                /*check to see if periodic task has already been initialized
                if so, it indicates user has clicked the tile more than once,so we
                have to re-initialize it to prevent exceptions

                */
            if (isRunning) {
                timer.cancel();
                timer.purge();
                timer = null;
                timer = new Timer(true);//Set boolean of timer to true to allow periodic task to run in background
                isRunning = false;
            }

            isRunning = true;
                /*if user has put up emergency contacts then respond to distress
                NOTE:Check connection handles the scenario if there are no emergency contacts
                */
            if (checkContacts()) {
                //periodic task that sends updates every 5 mins even from the background
                timer.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        if (!stop) {
                            startSendMessages();
                            isRunning = true;
                        }
                    }
                }, periodicUpdate, periodicUpdate);


                //check if timer limit has already been set. if yes,cancel it so that it can be reinitialized
                if (timerLimit != null) {
                    timerLimit.cancel();
                }

                Looper.prepare();
                //Set the count dow timer to wait for one minute(max) before forcing the app to send a distress call
                timerLimit = new CountDownTimer(oneMinute, milliseconds) {
                    public void onTick(long millisUntilFinished) {
                        // System.out.println("Ticking");
                    }

                    public void onFinish() {

                        if (pd != null) {
                            pd.dismiss();
                            if (internetConnection != null) {
                                internetConnection.cancel(true);
                            }

                            if (placesTask != null) {
                                placesTask.cancel(true);
                            }
                            if (!stop) {
                                startSendMessages();
                            }
                        }
                    }
                };
                timerLimit.start();

                //checks to get the latest and more accurate locations and use it or store in shared prefs
                handleNewLocation(mLastLocation);

                if (position == 0) {
                    message = messageRobber;
                } else if (position == 1) {
                    message = messageBurglar;
                } else if (position == 2) {
                    message = messageMugger;
                } else if (position == 3) {
                    message = messageCarjacker;
                }

                //When user presses distress call, first thing is to check for internet
                internetConnection = new InternetConnection();
                internetConnection.execute("");
            }
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    ///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
// GoogleAPIClient and Location overridden code and utility code

    //overridden code
//Handles the response from googleAPIClient
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_RECOVER_PLAY_SERVICES ) {
            if (resultCode == Activity.RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                if (getActivity() != null) {
                    Toast.makeText(getActivity().getApplicationContext(), R.string.text_to_install_googleplay_services,
                            Toast.LENGTH_SHORT).show();
                }
                //Activity.finish();
            }
        }
        if (requestCode == REQUEST_CHECK_SETTINGS) {

            if (resultCode == Activity.RESULT_OK) {

                Toast.makeText(getActivity(), "GPS enabled", Toast.LENGTH_LONG).show();
            } else {

                Toast.makeText(getActivity(), "GPS is not enabled", Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        System.out.println("Connected ***********");

        startLocationUpdates();
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        builder.build()
                );
        result.setResultCallback(this);

        if (mLastLocation == null) {

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);

            } else
            {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        } else {
            System.out.println("From on connected ************00000");
            locationEditor.putString(lastKnownLatitude, String.valueOf(mLastLocation.getLatitude()));
            locationEditor.putString(lastKnownLongitude, String.valueOf(mLastLocation.getLongitude()));
            locationEditor.putString(lastKnownAccuracy, String.valueOf(mLastLocation.getAccuracy()));
            locationEditor.apply();

            setAllPhoneNumbers();
            handleNewLocation(mLastLocation);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), R.string.alert_for_connection_suspended, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //      System.out.println("Latest location changed ************00000 --"+location.getAccuracy());
//        System.out.println("Previous location changed ************00000 --" + mLastLocation.getAccuracy());
        if (mLastLocation == null || location.getAccuracy() < mLastLocation.getAccuracy()) {
            System.out.println("We have a more accurate location ---------->");
            handleNewLocation(location);
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                if (getActivity() != null) {
                    connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
                }
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            // Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
            if (getActivity() != null) {
                Toast.makeText(getActivity(), R.string.alert_for_connectionfailed, Toast.LENGTH_LONG).show();
            }
        }
    }

    //utility code

    protected synchronized void buildGoogleApiClient() {
        if (getActivity() != null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    //check if google play services is available
    private boolean checkGooglePlayServices() {
        if (getActivity() != null) {
            int checkGooglePlayServices = GooglePlayServicesUtil
                    .isGooglePlayServicesAvailable(getActivity().getApplicationContext());
            if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
                GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices,
                        getActivity(), REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
                return false;
            }
        }
        return true;
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(20000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);

        }
        else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }


    private void handleNewLocation(Location location) {

        if (location != null) {
            locationEditor.putString(lastKnownLatitude, String.valueOf(location.getLatitude()));
            locationEditor.putString(lastKnownLongitude, String.valueOf(location.getLongitude()));
            locationEditor.putString(lastKnownAccuracy, String.valueOf(location.getAccuracy()));
            locationEditor.apply();
            mLastLocation = location;
            // Toast.makeText(getActivity(), "Latitude:" + location.getLatitude()+", Longitude:"+location.getLongitude(),Toast.LENGTH_SHORT).show();
        }
    }


    protected void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // Internet and nework utility methods
    //check for active internet connection
    public Boolean isOnline() {

        try {
            Process p1 = Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    private boolean checkForActiveConnection() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            if (isOnline()) {
                isInternetActive = true;
                // Toast.makeText(getActivity(), "You are connected to the internet", Toast.LENGTH_LONG).show();
                return true;
            } else {
                //Toast.makeText(getActivity(), "You are NOT connected to the internet", Toast.LENGTH_LONG).show();
                isInternetActive = false;
                return false;
            }
        } else {
            //Toast.makeText(getActivity(), "You are NOT connected to the internet", Toast.LENGTH_LONG).show();
            isInternetActive = false;
            return false;
        }
    }
    ///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////
// Send messages utility methods


    private boolean checkContacts() {

        if (allPhoneNumbers.size() == 0) {
            if(!isCheckContacts)
            {
                if(getActivity()!=null) {
                    new MaterialDialog.Builder(getActivity())
                            .title("No contacts")
                            .titleColorRes(R.color.colorPrimary)
                            .dividerColorRes(R.color.colorPrimaryDark)
                            .content("You have no emergency contacts.Open the last tab to add.")
                            .positiveText("OK")
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog) {
                                    MainActivity.pager.setCurrentItem(3);
                                }
                            })
                            .negativeText("Not now")
                            .show();
                }
                isCheckContacts=true;
            }
            return false;
        }
        return true;
    }


    private void setAllPhoneNumbers() {
        allPhoneNumbers.clear();
        if (phoneNumber != null && !"".equals(phoneNumber)) {
            allPhoneNumbers.add(phoneNumber);
        }

        if (phoneNumberOne != null && !"".equals(phoneNumberOne)) {
            allPhoneNumbers.add(phoneNumberOne);
        }

        if (phoneNumberTwo != null && !"".equals(phoneNumberTwo)) {
            allPhoneNumbers.add(phoneNumberTwo);
        }
    }


    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("download error", e.toString());
        } finally {
            assert iStream != null;
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }

    private void startSendMessages() {
        timerLimit.cancel();
        registerBroadCastReceivers();
        if (mLastLocation != null) {
            if (isInternetActive) {
                txtBody = "Usalama text: " + message + " Location:http://maps.google.com/?q=" + mLastLocation.getLatitude() + "," + mLastLocation.getLongitude() + "&z=14 .Accuracy level is " + mLastLocation.getLongitude() + " meters ";
                //mymessage=message;
                //mylat= String.valueOf(mLastLocation.getLatitude());
                //mylon= String.valueOf(mLastLocation.getLongitude());
                // new UploadCordinates().execute();
                if(policeOneLat!=" " && policeOneLong!=" ")
                {
                    txtBody=txtBody+"Police Station One" + " Location:http://maps.google.com/?q=" + policeOneLat + "," + policeOneLong;
                }
                if(policeTwoLat!=" " && policeTwoLong!=" ")
                {
                    txtBody=txtBody+" Police Station Two" + " Location:http://maps.google.com/?q=" + policeTwoLat + "," + policeTwoLong;
                }
                if(policeThreeLat!=" " && policeThreeLong!=" ")
                {
                    txtBody=txtBody+" Police Station Three" + " Location:http://maps.google.com/?q=" + policeThreeLat + "," + policeThreeLong;
                }

            } else {
                txtBody = "Usalama text: " + message + " Location:http://maps.google.com/?q=" + mLastLocation.getLatitude() + "," + mLastLocation.getLongitude() + "&z=14 .Accuracy level is " + mLastLocation.getLongitude()+" meters ";
            }

        }

        else {
            if (locationSharedPreferences.getString(lastKnownLatitude, null) != null && locationSharedPreferences.getString(lastKnownLongitude, null) != null && locationSharedPreferences.getString(lastKnownAccuracy, null) != null) {
                if (isInternetActive) {
                    txtBody = "Usalama text: " + message + " Location:http://maps.google.com/?q=" + locationSharedPreferences.getString(lastKnownLatitude, null) + "," + locationSharedPreferences.getString(lastKnownLongitude, null) + "&z=14 .Accuracy level is " + locationSharedPreferences.getString(lastKnownAccuracy, null) + " meters ";
                    //mymessage=message;
                    //mylat= locationSharedPreferences.getString(lastKnownLatitude, null);
                    // mylon= locationSharedPreferences.getString(lastKnownLongitude, null);
                    // new UploadCordinates().execute();
                    if(policeOneLat!=" " && policeOneLong!=" ")
                    {
                        txtBody=txtBody+"Police Station One" + " Location:http://maps.google.com/?q=" + policeOneLat + "," + policeOneLong;
                    }
                    if(policeTwoLat!=" " && policeTwoLong!=" ")
                    {
                        txtBody=txtBody+" Police Station Two" + " Location:http://maps.google.com/?q=" + policeTwoLat + "," + policeTwoLong;
                    }
                    if(policeThreeLat!=" " && policeThreeLong!=" ")
                    {
                        txtBody=txtBody+" Police Station Three" + " Location:http://maps.google.com/?q=" + policeThreeLat + "," + policeThreeLong;
                    }
                } else {
                    //mymessage=message;
                    //mylat= "";
                    //mylon= "";
                    //new UploadCordinates().execute();
                    txtBody = "Usalama text: " + message + " Location:http://maps.google.com/?q=" + locationSharedPreferences.getString(lastKnownLatitude, null) + "," + locationSharedPreferences.getString(lastKnownLongitude, null) + "&z=14 .Accuracy level is " + locationSharedPreferences.getString(lastKnownAccuracy, null)+" meters ";
                }

                //    Toast.makeText(getActivity(), "Usalama has used your last known location.For more accuracy turn on GPS and" +
                //        " internet connection.", Toast.LENGTH_LONG).show();
            } else {
                txtBody = "Usalama text: " + message;
                //   Toast.makeText(getActivity(), "Usalama cannot get your last location. Turn on GPS and internet connection",
                //        Toast.LENGTH_LONG).show();
            }

        }

        mMessageSentCount = 0;
        System.out.println("################# Txt body " + txtBody);

        if (message != null) {
            if (!stop) {
                sendSMS(allPhoneNumbers.get(mMessageSentCount), txtBody);
            }
        } else
        {
            if(getActivity()!=null) {
                Toast.makeText(getActivity(), "Distress not sent. Message field is not available", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void sendNextMessage() {
        if (thereAreSmsToSend()) {
            if (message != null)
                if (!stop) {
                    sendSMS(allPhoneNumbers.get(mMessageSentCount), txtBody);
                }
        } else {
            if(getActivity()!=null) {
                // Get instance of Vibrator from current Context
                Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

// Vibrate for 300 milliseconds
                v.vibrate(1000);
                Toast.makeText(getActivity().getBaseContext(), "All distress have been sent",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean thereAreSmsToSend() {

        return mMessageSentCount < allPhoneNumbers.size();
    }

    private void registerBroadCastReceivers(){

        String SENT = "SMS_SENT";

        class myReceiverTwo extends BroadcastReceiver
        {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                if (count <= allPhoneNumbers.size()) {
                    switch (getResultCode()) {

                        case Activity.RESULT_OK:

                            mMessageSentParts++;
                            if (mMessageSentParts == mMessageSentTotalParts) {

                                mMessageSentCount++;
                                sendNextMessage();
                            }
                            if(getActivity()!=null) {
                                Toast.makeText(getActivity(), "Distress call sent",
                                        Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            if(getActivity()!=null) {
                                Toast.makeText(getActivity(), "Inadequate airtime",
                                        Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            if(getActivity()!=null) {
                                Toast.makeText(getActivity(), "No service",
                                        Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            if(getActivity()!=null) {
                                Toast.makeText(getActivity(), "Null PDU",
                                        Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            if(getActivity()!=null) {
                                Toast.makeText(getActivity(), "Radio off",
                                        Toast.LENGTH_SHORT).show();
                            }
                            break;
                    }
                }
            }
        }
        if(receiverTwo!=null)
            if(getActivity()!=null) {
                try{
                    getActivity().unregisterReceiver(receiverTwo);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        receiverTwo=null;
        receiverTwo=new myReceiverTwo();
        if(getActivity()!=null)
        {
            try
            {
                getActivity().registerReceiver(receiverTwo,new IntentFilter(SENT));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }


        String DELIVERED = "SMS_DELIVERED";


        class myReceiver extends BroadcastReceiver
        {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {

                    case Activity.RESULT_OK:
                        if(getActivity()!=null) {
                            Toast.makeText(getActivity().getBaseContext(), "Distress call delivered",
                                    Toast.LENGTH_SHORT).show();
                            sent = true;
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        if(getActivity()!=null) {
                            Toast.makeText(getActivity().getBaseContext(), "Distress call not delivered",
                                    Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            }
        }
        if(receiver!=null)
            if(getActivity()!=null) {
                try{
                getActivity().unregisterReceiver(receiver);
                receiver = null;
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
            }

        receiver=new myReceiver();
        if(getActivity()!=null) {
            try{

                Intent intent = getActivity().registerReceiver(receiver, new IntentFilter(DELIVERED));
            }catch(Exception e)
            {
                e.printStackTrace();
            }

        }
        /*if(!sent)
        {
            if(intent==null)
            {
                if(dialog!=null)
                {dialog.dismiss();}

                if(pd!=null)
                {
                    pd.dismiss();
                }
                new FailedDelivery().show(getSupportFragmentManager(), "Delivery report");

            }
        }*/

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {


                            buildGoogleApiClient();
                            createLocationRequest();


                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(getActivity(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }
    private void sendSMS(final String phoneNumber, String message) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.SEND_SMS}, PERMISSIONS_REQUEST_SEND_SMS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            if (!stop) {
                String SENT = "SMS_SENT";
                String DELIVERED = "SMS_DELIVERED";

                SmsManager sms = SmsManager.getDefault();
                ArrayList<String> parts = sms.divideMessage(message);
                mMessageSentTotalParts = parts.size();

                Log.i("Message Count", "Message Count: " + mMessageSentTotalParts);

                ArrayList<PendingIntent> deliveryIntents = new ArrayList<>();
                ArrayList<PendingIntent> sentIntents = new ArrayList<>();

                if (getActivity() != null) {
                    PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(SENT), 0);
                    PendingIntent deliveredPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(DELIVERED), 0);

                    for (int j = 0; j < mMessageSentTotalParts; j++) {
                        sentIntents.add(sentPI);
                        deliveryIntents.add(deliveredPI);
                    }
                }
                mMessageSentParts = 0;
                if (!phoneNumber.equals("") && count <= allPhoneNumbers.size()) {
                    System.out.println("phoneNumber " + phoneNumber);
                    sms.sendMultipartTextMessage(phoneNumber, null, parts, sentIntents, deliveryIntents);
                    ++count;
                }

                UploadLocation uploader = new UploadLocation();
                uploader.execute("");
            }
        }

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // NO need to show the dialog;

                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  GPS disabled show the user a dialog to turn it on
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().

                    status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {

                    //failed to show dialog
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }

    private class UploadLocation extends AsyncTask<String, Integer, String> {

        String data = null;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... url) {
            uploadLocs();
            return data;
        }

        @Override
        protected void onPostExecute(String result) {

        }

    }

    private void uploadLocs(){
        final String latitude;
        final String longitude;

        // System.out.println("Latitudeeeeeeeeeeeeeeeeeeeeeeeee "+latitude);

        if(mLastLocation!=null)
        {
            latitude= String.valueOf(mLastLocation.getLatitude());
            longitude= String.valueOf(mLastLocation.getLongitude());
        }
        else
        {
            latitude= locationSharedPreferences.getString(lastKnownLatitude, null);
            longitude= locationSharedPreferences.getString(lastKnownLongitude, null);
        }


        //Creating a string request
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.LOCATION_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Repo************* "+response);
                        //If we are getting success from server
                        if(response.equalsIgnoreCase(Config.UPLOAD_SUCCESS)){

                            //Starting profile activity
                            //Toast.makeText(getActivity(), "Uploaded", Toast.LENGTH_LONG).show();
                        }else{
                            //If the server response is not success
                            //Displaying an error message on toast
                            // Toast.makeText(getActivity(), "Unable to upload location", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //You can handle error here if you want
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                String email = sharedPreferences.getString(Config.EMAIL_SHARED_PREF, "Not Available");
                String phone = credentialsSharedPreferences.getString(savedphone, "Not Available");


                //Adding parameters to request
                if(mLastLocation!=null)
                {
                    params.put(Config.KEY_LAT, String.valueOf(mLastLocation.getLatitude()));
                    params.put(Config.KEY_LON, String.valueOf(mLastLocation.getLongitude()));
                    params.put(Config.KEY_MES, message);
                    params.put(Config.KEY_EMAIL, email);
                    params.put(Config.KEY_PHONE, phone);
                }
                else if(locationSharedPreferences.getString(lastKnownLatitude, null) !=null )
                {
                    params.put(Config.KEY_LAT, locationSharedPreferences.getString(lastKnownLatitude, null));
                    params.put(Config.KEY_LON, locationSharedPreferences.getString(lastKnownLongitude, null));
                    params.put(Config.KEY_MES, message);
                    params.put(Config.KEY_EMAIL, email);
                    params.put(Config.KEY_PHONE, phone);
                }
                else
                {
                    params.put(Config.KEY_LAT, "0");
                    params.put(Config.KEY_LON, "0");
                    params.put(Config.KEY_MES, message);
                    params.put(Config.KEY_EMAIL, email);
                    params.put(Config.KEY_PHONE, phone);
                }



                //returning parameter
                return params;
            }
        };

        //Adding the string request to the queue
        if(getActivity()!=null )
        {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }

    }

    private class InternetConnection extends AsyncTask<String, Integer, Boolean> {
        @Override
        protected void onPreExecute() {
            if(getActivity()!=null) {
                pd = new MaterialDialog.Builder(getActivity())
                        .title("Location data")
                        .titleColorRes(R.color.colorPrimary)
                        .dividerColorRes(R.color.colorPrimaryDark)
                        .content("Getting location data(60s)...")
                        .progress(true, 0)
                        .show();
            }
        }

        @Override
        protected Boolean doInBackground(String... url) {
            //calls helper method to determine if their is an internet connection
            boolean status = checkForActiveConnection();
            pd.dismiss();
            return status;

        }


        @Override
        protected void onPostExecute(Boolean result) {

            if (result) {
                //if there is internet connection
                placesTask = new PlacesTask();
                if (mLastLocation != null) {
                   /* If there is internet connection and googleLocation client is NOT null
                   Execute the code nearest police station called PlacesTask
                     */
                    System.out.println("111111111111111");
                    placesTask.execute("https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                            "location=" + mLastLocation.getLatitude() + "," + mLastLocation.getLongitude() + "&radius=5000" + "&types=" + "police" +
                            "&sensor=true" + "&key=AIzaSyBBq3V_OY_A2RRG1zRe3XUx6iU6i7bHQ_Y");
                } else {
               /* If there is internet connection and googleLocation client is null
                   --Utilize values stored in shared preferences
                   --Execute the code nearest police station called PlacesTask
                     */
                    System.out.println("222222222222222");
                    if (locationSharedPreferences.getString(lastKnownLatitude, null) != null && !"".equals(locationSharedPreferences.getString(lastKnownLatitude, null)) && locationSharedPreferences.getString(lastKnownLongitude, null) != null && !"".equals(locationSharedPreferences.getString(lastKnownLatitude, null)) && locationSharedPreferences.getString(lastKnownAccuracy, null) != null && !"".equals(locationSharedPreferences.getString(lastKnownAccuracy, null))) {
                        placesTask.execute("https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                                "location=" + locationSharedPreferences.getString(lastKnownLatitude, null) + "," + locationSharedPreferences.getString(lastKnownLongitude, null) + "&radius=5000" + "&types=" + "police" +
                                "&sensor=true" + "&key=AIzaSyBBq3V_OY_A2RRG1zRe3XUx6iU6i7bHQ_Y");
                    } else {
                   /* If there is internet connection and googleLocation client is null
                   AND NO values stored in shared preferences
                   --Execute the code to start sending messages
                   --Cancel the timerLimit check as sending the messages should not be time consuming
                     */
                        System.out.println("333333333333333");
                        timerLimit.cancel();
                        if (!stop) {
                            startSendMessages();
                        }
                    }
                }
            } else {
            /* If there is internet connection and googleLocation client is null
                   AND NO values stored in shared preferences
                   --Execute the code to start sending messages
                   --Cancel the timerLimit check as sending the messages should not be time consuming
                     */
                timerLimit.cancel();
                if (!stop) {
                    startSendMessages();
                }
            }
        }
    }

    /**
     * A class, to download Google Places
     */
    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        @Override
        protected void onPreExecute() {
            if(getActivity()!=null) {
                pd = new MaterialDialog.Builder(getActivity())
                        .title("Distress messages")
                        .titleColorRes(R.color.colorPrimary)
                        .dividerColorRes(R.color.colorPrimaryDark)
                        .content("Sending your distress messages(60s)...")
                        .progress(true, 0)
                        .show();
            }
        }

        @Override
        protected String doInBackground(String... url) {
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }

    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;


        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                /** Getting the parsed data as a List construct */
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> list) {
            if (list != null) {
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {

                        MarkerOptions markerOptions = new MarkerOptions();

                        HashMap<String, String> hmPlace = list.get(i);

                        double lat = Double.parseDouble(hmPlace.get("lat"));

                        double lng = Double.parseDouble(hmPlace.get("lng"));

                        String name = hmPlace.get("place_name");

                        String vicinity = hmPlace.get("vicinity");

                        LatLng latLng = new LatLng(lat, lng);

                        markerOptions.position(latLng);

                        markerOptions.title(name + " : " + vicinity);
                        if (i == 0) {
                            policeOneLong = String.valueOf(lng);
                            policeOneLat = String.valueOf(lat);
                        } else if (i == 1) {
                            policeTwoLong = String.valueOf(lng);
                            policeTwoLat = String.valueOf(lat);
                        } else if (i == 2) {
                            policeThreeLong = String.valueOf(lng);
                            policeThreeLat = String.valueOf(lat);
                        }

                    }
                }

            }
            timerLimit.cancel();
            if (!stop) {
                startSendMessages();
            }
            pd.dismiss();
        }

    }
    ///////////////////////////////////////////////////////////////////////////

}