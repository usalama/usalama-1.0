package com.usalamatechnology.application.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.usalamatechnology.application.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class UsalamaAroundActivity extends AppCompatActivity implements OnMapReadyCallback,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";
    public static SharedPreferences credentialsSharedPreferences;
    SharedPreferences.Editor credentialsEditor;


    private String KEY_EMAIL = "email";
    public static final String savedemail = "email";

    private static final String TAG = "LocationActivity";
    private static final long INTERVAL = 1000 * 60 * 1; //1 minute
    private static final long FASTEST_INTERVAL = 1000 * 60 * 1; // 1 minute


    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;


    String mLastUpdateTime;
    GoogleMap mMap;
    public static final String LOCATIONPREFERENCES = "LocationPrefs";
    public static SharedPreferences locationSharedPreferences;
    SharedPreferences.Editor locationEditor;

    public static final String lastKnownLatitude = "lastLatitude";
    public static final String lastKnownLongitude = "lastLongitude";
    public static final String homeLatitude = "homeLatitude";
    public static final String homeLongitude = "homeLongitude";
    public static final String lastKnownAccuracy = "lastAccuracy";
    private Marker myMarker;

    ///////////////////////////////////////////////////////////////////////////
    // Internet connection variables
    TextView mainText;
    ConnectionDetector cd;
    // flag for Internet connection status
    private InternetConnection internetConnection;
    Boolean isInternetPresent = false;
    Boolean isInternetActive = false;
    private CameraPosition cameraPosition;
    private Context context;
    private String textContent;
    private String address;
    private float distance;
    private ProgressDialog pDialog;
    // Connection detector class

    ///////////////////////////////////////////////////////////////////////////

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate ...............................");
        context= this.getApplicationContext();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        cd = new ConnectionDetector(this.getApplicationContext());
        locationSharedPreferences = getSharedPreferences(LOCATIONPREFERENCES, Context.MODE_PRIVATE);
        locationEditor = locationSharedPreferences.edit();

        credentialsSharedPreferences = getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
        credentialsEditor = credentialsSharedPreferences.edit();

        //When user presses distress call, first thing is to check for internet
        internetConnection = new InternetConnection();
        internetConnection.execute("");
        //show error dialog if GoolglePlayServices not available
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }
        createLocationRequest();


        setContentView(R.layout.activity_usalama_around);

        createMapView();
        addMarker(16.0f);


        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        if(mToolbar!=null) {
            mToolbar.setTitle("");

            setSupportActionBar(mToolbar);
        }
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mainText =(TextView)findViewById(R.id.textViewMessage);



        findViewById(R.id.directions).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                System.out.println("Animate");

                if(mMap!=null && cameraPosition!=null)
                {
                    mMap.clear();
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    addMarker(16.0f);
                    drawCircle(mCurrentLocation.getLongitude(),mCurrentLocation.getLatitude(),mCurrentLocation.getAccuracy());
                  if(isInternetActive)
                   {
                       getNearestUsers();
                  }
                    else
                   {
                      Toast.makeText(UsalamaAroundActivity.this,"Cannot get nearest users without internet connection",Toast.LENGTH_LONG).show();
                       mainText.setText("Cannot get nearest users without internet connection");
                   }

                }

            }
        });
        //When user presses distress call, first thing is to check for internet
        internetConnection = new InternetConnection();
        internetConnection.execute("");


       // if(checkForActiveConnection())
       // {
            //getNearestUsers();
       // }
       // else
       // {
      //      Toast.makeText(UsalamaAroundActivity.this,"Cannot get nearest users without internet connection",Toast.LENGTH_LONG).show();
       //     mainText.setText("Cannot get nearest users without internet connection");
       // }
       /* if(isInternetActive)
        {
            getNearestUsers();
        }
        else
        {
            System.out.println("Internet is not active");
        }*/

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    private void getNearestUsers(){

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Checking for nearby Usalama users...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_NEARBY_USERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog

                        System.out.println("MAIN response "+s);

                        pDialog.dismiss();
                        if( !s.contains("There are no Usalama users within 200 meters of your location"))
                        {
                            mainText.setText("Nearby Usalama Users (200m)");
                            //Showing toast message of the response
                            //Toast.makeText(CreateProfile.this,"Successfully created",Toast.LENGTH_LONG);
                            System.out.println("MAIN response "+s);

                            String locs=s.substring(0,(s.length()-1));
                            System.out.println("Locs************** "+locs);
                            String [] longlat = locs.split("#");

                            for (int i = 0; i < longlat.length; i++) {
                                System.out.println("Locs************** "+longlat[i]);
                                String [] aLoc= longlat[i].split(",");
                                //System.out.println("Locs************** "+aLoc[i]);
                                LatLng currentLatLng = new LatLng(Double.parseDouble(aLoc[0]), Double.parseDouble(aLoc[1]));
                                myMarker = mMap.addMarker(new MarkerOptions()
                                                .position(currentLatLng)
                                                .title("User")
                                                .draggable(true)
                                                .snippet("Usalama user")
                                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
                                );
                                myMarker.showInfoWindow();

                            }
                        }
                        else
                        {
                            Toast.makeText(UsalamaAroundActivity.this,"There are no Usalama users within 200 meters of your location",Toast.LENGTH_LONG).show();
                            mainText.setText("There are no Usalama users within 200 meters of your location");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        //Toast.makeText(CreateProfile.this,"Error creating account",Toast.LENGTH_LONG);
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String


                //Creating parameters
                Map<String,String> params = new Hashtable<>();


                params.put(KEY_EMAIL, credentialsSharedPreferences.getString(savedemail,null));


                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent resultIntent = new Intent(this, MainActivity.class);
        startActivity(resultIntent);
    }


    public Boolean isOnline() {

        try {
            Process p1 = Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }


    private class InternetConnection extends AsyncTask<String, Integer, Boolean> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Boolean doInBackground(String... url) {
            //calls helper method to determine if their is an internet connection
            boolean status = checkForActiveConnection();
            return status;

        }


        @Override
        protected void onPostExecute(Boolean result) {

        }
    }

    private boolean checkForActiveConnection() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            if (isOnline()) {
                isInternetActive = true;
                // Toast.makeText(getActivity(), "You are connected to the internet", Toast.LENGTH_LONG).show();
                return true;
            } else {
                isInternetActive = false;
                return false;
            }
        } else {
            //Toast.makeText(getActivity(), "You are NOT connected to the internet", Toast.LENGTH_LONG).show();
            isInternetActive = false;
            return false;
        }
    }

    /**
     * Initialises the mapview
     */
    private void createMapView() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }



    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
       /* Log.d(TAG, "onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
        startLocationUpdates();

        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        if (mCurrentLocation == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);


        } else {

            handleNewLocation(mCurrentLocation);

        }*/

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    private void handleNewLocation(Location location) {

        if (location != null) {
            Log.d(TAG, "Firing onLocationChanged..............................................");
            mCurrentLocation = location;
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            addMarker(16.0f);
        }
    }

    protected void startLocationUpdates() {

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, R.string.alert_for_connection_suspended, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            // Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
            Toast.makeText(this, R.string.alert_for_connectionfailed, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //      System.out.println("Latest location changed ************00000 --"+location.getAccuracy());
//        System.out.println("Previous location changed ************00000 --" + mLastLocation.getAccuracy());
        /*if (mCurrentLocation == null || location.getAccuracy() < mCurrentLocation.getAccuracy()) {
            System.out.println("We have a more accurate location ---------->");
            handleNewLocation(location);
        }*/

        mCurrentLocation = location;
        if (myMarker != null) {
            myMarker.remove();
        }
        if(mMap!=null)
        {
            mMap.clear();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
       // myMarker = mMap.addMarker(markerOptions);

        if(isInternetActive)
        {
            getNearestUsers();
        }
        else
        {
            Toast.makeText(UsalamaAroundActivity.this,"Cannot get nearest users without internet connection",Toast.LENGTH_LONG).show();
            mainText.setText("Cannot get nearest users without internet connection");
        }

        drawCircle(mCurrentLocation.getLongitude(), mCurrentLocation.getLatitude(), 200);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
        cameraPosition = new CameraPosition.Builder()
                .target(latLng)      // Sets the center of the map to Mountain View
                .zoom(17)                   // Sets the zoom// Sets the tilt of the camera to 30 degrees
                .build();
        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    class GetAddress extends AsyncTask<String, Date, String> {


        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            getMyLocationAddress(1.3080165,36.8131399);
            return "";
        }

        @Override
        protected void onPostExecute(String msg) {
            //pDialog.setMessage("Calling Upload");

        }

    }

    public void getMyLocationAddress(Double latitude, Double longitude) {

        Geocoder geocoder= new Geocoder(this, Locale.ENGLISH);

        try {

            //Place your latitude and longitude
            List<Address> addresses = geocoder.getFromLocation(latitude,longitude, 1);

            if(addresses != null) {

                Address fetchedAddress = addresses.get(0);
                StringBuilder strAddress = new StringBuilder();

                for(int i=0; i<fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(fetchedAddress.getAddressLine(i)).append("\n");
                }

                address= strAddress.toString();

            }

            else
            {
                address="No location found!";
            }


        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //Toast.makeText(getApplicationContext(),"Could not get address..!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Adds a marker to the map
     */

    //overridden code
//Handles the response from googleAPIClient
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_RECOVER_PLAY_SERVICES) {
            if (resultCode == Activity.RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this.getApplicationContext(), R.string.text_to_install_googleplay_services,
                        Toast.LENGTH_SHORT).show();
                //Activity.finish();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    private void addMarker(Float zoom) {
/*

        if (mCurrentLocation != null) {
            if(googleMap!=null) {
                googleMap.clear();
                System.out.println("Was executed");
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), zoom));
            }

            LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            cameraPosition = new CameraPosition.Builder()
                    .target(currentLatLng)      // Sets the center of the map to Mountain View
                    .zoom(zoom)
                    .build();                   //
            if (null != googleMap) {

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                System.out.println("user "+mCurrentLocation.getLongitude());
                myMarker = googleMap.addMarker(new MarkerOptions()
                                .position(currentLatLng)
                                .title("Me")
                                .draggable(true)
                                .snippet("My position")
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.markerfinal))
                );
                myMarker.showInfoWindow();
                drawCircle(mCurrentLocation.getLongitude(), mCurrentLocation.getLatitude(), 200);


            }
        } else if (locationSharedPreferences.getString(lastKnownLatitude, null) != null && locationSharedPreferences.getString(lastKnownLongitude, null) != null && locationSharedPreferences.getString(lastKnownAccuracy, null) != null) {
            Toast.makeText(this, "Usalama has used your last known location.Turn on GPS and WIFI for better accuracy", Toast.LENGTH_LONG).show();
            if(googleMap!=null) {
                googleMap.clear();

                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        Double.parseDouble(locationSharedPreferences.getString(lastKnownLatitude, null)), Double.parseDouble(locationSharedPreferences.getString(lastKnownLongitude, null))), 16.0f));
            }


            LatLng currentLatLng = new LatLng(Double.parseDouble(locationSharedPreferences.getString(lastKnownLatitude, null)), Double.parseDouble(locationSharedPreferences.getString(lastKnownLongitude, null)));

            cameraPosition = new CameraPosition.Builder()
                    .target(currentLatLng)      // Sets the center of the map to Mountain View
                    .zoom(zoom)                   // Sets the zoom// Sets the tilt of the camera to 30 degrees
                    .build();                   //
            if (null != googleMap) {

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                myMarker = googleMap.addMarker(new MarkerOptions()
                                .position(currentLatLng)
                                .title("Me")
                                .draggable(true)
                                .snippet("My position")
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.markerfinal))

                );
                myMarker.showInfoWindow();

                drawCircle(Double.parseDouble(locationSharedPreferences.getString(lastKnownLongitude, null)), Double.parseDouble(locationSharedPreferences.getString(lastKnownLatitude, null)), 200);
                System.out.println("passed through");

            }
        }
        else
        {

            LatLng currentLatLng = new LatLng(-1.2833, 36.8167);
            Toast.makeText(this, "Usalama was unable to get your location.Please turn on GPS", Toast.LENGTH_LONG).show();
            cameraPosition = new CameraPosition.Builder()
                    .target(currentLatLng)      // Sets the center of the map to Mountain View
                    .zoom(zoom)                   // Sets the zoom// Sets the tilt of the camera to 30 degrees
                    .build();                   //
            if (null != googleMap) {

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                myMarker = googleMap.addMarker(new MarkerOptions()
                                .position(currentLatLng)
                                .title("Me")
                                .draggable(true)
                                .snippet("My position")
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.markerfinal))

                );
                myMarker.showInfoWindow();

                drawCircle(36.8167,-1.2833, 200);
            }
        }*/



    }

    private void drawCircle(double lon, double lat, float accuracy) {
        Log.d("Circle", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcuracy " + accuracy);
        LatLng latLng = new LatLng(lat, lon);

        CircleOptions circleOptions = new CircleOptions()
                .center(latLng)   //set center
                .radius(accuracy)   //set radius in meters
                .fillColor(0x110000FF)  //default
                .strokeColor(0x110000FF)
                .strokeWidth(1);
        if(mMap!=null) {
            mMap.addCircle(circleOptions);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            stopLocationUpdates();
        }

    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
        Log.d(TAG, "Location update stopped .......................");
    }

    @Override
    public void onResume() {
        super.onResume();


        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
            if (mGoogleApiClient.isConnected()) {
                startLocationUpdates();
                Log.d(TAG, "Location update resumed .....................");
            }
        }


    }
}

