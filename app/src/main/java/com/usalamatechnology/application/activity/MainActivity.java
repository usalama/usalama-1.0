package com.usalamatechnology.application.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.usalamatechnology.application.R;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener,MaterialTabListener {

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSeach;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String robberMessages = "robber";

    public static final String burglarMessages = "burglar";
    public static final String muggerMessages = "mugger";
    public static final String carjackerMessages = "carjacker";
    public static final String showhelp = "showhelp";
    public static final String openOnShake = "openOnShake";
    public static final String broadcast_group = "broadcast_group";
    public static final String broadcast_public = "broadcast_public";
    public static final String broadcast_locale = "broadcast_locale";
    public static final String volumeCounter = "volumeCounter";

    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";
    public static SharedPreferences credentialsSharedPreferences;

    SharedPreferences.Editor credentialsEditor;




    //emergency contacts
    public static final String phoneKey = "phoneKey";
    public static final String phoneOneKey = "phoneKey1";
    public static final String phoneTwoKey = "phoneKey2";

    // Store variables

    //emergency contacts
    public static String phoneNumber = "";
    public static String phoneNumberOne = "";
    public static String phoneNumberTwo = "";

    public static SharedPreferences sharedpreferences;
    public static ViewPager pager;
    private ViewPagerAdapter pagerAdapter;
    MaterialTabHost tabHost;
    private Resources res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        res = this.getResources();

        startService(new Intent(this, MyService.class));
        startService(new Intent(this, MyLocService.class));
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if(mToolbar!=null) {
            setSupportActionBar(mToolbar);
        }

        if(getSupportActionBar()!=null)
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        credentialsSharedPreferences =getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
        credentialsEditor= credentialsSharedPreferences.edit();

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(robberMessages, "Help! An armed robber has attacked me.");
        editor.putString(burglarMessages, "Help! A burglar has attacked me.");
        editor.putString(muggerMessages, "Help! A mugger is on my trail.");
        editor.putString(carjackerMessages, "Help! A carjacker has attacked me.");
        editor.putString(volumeCounter, "0");
        editor.putString(showhelp, "yes");
        if(sharedpreferences.getString(openOnShake,null )==null)
        {
            editor.putString(openOnShake, "no");
        }
        if(sharedpreferences.getString(broadcast_group,null )==null)
        {
            editor.putString(broadcast_group, "yes");
        }
        if(sharedpreferences.getString(broadcast_public ,null )==null)
        {
            editor.putString(broadcast_public, "no");
        }
        if(sharedpreferences.getString(broadcast_locale,null )==null)
        {
            editor.putString(broadcast_locale, "no");
        }

        editor.apply();

        phoneNumber = sharedpreferences.getString(phoneKey, null);
        phoneNumberOne = sharedpreferences.getString(phoneOneKey, null);
        phoneNumberTwo = sharedpreferences.getString(phoneTwoKey, null);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        // display the first navigation drawer view on app launch
        displayView(0);

        // Show menu icon
        tabHost = (MaterialTabHost) this.findViewById(R.id.tabHost);
        pager = (ViewPager) this.findViewById(R.id.pager);
        // init view pager
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // when user do a swipe the selected tab change
                tabHost.setSelectedNavigationItem(position);
            }
        });




        // insert all tabs from pagerAdapter data
        for (int i = 0; i < pagerAdapter.getCount(); i++) {
            tabHost.addTab(
                    tabHost.newTab()
                            .setIcon(getIcon(i))
                            .setTabListener(this)
            );
        }

    }

    @Override
    public void onTabSelected(MaterialTab tab) {
// when the tab is clicked the pager swipe content to the tab position
        pager.setCurrentItem(tab.getPosition());
    }


    @Override
    public void onTabReselected(MaterialTab tab) {
    }

    @Override
    public void onTabUnselected(MaterialTab tab) {
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        public Fragment getItem(int num) {
            if(num == 0) // if the position is 0 we are returning the First tab
            {
                return new HomeFragment();
            }
            else if  (num == 1)   // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
            {
                return new DistressFragment();
            }
            else if  (num == 2)   // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
            {
                return new NewsFragment();
            }
            else           // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
            {
                return new ContactsFragment();
            }
        }
        @Override
        public int getCount() {
            return 4;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            switch(position) {
                case 0: return "Home";
                case 1: return "Distress";
                case 2: return "Message";
                case 3: return "Contacts";
                default: return null;
            }
        }
    }
    /*
    * It doesn't matter the color of the icons, but they must have solid colors
    */
    private Drawable getIcon(int position) {
        switch(position) {
            case 0:
                return ContextCompat.getDrawable(this.getApplicationContext(), R.drawable.ic_home);
            case 1:
                return ContextCompat.getDrawable(this.getApplicationContext(), R.drawable.ic_distress);
            case 2:
                return ContextCompat.getDrawable(this.getApplicationContext(), R.drawable.ic_news);
            case 3:
                return ContextCompat.getDrawable(this.getApplicationContext(), R.drawable.ic_contacts);
        }
        return null;
    }

    //Logout function
    private void logout(){
        //Creating an alert dialog to confirm logout
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure you want to logout?");
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        //Getting out sharedpreferences
                        SharedPreferences preferences = getSharedPreferences(Config.SHARED_PREF_NAME,Context.MODE_PRIVATE);
                        //Getting editor
                        SharedPreferences.Editor editor = preferences.edit();

                        //Puting the value false for loggedin
                        editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, false);

                        //Putting blank value to email
                        editor.putString(Config.EMAIL_SHARED_PREF, "");

                        //Saving the sharedpreferences
                        editor.commit();

                        //Starting login activity
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });

        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        //Showing the alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        Intent i;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                title = getString(R.string.nav_item_home);
                break;
            case 1:
                i = new Intent(this, UsalamaAroundActivity.class);
                title=getString(R.string.nav_item_contact_us);
                startActivity(i);
                break;
            case 2:
                i = new Intent(this, MainTimerActivity.class);
                title=getString(R.string.nav_item_contact_us);
                startActivity(i);
                break;

            case 3:
                i = new Intent(this, WalkActivity.class);
                title=getString(R.string.nav_item_contact_us);
                startActivity(i);
                break;
            case 4:
                i = new Intent(this, UsalamaGroups.class);
                title=getString(R.string.nav_item_contact_us);
                startActivity(i);
                break;

            case 5:
                i = new Intent(this, EmergencyActivity.class);
                title=getString(R.string.nav_item_contact_us);
                startActivity(i);
                break;

            case 6:
                i = new Intent(this, ContactUsActivity.class);
                title=getString(R.string.nav_item_contact_us);
                startActivity(i);
                break;
            case 7:
                i = new Intent(this, SettingsActivity.class);
                title=getString(R.string.nav_item_settings);
                startActivity(i);
                break;
            case 8:
                logout();
                break;
            default:
                if (fragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();

                    // set the toolbar title

                }
                if(getSupportActionBar()!=null)
                getSupportActionBar().setTitle(title);
        }
    }

    protected void handleMenuSearch(){
        ActionBar action = getSupportActionBar(); //get the actionbar

        if(isSearchOpened){ //test if the search is open

            action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
            action.setDisplayShowTitleEnabled(true); //show the title in the action bar

            //hides the keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtSeach.getWindowToken(), 0);

            //add the search icon in the action bar
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_action_search));

            isSearchOpened = false;
        } else { //open the search entry

            action.setDisplayShowCustomEnabled(true); //enable it to display a
            // custom view in the action bar.
            action.setCustomView(R.layout.search_bar);//add the custom view
            action.setDisplayShowTitleEnabled(false); //hide the title

            edtSeach = (EditText)action.getCustomView().findViewById(R.id.edtSearch); //the text editor

            //this is a listener to do a search when the user clicks on search button
            edtSeach.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        doSearch();
                        return true;
                    }
                    return false;
                }
            });

            edtSeach.requestFocus();

            //open the keyboard focused in the edtSearch
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSeach, InputMethodManager.SHOW_IMPLICIT);

            //add the close icon
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_close_search));

            isSearchOpened = true;
        }
    }

    @Override
    public void onBackPressed() {
        if(isSearchOpened) {
           // handleMenuSearch();
            return;
        }
        super.onBackPressed();
    }

    private void doSearch() {
//
    }
}