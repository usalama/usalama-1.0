package com.usalamatechnology.application.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.usalamatechnology.application.R;

public class MoreActivity extends AppCompatActivity implements FragmentDrawerM.FragmentDrawerListener {

    private static String TAG = MoreActivity.class.getSimpleName();
    public static final String MyPREFERENCES = "detailsprefs" ;
    public static final String myLatitude = "myLatitude";
    public static final String myLongitude = "myLatitude";
    public static final String name = "name";
    public static final String distancetovic = "distancetovic";
    public static final String vicLongitude = "vicLongitude";
    public static final String vicLatitude = "vicLatitude";
    public static final String policeOneLong = "policeOneLong";
    public static final String policeOneLat = "policeOneLat";
    public static final String policeTwoLong = "policeTwoLong";
    public static final String policeTwoLat = "policeTwoLat";
    public static final String policeThreeLong = "policeThreeLong";
    public static final String policeThreeLat = "policeThreeLat";

    public static SharedPreferences detailsSharedpreferences;
    private Toolbar mToolbar;
    private FragmentDrawerM drawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        detailsSharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = detailsSharedpreferences.edit();
        editor.putString(myLatitude, getIntent().getStringExtra("myLongitude"));
        editor.putString(myLongitude, getIntent().getStringExtra("myLatitude"));
        editor.putString(name, getIntent().getStringExtra("name"));
        editor.putString(distancetovic, getIntent().getStringExtra("distancetovic"));
        editor.putString(vicLongitude, getIntent().getStringExtra("vicLongitude"));
        editor.putString(vicLatitude,getIntent().getStringExtra("vicLatitude"));
        editor.putString(policeOneLong, getIntent().getStringExtra("policeOneLong"));
        editor.putString(policeOneLat,getIntent().getStringExtra("policeOneLat"));
        editor.putString(policeTwoLong, getIntent().getStringExtra("policeTwoLong"));
        editor.putString(policeTwoLat, getIntent().getStringExtra("policeTwoLat"));
        editor.putString(policeThreeLong, getIntent().getStringExtra("policeThreeLong"));
        editor.putString(policeThreeLat, getIntent().getStringExtra("policeThreeLat"));
        editor.apply();

        System.out.println("myLatitude" + getIntent().getStringExtra("myLongitude"));
        System.out.println("myLatitude" + getIntent().getStringExtra("myLatitude"));
        System.out.println("myLatitude" + getIntent().getStringExtra("name"));
        System.out.println("distancetovic" + getIntent().getStringExtra("distancetovic"));
        System.out.println("myLatitude" + getIntent().getStringExtra("vicLongitude"));
        System.out.println("myLatitude" + getIntent().getStringExtra("vicLatitude"));
        System.out.println("myLatitude" + getIntent().getStringExtra("policeOneLong"));
        System.out.println("myLatitude" + getIntent().getStringExtra("policeOneLat"));
        System.out.println("myLatitude" + getIntent().getStringExtra("policeTwoLong"));
        System.out.println("myLatitude" + getIntent().getStringExtra("policeTwoLat"));
        System.out.println("myLatitude" + getIntent().getStringExtra("policeThreeLong"));
        System.out.println("myLatitude" + getIntent().getStringExtra("policeThreeLat"));

        drawerFragment = (FragmentDrawerM)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer_more);
        drawerFragment.setUp(R.id.fragment_navigation_drawer_more, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        // display the first navigation drawer view on app launch
        displayView(0);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new HomeFragmentMore();
                title = "Victim Details";
                break;
            case 1:
                fragment = new PoliceFragmentOne();
                title = "1st Police Details";
                break;
            case 2:
                fragment = new PoliceFragmentTwo();
                title = "2nd Police Details";
                break;
            case 3:
                fragment = new PoliceFragmentThree();
                title = "3rd Police Details";
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }
}