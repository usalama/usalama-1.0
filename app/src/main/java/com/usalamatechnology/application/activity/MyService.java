package com.usalamatechnology.application.activity;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MyService extends Service implements AccelerometerListener {
    public static final String openOnShake = "openOnShake";
    public static SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
       // Toast.makeText(getBaseContext(), "onResume Accelerometer Started",
        //        Toast.LENGTH_LONG).show();

        //Check device supported Accelerometer senssor or not
        if (AccelerometerManager.isSupported(this)) {

            //Start Accelerometer Listening
            AccelerometerManager.startListening(this);
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("Sensor", "Service  destroy");

        //Check device supported Accelerometer senssor or not
        if (AccelerometerManager.isListening()) {

            //Start Accelerometer Listening
            AccelerometerManager.stopListening();

            //Toast.makeText(getBaseContext(), "onDestroy Accelerometer Stoped",
               //     Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onAccelerationChanged(float x, float y, float z) {
        // Called when Motion Detected

    }

    @Override
    public void onShake(float force) {

        if(sharedpreferences.getString(openOnShake, "no").equals("yes"))
        {
            Context ctx=this; // or you can replace **'this'** with your **ActivityName.this**
            try {
                Intent i = ctx.getPackageManager().getLaunchIntentForPackage("com.edu.siredwin.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.edu.siredwin.application");
                ctx.startActivity(i);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getBaseContext(), "Motion detected",
                        Toast.LENGTH_LONG).show();
            }
        }

    }
}
