package com.usalamatechnology.application.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.usalamatechnology.application.R;
import com.usalamatechnology.application.model.DatabaseHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyReceiverSMS extends BroadcastReceiver {

    private String senderNum;
    private String message = "";
    private String name="";
    private String lon="";
    private String lat="";
    private float accuracy=0;

    private String policeOneLong="";
    private String policeOneLat="";

    private String policeTwoLong="";
    private String policeTwoLat="";

    private String policeThreeLong="";
    private String policeThreeLat="";


    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();

        try {
            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (Object aPdusObj : pdusObj) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);
                    message += currentMessage.getMessageBody();
                    senderNum = currentMessage.getDisplayOriginatingAddress();
                }

                Uri uri = null;
                if (check(message)) {
                    stripper(message);
                    uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(senderNum));

                    if (uri != null) {
                        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
                        if (cursor.moveToFirst()) {
                            name = cursor.getString(
                                    cursor.getColumnIndex(
                                            ContactsContract.Contacts.DISPLAY_NAME
                                    )
                            );
                        }
                        cursor.close();
                    }

                    // Get instance of Vibrator from current Context
                    Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

// Vibrate for 300 milliseconds
                    v.vibrate(1000);

                    DatabaseHelper databaseHelper = new DatabaseHelper(context);

                    Intent resultIntent = new Intent(context, MapsActivity.class);
                    resultIntent.putExtra("Message", message);
                    resultIntent.putExtra("Name", name);
                    resultIntent.putExtra("Number", senderNum + "'" + accuracy);
                    resultIntent.putExtra("long", lon);
                    resultIntent.putExtra("lat", lat);
                    resultIntent.putExtra("fromRec", "true");
                    resultIntent.putExtra("policeOneLong", policeOneLong);
                    resultIntent.putExtra("policeOneLat", policeOneLat);
                    resultIntent.putExtra("policeTwoLong", policeTwoLong);
                    resultIntent.putExtra("policeTwoLat", policeTwoLat);
                    resultIntent.putExtra("policeThreeLong", policeThreeLong);
                    resultIntent.putExtra("policeThreeLat", policeThreeLat);

                    databaseHelper.saveDistress(message, new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()),
                            new SimpleDateFormat("HH:mm:ss").format(new Date()), name, String.valueOf(accuracy),
                            senderNum, lon, lat, policeOneLong
                            , policeOneLat, policeTwoLong, policeTwoLat, policeThreeLong, policeThreeLat);

                    PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), resultIntent, 0);

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.notification)
                                    .setContentTitle("Call for help")
                                    .setContentIntent(pIntent)
                                    .setAutoCancel(true)
                                    .setContentText(name == null ? "Someone needs your help" : name + " needs your help");

                    NotificationManager mNotificationManager =
                            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    int mId = 1;
                    mNotificationManager.notify(mId, mBuilder.build());
                    System.out.println("Has shown notification");

                }
                if (message.contains(" seen your distress call.") && message.contains("Usalama Text:")) {

                    uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(senderNum));

                    if (uri != null) {
                        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
                        if (cursor.moveToFirst()) {
                            name = cursor.getString(
                                    cursor.getColumnIndex(
                                            ContactsContract.Contacts.DISPLAY_NAME
                                    )
                            );
                        }
                        cursor.close();
                    }

                    int duration = Toast.LENGTH_LONG;
                    Toast toast;
                    if (!name.equals("") || name == null)
                        toast = Toast.makeText(context, name + message, duration);
                    else
                        toast = Toast.makeText(context, "Someone " + message, duration);
                    toast.show();

                }

            }

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);

        }
    }


    private boolean check(String text) {
        System.out.println("Yesssssssssssssssssss");

        boolean isUsalama = false;
        if (text.contains("Usalama text: ") && text.contains(" Location:http://maps.google.com/?q=") || text.contains("Help!")) {
            isUsalama = true;

        }

        if (text.contains("e.com/?q=")) {
            isUsalama = true;
        }

        return isUsalama;
    }

    private void stripper(String text) {
        if (text != null) {

            String newValue = text.replace(" Location:http://maps.google.com/?q=", "'").replace("&z=14", "")
                    .replace(" Police Station One ", "").replace("Police Station Two", "").replace("Police Station Three", "")
                    .replace("\n", "").replace(".Accuracy level is ", "'").replace("meters", ",");
            char[] valueChar;
            valueChar = newValue.toCharArray();
            for (int i = 0; i < newValue.length(); i++) {
                if (valueChar[i] == '*') {
                    newValue = newValue.substring(i + 1, newValue.length());
                    break;
                }
            }
            String[] newArray = newValue.split("'");
            String[] msg = null;
            String[] victimLocArray= null;
            String[] accuracyArray= null;
            String[] policeOneArray= null;
            String[] policeTwoArray= null;
            String[] policeThreeArray= null;
            System.out.println("Array length " + newArray.length);
            if (newArray.length >= 1) {
                msg = newArray[0].split(",");
            }
            if (newArray.length >= 2) {
                victimLocArray = newArray[1].split(",");
            }
            if (newArray.length >= 3) {
                accuracyArray = newArray[2].split(",");
            }
            if (newArray.length >= 4) {
                policeOneArray = newArray[3].split(",");
            }
            if (newArray.length >= 5) {
                policeTwoArray = newArray[4].split(",");
            }
            if (newArray.length >= 6) {
                policeThreeArray = newArray[5].split(",");
            }


            String raw = msg[0];
            message = raw.substring(13);

            if (victimLocArray!=null && victimLocArray.length > 0) {
                lon = victimLocArray[1];
                lat = victimLocArray[0];
            }

            if (accuracyArray!=null && accuracyArray.length > 0) {
                accuracy = Float.parseFloat(accuracyArray[0]);
            }

            if (policeOneArray!=null && policeOneArray.length > 0) {
                policeOneLong = policeOneArray[1];
                policeOneLat = policeOneArray[0];
            }
            if (policeTwoArray!=null && policeTwoArray.length > 0) {
                policeTwoLong = policeTwoArray[1];
                policeTwoLat = policeTwoArray[0];
            }
            if (policeThreeArray!=null && policeThreeArray.length > 0) {
                policeThreeLong = policeThreeArray[1];
                policeThreeLat = policeThreeArray[0];
            }


            System.out.println("message" + message);
            System.out.println("lon" + lon);
            System.out.println("lat" + lat);
            System.out.println("accuracy" + accuracy);
            System.out.println("policeOneLong" + policeOneLong);
            System.out.println("policeOneLat" + policeOneLat);
            System.out.println("policeTwoLong" + policeTwoLong);
            System.out.println("policeTwoLat" + policeTwoLat);
            System.out.println("policeThreeLong" + policeThreeLong);
            System.out.println("policeThreeLat" + policeThreeLat);
        }

    }
}
