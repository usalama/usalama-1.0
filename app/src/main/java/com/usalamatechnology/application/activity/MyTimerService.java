package com.usalamatechnology.application.activity;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MyTimerService extends Service {
    private int mYear, mMonth, mDay, mHour, mMinute;
    public static final String TIMEPREFERENCES = "timePrefs";
    public static SharedPreferences timeSharedPreferences;
    SharedPreferences.Editor timeEditor;

    public static final String USERLOCATIONPREFERENCES = "userlocationPrefs";
    public static SharedPreferences userLocationSharedPreferences;
    SharedPreferences.Editor userLocationEditor;

    //emergency contacts
    public static final String phoneKey = "phoneKey";
    public static final String phoneOneKey = "phoneKey1";
    public static final String phoneTwoKey = "phoneKey2";

    //messages
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;

    public static final String userLatitude = "userLatitude";
    public static final String userLongitude = "userLongitude";
    public static final String userAccuracy = "userAccuracy";

    public static final String LOCATIONPREFERENCES = "LocationPrefs";
    public static SharedPreferences locationSharedPreferences;
    SharedPreferences.Editor locationEditor;


    public static final String homeLatitude = "homeLatitude";
    public static final String homeLongitude = "homeLongitude";


    public static final String timeHour = "timeHour";
    public static final String timeMinute = "timeMinute";
    public static final String setTime = "setTime";
    private float distance;

    private ArrayList<String> allPhoneNumbers = new ArrayList<>();
    String message;
    boolean sent = false;
    boolean stop=false;
    private int mMessageSentCount;
    private String txtBody;
    private int mMessageSentTotalParts;
    private int mMessageSentParts;
    private int count;
    private BroadcastReceiver receiver;
    private BroadcastReceiver receiverTwo;

    String phoneNumber;
    String phoneNumberOne ;
    String phoneNumberTwo;
    SimpleDateFormat sdf;
    Date systemDate;
    String myDate;
    double millse;
    Date Date1;
    Date Date2;

    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    Thread myThread;
    Runnable myRunnableThread;

    public MyTimerService() {
    }

    private void setAllPhoneNumbers() {
        allPhoneNumbers.clear();
        if (phoneNumber != null && !"".equals(phoneNumber)) {
            allPhoneNumbers.add(phoneNumber);
        }

        if (phoneNumberOne != null && !"".equals(phoneNumberOne)) {
            allPhoneNumbers.add(phoneNumberOne);
        }

        if (phoneNumberTwo != null && !"".equals(phoneNumberTwo)) {
            allPhoneNumbers.add(phoneNumberTwo);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void startSendMessages() {
        registerBroadCastReceivers();
            message="I need your help";
            if (locationSharedPreferences.getString(userLatitude, null) != null) {

                    txtBody = "Usalama text: " + message + " Location:http://maps.google.com/?q=" + locationSharedPreferences.getString(userLatitude, null) + "," + locationSharedPreferences.getString(userLongitude, null) + "&z=14 .Accuracy level is " + locationSharedPreferences.getString(userAccuracy, null)+" meters ";

            } else {
                txtBody = "Usalama text: " + message;

            }
        mMessageSentCount = 0;


        if (message != null) {
            sendSMS(allPhoneNumbers.get(mMessageSentCount), txtBody);
        } else
        {
            if(getApplication()!=null) {
                Toast.makeText(getApplication(), "Distress not sent. Message field is not available", Toast.LENGTH_LONG).show();
            }
        }
    }

    public static boolean checkSMSPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedpreferences = getApplication().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        timeSharedPreferences = getApplication().getSharedPreferences(TIMEPREFERENCES, Context.MODE_PRIVATE);
        timeEditor = timeSharedPreferences.edit();

        userLocationSharedPreferences = getApplication().getSharedPreferences(USERLOCATIONPREFERENCES, Context.MODE_PRIVATE);
        userLocationEditor = userLocationSharedPreferences.edit();

        locationSharedPreferences = getApplication().getSharedPreferences(LOCATIONPREFERENCES, Context.MODE_PRIVATE);
        locationEditor = locationSharedPreferences.edit();

        System.out.println("Timer has started ..............**********==============>>>>>>>>>>>");

        if(timeSharedPreferences.getString(timeHour, null)!=null)
        {
            // cancel if already existed
            if(mTimer != null) {
                mTimer.cancel();
            } else {
                // recreate new
                mTimer = new Timer();
            }
            // schedule task
            mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, 1000);
        }


    }

    private boolean thereAreSmsToSend() {

        return mMessageSentCount < allPhoneNumbers.size();
    }

    private void sendNextMessage() {
        if (thereAreSmsToSend()) {
            if (message != null)
                sendSMS(allPhoneNumbers.get(mMessageSentCount), txtBody);
        } else {
            if(getApplication()!=null) {
                // Get instance of Vibrator from current Context
                Vibrator v = (Vibrator) getApplication().getSystemService(Context.VIBRATOR_SERVICE);

// Vibrate for 300 milliseconds
                v.vibrate(1000);
                Toast.makeText(getApplication().getBaseContext(), "All distress have been sent",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendSMS(final String phoneNumber, String message) {
        if(!stop)
        {
            String SENT = "SMS_SENT";
            String DELIVERED = "SMS_DELIVERED";

            SmsManager sms = SmsManager.getDefault();
            ArrayList<String> parts = sms.divideMessage(message);
            mMessageSentTotalParts = parts.size();

            Log.i("Message Count", "Message Count: " + mMessageSentTotalParts);

            ArrayList<PendingIntent> deliveryIntents = new ArrayList<>();
            ArrayList<PendingIntent> sentIntents = new ArrayList<>();

            if(getApplication()!=null) {
                PendingIntent sentPI = PendingIntent.getBroadcast(getApplication(), 0, new Intent(SENT), 0);
                PendingIntent deliveredPI = PendingIntent.getBroadcast(getApplication(), 0, new Intent(DELIVERED), 0);

                for (int j = 0; j < mMessageSentTotalParts; j++) {
                    sentIntents.add(sentPI);
                    deliveryIntents.add(deliveredPI);
                }
            }
            mMessageSentParts = 0;
            if (!phoneNumber.equals("") && count <= allPhoneNumbers.size()) {
                System.out.println("phoneNumber "+phoneNumber);
                sms.sendMultipartTextMessage(phoneNumber, null, parts, sentIntents, deliveryIntents);
                ++count;
            }

        }


    }

    private void registerBroadCastReceivers() {

        String SENT = "SMS_SENT";

        class myReceiverTwo extends BroadcastReceiver
        {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                if (count <= allPhoneNumbers.size()) {
                    switch (getResultCode()) {

                        case Activity.RESULT_OK:

                            mMessageSentParts++;
                            if (mMessageSentParts == mMessageSentTotalParts) {

                                mMessageSentCount++;
                                sendNextMessage();
                            }

                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:

                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:

                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:

                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:

                            break;
                    }
                }
            }
        }
        if(receiverTwo!=null)
            if(getApplicationContext()!=null) {
                getApplicationContext().unregisterReceiver(receiverTwo);
            }
        receiverTwo=null;
        receiverTwo=new myReceiverTwo();
        if(getApplicationContext()!=null)
        {
            getApplicationContext().registerReceiver(receiverTwo,new IntentFilter(SENT));
        }


        String DELIVERED = "SMS_DELIVERED";


        class myReceiver extends BroadcastReceiver
        {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {

                    case Activity.RESULT_OK:

                        break;
                    case Activity.RESULT_CANCELED:

                        break;
                }
            }
        }
        if(receiver!=null)
            if(getApplicationContext()!=null) {
                getApplicationContext().unregisterReceiver(receiver);
                receiver = null;
            }

        receiver=new myReceiver();
        if(getApplicationContext()!=null) {
            Intent intent = getApplicationContext().registerReceiver(receiver, new IntentFilter(DELIVERED));
        }
    }

    class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA runnnnnnnnnnnnnnnnnnnnnner===========>");
                    if (timeSharedPreferences.getString(timeHour, null) != null) {
                        // display toast
                        sdf = new SimpleDateFormat("HH:mm:ss");

                        systemDate = Calendar.getInstance().getTime();
                        myDate = sdf.format(systemDate);
                        millse = 0.0;
                        Date1 = null;
                        try {
                            Date1 = sdf.parse(myDate);
                            Date2 = sdf.parse(timeSharedPreferences.getString(timeHour, null) + ":" + timeSharedPreferences.getString(timeMinute, null) + ":" + "00");
                            millse = Date2.getTime() - Date1.getTime();
                            if (millse <= 0) {

                                if (userLocationSharedPreferences.getString(userLatitude, null) != null) {
                                    Double userCurrentLat = Double.valueOf(userLocationSharedPreferences.getString(userLatitude, null));
                                    Double userCurrentLong = Double.valueOf(userLocationSharedPreferences.getString(userLongitude, null));

                                    Double homeCurrentLat = Double.valueOf(locationSharedPreferences.getString(homeLatitude, null));
                                    Double homeCurrentLong = Double.valueOf(locationSharedPreferences.getString(homeLongitude, null));

                                    Location locationA = new Location("point A");
                                    locationA.setLatitude(userCurrentLat);
                                    locationA.setLongitude(userCurrentLong);
                                    Location locationB = new Location("point B");
                                    locationB.setLatitude(homeCurrentLat);
                                    locationB.setLongitude(homeCurrentLong);
                                    distance = locationA.distanceTo(locationB);

                                    System.out.println("homeCurrentLat ********************* " + homeCurrentLat);
                                    System.out.println("homeCurrentLong ********************* " + homeCurrentLong);

                                    System.out.println("userCurrentLat ********************* " + userCurrentLat);
                                    System.out.println("userCurrentLong ********************* " + userCurrentLong);

                                    System.out.println("distance ********************* " + distance);
                                    if (distance > 100) {
                                        Intent intnet = new Intent("com.edu.siredwin.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.edu.siredwin.application.USER_ACTION");
                                        sendBroadcast(intnet);
                                        // System.out.println("Sent out sth....");
                                        timeEditor.putString(timeHour, null);
                                        timeEditor.putString(timeMinute, null);
                                        timeEditor.putString(setTime, null);
                                        timeEditor.apply();

                                        phoneNumber = sharedpreferences.getString(phoneKey, null);
                                        phoneNumberOne = sharedpreferences.getString(phoneOneKey, null);
                                        phoneNumberTwo = sharedpreferences.getString(phoneTwoKey, null);
                                        System.out.println("phoneNumberMy Timer Service " + phoneNumber);
                                        setAllPhoneNumbers();
                                        if (phoneNumber != null) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                if(checkSMSPermission(getApplicationContext()))
                                                {
                                                    startSendMessages();
                                                }
                                            }
                                            else
                                            {
                                                startSendMessages();
                                            }
                                        }

                                        System.out.println("Done sending " + phoneNumber);
                                    } else {
                                        // Toast.makeText(getApplication(), "Distress not sent. You are "+distance+"m from your home", Toast.LENGTH_LONG).show();
                                    }

                                }
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        MyTimerService.this.stopSelf();
                    }

                }

            });
        }
    }

}
