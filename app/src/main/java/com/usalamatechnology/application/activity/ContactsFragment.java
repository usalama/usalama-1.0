package com.usalamatechnology.application.activity;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.usalamatechnology.application.R;
import com.usalamatechnology.application.adapter.IObserver;
import com.usalamatechnology.application.adapter.RVAdapter;
import com.usalamatechnology.application.model.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp1 on 21-01-2015.
 */
public class ContactsFragment extends Fragment implements IObserver {


    public static List<Person> persons;
    private RecyclerView rv;
    private static final String TAG = "ContactsFragment";
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String name = "nameKey";
    public static final String phone = "phoneKey";
    public static final String nameOne = "nameKey1";
    public static final String phoneOne = "phoneKey1";
    public static final String nameTwo = "nameKey2";
    public static final String phoneTwo = "phoneKey2";
    public static SharedPreferences sharedpreferences;
    private static final int PICK_CONTACT_REQUEST1 = 0;
    private static final int EDIT_CONTACT_REQUEST1 = 1;
    private int mMessageSentCount;
    private int mMessageSentParts;
    private int count;
    private BroadcastReceiver receiverTwo;

    private static final int PERMISSIONS_REQUEST_SEND_SMS = 100;
    public static FragmentActivity host ;

    public static int positionOfCard=0;
    RVAdapter adapter;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.contacts_fragment, container, false);
        rootView.setTag(TAG);

        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        host=getActivity();


        rootView.findViewById(R.id.add_contact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(getActivity(), "Clicked pink Floating Action Button", Toast.LENGTH_SHORT).show();
                if (sharedpreferences.getString(name, null) != null && sharedpreferences.getString(nameOne, null) != null && sharedpreferences.getString(nameTwo, null) != null) {
                    if (sharedpreferences.getString(name, null) != "" && sharedpreferences.getString(nameOne, null) != "" && sharedpreferences.getString(nameTwo, null) != "") {
                        Toast.makeText(getActivity(), "Only a maximum of 3 contacts allowed, to edit click on the contact", Toast.LENGTH_LONG).show();
                    } else {
showContacts();
                    }


                } else {
                    showContacts();

                }

            }
        });

       //SharedPreferences.Editor editor = sharedpreferences.edit();
       //editor.clear();
      // editor.apply();

        rv = (RecyclerView) rootView.findViewById(R.id.rv);
        TextView emptyView = (TextView) rootView.findViewById(R.id.empty_view);


        if (sharedpreferences.getString(name,null)==null) {
            rv.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
        else {
            rv.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());

            rv.setLayoutManager(llm);
            rv.setHasFixedSize(true);
            registerForContextMenu(rv);

            initializeData();
             initializeAdapter();
        }
        return rootView;
    }
    private void showContacts() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            startActivityForResult(
                    new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
                    , PICK_CONTACT_REQUEST1);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(getActivity(), "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void initializeData(){

        persons = new ArrayList<>();
        if (sharedpreferences.getString(name, null)!=null && sharedpreferences.getString(name, null)!="") {
            System.out.println("One "+sharedpreferences.getString(name, null));
            persons.add(new Person(sharedpreferences.getString(name, null), sharedpreferences.getString(phone, null), R.drawable.logo));
        }

        if (sharedpreferences.getString(nameOne, null)!=null && sharedpreferences.getString(nameOne, null)!="") {
            System.out.println("Two "+sharedpreferences.getString(nameOne, null));
            persons.add(new Person(sharedpreferences.getString(nameOne, null), sharedpreferences.getString(phoneOne, null), R.drawable.logo));
        }
        if (sharedpreferences.getString(nameTwo, null)!=null && sharedpreferences.getString(nameTwo, null)!="") {
            System.out.println("Two " + sharedpreferences.getString(nameTwo, null));
            persons.add(new Person(sharedpreferences.getString(nameTwo, null), sharedpreferences.getString(phoneTwo, null), R.drawable.logo));
        }
    }

    private void initializeAdapter(){
        adapter = new RVAdapter(persons);
        rv.setAdapter(adapter);
        adapter.setListener(this);
    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {

        getActivity().getMenuInflater().inflate(R.menu.fragment_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        if(item.getTitle()=="Delete"){
            Toast.makeText(getActivity().getApplicationContext(),"calling code",Toast.LENGTH_LONG).show();
        }
        else{
            return false;
        }
        return true;
    }

    public void onItemClicked(int pos) {
        positionOfCard =pos;
        System.out.println("Pos " + pos);

            startActivityForResult(
                    new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
                    , EDIT_CONTACT_REQUEST1);

    }

    @Override
    public void onItemLongClicked(int pos) {
        positionOfCard =pos;

        new MaterialDialog.Builder(getActivity())
                .title("Confirmation")
                .titleColorRes(R.color.colorPrimary)
                .dividerColorRes(R.color.colorPrimaryDark)
                .content("Do you want to delete this contact?")
                .positiveText("Yes")
                .negativeText("No")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        int pos =ContactsFragment.positionOfCard;
                        System.out.println("POSITON "+pos);

                        if(pos==0)
                        {
                            System.out.println();
                            sharedpreferences.edit().remove(name).apply();
                            sharedpreferences.edit().remove(phone).apply();;
                        }
                        else if(pos==1)
                        {
                            sharedpreferences.edit().remove(nameOne).apply();
                            sharedpreferences.edit().remove(phoneOne).apply();
                        }
                        else if(pos==2)
                        {
                            sharedpreferences.edit().remove(nameTwo).apply();
                            sharedpreferences.edit().remove(phoneTwo).apply();
                        }


                        persons.get(pos).name="";
                        persons.get(pos).age="";

                        ContactsFragment.initializeData();
                        // Reload current fragment
                        if(host!=null)
                        {
                            host.getSupportFragmentManager()
                                    .beginTransaction()
                                    .detach(ContactsFragment.this)
                                    .attach(ContactsFragment.this)
                                    .commit();
                        }
                    }
                })
                .show();

    }

    @Override
    public void onTextClicked(int pos) {
        //System.out.println("Am clicked for edwin mimi");
    }


    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == PICK_CONTACT_REQUEST1) {
            if (resultCode == Activity.RESULT_OK) {
                renderContact(intent.getData());
                Log.d("Selection", intent.toString());
            }
        }
        if (requestCode == EDIT_CONTACT_REQUEST1) {
            if (resultCode == Activity.RESULT_OK) {
                editContact(intent.getData());
                Log.d("Selection", intent.toString());

            }
        }
    }

    private void renderContact(Uri uri) {
        if (uri != null) {
            String selecetedContactNumber=getMobileNumber(uri);
            String selecetedContactName = getDisplayName(uri);

            System.out.println("%%%%%%%%%%%%%%% "+selecetedContactNumber);

            if(selecetedContactName!=""&&selecetedContactName!=null&& selecetedContactNumber!=""&& selecetedContactNumber!=null)
            {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                if(sharedpreferences.getString(name,null)==null)
                {
                    editor.putString(name, selecetedContactName);
                    editor.putString(phone, selecetedContactNumber);
                    editor.apply();

                    startSendMessages(selecetedContactNumber);
                }
                else if(sharedpreferences.getString(nameOne,null)==null)
                {
                    editor.putString(nameOne, selecetedContactName);
                    editor.putString(phoneOne, selecetedContactNumber);
                    editor.apply();
                    startSendMessages(selecetedContactNumber);
                }
                else if(sharedpreferences.getString(nameTwo,null)==null)
                {
                    editor.putString(nameTwo, selecetedContactName);
                    editor.putString(phoneTwo, selecetedContactNumber);
                    editor.apply();
                    startSendMessages(selecetedContactNumber);
                }

            }
            else
            {
                if(selecetedContactNumber!="")
                {
                    new MaterialDialog.Builder(getActivity())
                            .title("No phone number")
                            .titleColorRes(R.color.colorPrimary)
                            .dividerColorRes(R.color.colorPrimaryDark)
                            .content("The selected contact lack an associated phone number")
                            .positiveText("OK")
                            .show();
                }

                if(selecetedContactName!="")
                {
                    new MaterialDialog.Builder(getActivity())
                            .title("No name")
                            .titleColorRes(R.color.colorPrimary)
                            .dividerColorRes(R.color.colorPrimaryDark)
                            .content("The selected contact lack an associated name")
                            .positiveText("OK")
                            .show();
                }
            }
            refreshFragment();

            //System.out.println("Name***************** "+sharedpreferences.getString(name, null));
        }
    }
    String message;
    String txtBody;
    private void startSendMessages(String phoneNumber) {

        registerBroadCastReceivers(phoneNumber);
        mMessageSentCount = 0;

 txtBody="Hi, I have added you as an emergency contact on the usalama app, please download it and add me as an emergency contact as well https://play.google.com/store/apps/details?id=com.usalamatechnology.application";
        if (txtBody != null) {

                sendSMS(phoneNumber, txtBody);

        } else
        {
            if(getActivity()!=null) {
                //Toast.makeText(getActivity(), "Distress not sent. Message field is not available", Toast.LENGTH_LONG).show();
            }
        }
    }
    private int mMessageSentTotalParts;

    private boolean thereAreSmsToSend() {

        return mMessageSentCount < 1;
    }

    private void sendNextMessage(String phone) {
        if (thereAreSmsToSend()) {
            if (message != null)

                    sendSMS(phone, txtBody);

        } else {
            if(getActivity()!=null) {
                // Get instance of Vibrator from current Context
                Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

// Vibrate for 300 milliseconds
                v.vibrate(1000);
              //  Toast.makeText(getActivity().getBaseContext(), "All distress have been sent",
                   //     Toast.LENGTH_SHORT).show();
            }
        }
    }
    private BroadcastReceiver receiver;

    private void registerBroadCastReceivers(final String phone){

        String SENT = "SMS_SENT";

        class myReceiverTwo extends BroadcastReceiver
        {
            @Override
            public void onReceive(Context arg0, Intent arg1) {

                    switch (getResultCode()) {

                        case Activity.RESULT_OK:

                            mMessageSentParts++;
                            if (mMessageSentParts == mMessageSentTotalParts) {

                                mMessageSentCount++;
                                sendNextMessage(phone);
                            }
                            if(getActivity()!=null) {
                               // Toast.makeText(getActivity(), "Distress call sent",
                                 //       Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            if(getActivity()!=null) {
                             //   Toast.makeText(getActivity(), "Inadequate airtime",
                                //        Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            if(getActivity()!=null) {
                              //  Toast.makeText(getActivity(), "No service",
                                  //      Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            if(getActivity()!=null) {
                              //  Toast.makeText(getActivity(), "Null PDU",
                                //        Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            if(getActivity()!=null) {
                               // Toast.makeText(getActivity(), "Radio off",
                                  //      Toast.LENGTH_SHORT).show();
                            }
                            break;
                    }

            }
        }
        if(receiverTwo!=null)
            if(getActivity()!=null) {
                try{
                    getActivity().unregisterReceiver(receiverTwo);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        receiverTwo=null;
        receiverTwo=new myReceiverTwo();
        if(getActivity()!=null)
        {
            try
            {
                getActivity().registerReceiver(receiverTwo,new IntentFilter(SENT));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }


        String DELIVERED = "SMS_DELIVERED";


        class myReceiver extends BroadcastReceiver
        {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {

                }
            }
        }
        if(receiver!=null)
            if(getActivity()!=null) {
                try{
                    getActivity().unregisterReceiver(receiver);
                    receiver = null;
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
            }

        receiver=new myReceiver();
        if(getActivity()!=null) {
            try{

                Intent intent = getActivity().registerReceiver(receiver, new IntentFilter(DELIVERED));
            }catch(Exception e)
            {
                e.printStackTrace();
            }

        }
        /*if(!sent)
        {
            if(intent==null)
            {
                if(dialog!=null)
                {dialog.dismiss();}

                if(pd!=null)
                {
                    pd.dismiss();
                }
                new FailedDelivery().show(getSupportFragmentManager(), "Delivery report");

            }
        }*/

    }

    private void sendSMS(final String phoneNumber, String message) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.SEND_SMS}, PERMISSIONS_REQUEST_SEND_SMS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {

                String SENT = "SMS_SENT";
                String DELIVERED = "SMS_DELIVERED";

                SmsManager sms = SmsManager.getDefault();
                ArrayList<String> parts = sms.divideMessage(message);
                mMessageSentTotalParts = parts.size();

                Log.i("Message Count", "Message Count: " + mMessageSentTotalParts);

                ArrayList<PendingIntent> deliveryIntents = new ArrayList<>();
                ArrayList<PendingIntent> sentIntents = new ArrayList<>();

                if (getActivity() != null) {
                    PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(SENT), 0);
                    PendingIntent deliveredPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(DELIVERED), 0);

                    for (int j = 0; j < mMessageSentTotalParts; j++) {
                        sentIntents.add(sentPI);
                        deliveryIntents.add(deliveredPI);
                    }
                }
                mMessageSentParts = 0;

                    sms.sendMultipartTextMessage(phoneNumber, null, parts, sentIntents, deliveryIntents);
                    ++count;


        }

    }

    private void editContact(Uri uri) {
        if (uri != null) {
            String selecetedContactNumber=getMobileNumber(uri);
            String selecetedContactName = getDisplayName(uri);

            if(selecetedContactName!=""&&selecetedContactName!=null&& selecetedContactNumber!=""&& selecetedContactNumber!=null)
            {
                if(positionOfCard==0)
                {
                    sharedpreferences.edit().putString(name, selecetedContactName).apply();
                    sharedpreferences.edit().putString(phone, selecetedContactNumber).apply();
                    persons.get(0).name=selecetedContactName;
                    persons.get(0).age=selecetedContactNumber;
                }
                else if(positionOfCard==1)
                {
                    sharedpreferences.edit().putString(nameOne, selecetedContactName).apply();
                    sharedpreferences.edit().putString(phoneOne, selecetedContactNumber).apply();
                    persons.get(1).name=selecetedContactName;
                    persons.get(1).age=selecetedContactNumber;
                }
                else if(positionOfCard==2)
                {
                    sharedpreferences.edit().putString(nameTwo, selecetedContactName).apply();
                    sharedpreferences.edit().putString(phoneTwo, selecetedContactNumber).apply();
                    persons.get(2).name=selecetedContactName;
                    persons.get(2).age=selecetedContactNumber;
                }


                initializeData();



                adapter.notifyDataSetChanged();
                System.out.println("EditContact " + sharedpreferences.getString(name, null));
                // Reload current fragment
            }
            else
            {
                if(selecetedContactNumber!="")
                {
                    new MaterialDialog.Builder(getActivity())
                            .title("No phone number")
                            .titleColorRes(R.color.colorPrimary)
                            .dividerColorRes(R.color.colorPrimaryDark)
                            .content("The selected contact lacks an associated phone number")
                            .positiveText("OK")
                            .show();
                }

                if(selecetedContactName!="")
                {
                    new MaterialDialog.Builder(getActivity())
                            .title("No name")
                            .titleColorRes(R.color.colorPrimary)
                            .dividerColorRes(R.color.colorPrimaryDark)
                            .content("The selected contact lack an associated name")
                            .positiveText("OK")
                            .show();
                }
            }



            refreshFragment();
        }
    }

    public  void refreshFragment()
    {
        if(getActivity()!=null)
        {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit();
        }

    }

    private String getMobileNumber(Uri uri) {
        String phoneNumber = null;
        Cursor contactCursor = getActivity().getContentResolver().query(
                uri, new String[]{ContactsContract.Contacts._ID},
                null, null, null);
        String id = null;
        if (contactCursor.moveToFirst()) {
            id = contactCursor.getString(
                    contactCursor.getColumnIndex(ContactsContract.Contacts._ID));
        }
        contactCursor.close();
        Cursor phoneCursor = getActivity().getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND "
                        + ContactsContract.CommonDataKinds.Phone.TYPE + " = "
                        + ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,
                new String[]{id},
                null
        );
        if (phoneCursor.moveToFirst()) {
            phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(
                            ContactsContract.CommonDataKinds.Phone.NUMBER)
            );
        }
        phoneCursor.close();
        return phoneNumber;
    }



    private String getDisplayName(Uri uri) {
        String displayName=null;
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        if (cursor.moveToFirst()) {
            displayName = cursor.getString(
                    cursor.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME
                    )
            );
        }
        cursor.close();
        return displayName;
    }
}