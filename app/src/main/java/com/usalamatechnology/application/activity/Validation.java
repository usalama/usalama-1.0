package com.usalamatechnology.application.activity;

import android.widget.EditText;

import java.util.regex.Pattern;

/**
 * Created by Gachukia on 8/20/2015.
 */
public class Validation {
    // Regular Expression
    // you can change the expression based on your need
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


    // Error Messages
    private static final String REQUIRED_MSG = "required";
    private static final String EMAIL_MSG = "invalid email";


    // call this method when you need to check email Validation
    public static boolean isEmailAddress(EditText editText, boolean required) {
        return isValid(editText, EMAIL_REGEX, EMAIL_MSG, required);
    }



    // return true if the input field is valid, based on the parameter passed
    public static boolean isValid(EditText editText, String regex, String errMsg, boolean required) {

        String text = editText.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editText.setError(null);

        // text required and editText is blank, so return false
        if ( required && !hasText(editText) )

            return false;

        // pattern doesn't match so returning false
        if (required && !Pattern.matches(regex, text)) {
            editText.setError(errMsg);
            return false;
        };

        return true;
    }

    // return true if the input field is valid, based on the parameter passed
    public static boolean isPasswordSame(EditText editTextOne,EditText editTextTwo) {

        String text = editTextOne.getText().toString().trim();
        String textTwo = editTextTwo.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editTextOne.setError(null);


        // pattern doesn't match so returning false
        if (!text.equals(textTwo)) {
            editTextTwo.setError("Passwords do not match");
            return false;
        };

        return true;
    }


    // return true if the input field is valid, based on the parameter passed
    public static boolean isPhonenumber(EditText editTextOne) {

        String text = editTextOne.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editTextOne.setError(null);
        if(!text.startsWith("0"))
        {
            editTextOne.setError("Phone number must start with '0'");
            return false;
        }

        // clearing the error, if it was previously set by some other values
        editTextOne.setError(null);
        // pattern doesn't match so returning false
        if (text.length()!=10) {
            editTextOne.setError("Invalid phone number");
            return false;
        }

        return true;
    }

    // check the input field has any text or not
    // return true if it contains text otherwise false
    public static boolean hasText(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            editText.setError(REQUIRED_MSG);
            return false;
        }

        return true;
    }

    public static boolean isLimit(EditText editText) {

        String text = editText.getText().toString();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() >250) {
            editText.setError("Exceeded the limit of characters(250)");
            return false;
        }

        return true;
    }
}
