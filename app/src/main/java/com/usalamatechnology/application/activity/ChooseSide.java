package com.usalamatechnology.application.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.usalamatechnology.application.R;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class ChooseSide extends AppCompatActivity {

    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";
    public static SharedPreferences credentialsSharedPreferences;
    SharedPreferences.Editor credentialsEditor;

    List<String> spinnerArray;
    ArrayAdapter<String> adapter;
    String  KEY_CHOICE="choice";
    String  KEY_GROUPNAME="group_name";
    String  KEY_HOUSENO="house_no";
    String  KEY_USEREMAIL="user_email";
    public static final String savedemail = "email";
    public static final String isFirstGroup = "isFirstGroup";

    Spinner sItems;
    private MaterialDialog pd; //Varaible for all material dialogs

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_side);

        credentialsSharedPreferences = getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
        credentialsEditor = credentialsSharedPreferences.edit();

        // you need to have a list of data that you want the spinner to display
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Choose your option");
        spinnerArray.add("Estate");
        spinnerArray.add("Personal");

        adapter = new ArrayAdapter<String>(
                ChooseSide.this, R.layout.spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sItems = (Spinner) findViewById(R.id.spinner2);
        sItems.setAdapter(adapter);

        findViewById(R.id.add_status).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                credentialsEditor.putString(KEY_CHOICE, sItems.getSelectedItem().toString());
                credentialsEditor.apply();
                System.out.println("******************************"+ credentialsSharedPreferences.getString(KEY_CHOICE, "Not Available"));
                if(credentialsSharedPreferences.getString(KEY_CHOICE, "Not Available") == "Estate")
                {
                    Intent intent = new Intent(ChooseSide.this, PickGroupActivity.class);
                    startActivity(intent);
                }
                else if(credentialsSharedPreferences.getString(KEY_CHOICE, "Not Available") == "Personal")
                {
                    updateUserDetails();
                }
                else
                {
                    Toast.makeText(ChooseSide.this,"Please choose a valid option", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void updateUserDetails(){


        pd = new MaterialDialog.Builder(this)
                .title("Registration")
                .titleColorRes(R.color.colorPrimary)
                .dividerColorRes(R.color.colorPrimaryDark)
                .content("Registering user")
                .progress(true, 0)
                .show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.updateUserDetails2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        if(pd!=null)
                        {
                            pd.dismiss();
                        }

                        credentialsEditor.putString(isFirstGroup, "true");
                        credentialsEditor.apply();

                        credentialsEditor.putString(KEY_GROUPNAME, "PERSONAL");
                        credentialsEditor.apply();

                        credentialsEditor.putString(KEY_HOUSENO, "0");
                        credentialsEditor.apply();

                        Intent intent = new Intent(ChooseSide.this, MainActivity.class);
                        startActivity(intent);

                        Toast.makeText(ChooseSide.this,"Successfully registered!", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {



                //Creating parameters
                Map<String,String> params = new Hashtable<>();

                //Adding parameters
                params.put(KEY_HOUSENO,"0");
                params.put(KEY_GROUPNAME, "PERSONAL");
                params.put(KEY_USEREMAIL, credentialsSharedPreferences.getString(savedemail, "Not Available"));


                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }
}
