package com.usalamatechnology.application.activity;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

import com.usalamatechnology.application.R;
import com.usalamatechnology.application.model.DatabaseHelper;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GCMNotificationIntentService extends IntentService {
    // Sets an ID for the notification, so it can be updated
    public static final int notifyID = 9001;
    NotificationCompat.Builder builder;
    private String msg;
    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";

    public static SharedPreferences credentialsSharedPreferences;
    SharedPreferences.Editor credentialsEditor;
    public static final String savedjson = "json";
    public static final String revoked = "revoked";
    public static final String forNews = "forNews";

    public static final String savedresponse = "response";
    public static final String savedEmail = "email";
    private JSONObject jsonObj;
    private String location;

    public GCMNotificationIntentService() {
        super("GcmIntentService");
    }

    public static final String TAG = "GCMNotificationIntentService";
String userEmail;
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        credentialsSharedPreferences = getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
        credentialsEditor = credentialsSharedPreferences.edit();

        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                    .equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                    .equals(messageType)) {
                sendNotification("Deleted messages on server: "
                        + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                    .equals(messageType)) {
                String message= String.valueOf(extras.get(ApplicationConstants.MSG_KEY));
                try {
                    jsonObj = new JSONObject(message);
                    System.out.println("JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ "+jsonObj.getString("msg"));
                     msg=jsonObj.getString("msg");
                    System.out.println("I would like to walk with you--"+msg);
                    if(msg.contains("I would like to walk with you"))
                    {
                        System.out.println("contains get it");
                        System.out.println("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY "+jsonObj.getString("msg"));
                        userEmail=jsonObj.getString("email");
                        respondWalkRequest(message);
                    }
                    else if(msg.contains("A police person has seen your distress call.") )
                    {
                        System.out.println("contains get it");
                        System.out.println("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY "+jsonObj.getString("msg"));
                        userEmail=jsonObj.getString("email");
                        policeResponse(message);
                    }
                    else if(msg.contains("accept")||msg.contains("reject") )
                    {
                        System.out.println("contains get it");
                        System.out.println("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY "+jsonObj.getString("msg"));
                        userEmail=jsonObj.getString("email");
                        userResponse(message);
                    }
                    else if(msg.contains("Security news alert from usalama") )
                    {
                        System.out.println("contains get it");
                        System.out.println("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY "+jsonObj.getString("msg"));
                        //userEmail=jsonObj.getString("email");
                        newsAlert(message);
                    }
                    else if(msg.contains("User has revoked access to their location data."))
                    {
                        System.out.println("contains get it");
                        System.out.println("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY "+jsonObj.getString("msg"));
                        userEmail=jsonObj.getString("email");
                        revokeResponse(message);
                    }
                    else
                    {
                        location=jsonObj.getString("location");
                        System.out.println("contains is wrong");
                        sendNotification(message);
                    }

                    /*if(msg =="I would like to walk with you")
                    {
                        System.out.println("equals get it");
                    }
                    else
                    {
                        System.out.println("equals is wrong");
                    }

                    if(msg=="I would like to walk with you")
                    {
                        System.out.println("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY "+jsonObj.getString("msg"));
                        userEmail=jsonObj.getString("email");
                        respondWalkRequest(message);
                    }
                    else
                    {
                        sendNotification(message);
                    }*/
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("EEEEEEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRRRRRRRRRROOOOOOOOOOOOOORR");
                }
                //sendNotification("" + extras.get(ApplicationConstants.MSG_KEY)); //When Message is received normally from GCM Cloud Server
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(String greetMsg) {
        System.out.println("sendNotification was called");
        System.out.println("Location-------------------------> "+location);
        String [] data= location.split(",");
        String message=msg;
        String lat=data[0];
        String lon=data[1];
        String name=data[2];
        String senderNum=data[3];
        String accuracy="0";
        // Get instance of Vibrator from current Context
        Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

// Vibrate for 300 milliseconds
        v.vibrate(1000);

        DatabaseHelper databaseHelper = new DatabaseHelper(this);

        Intent resultIntent = new Intent(this, MapsActivity.class);
        resultIntent.putExtra("Message", message);
        resultIntent.putExtra("Name", name);
        resultIntent.putExtra("Number", senderNum + "'" + accuracy);
        resultIntent.putExtra("long", lon);
        resultIntent.putExtra("lat", lat);
        resultIntent.putExtra("fromRec", "true");
        resultIntent.putExtra("policeOneLong", "");
        resultIntent.putExtra("policeOneLat", "");
        resultIntent.putExtra("policeTwoLong", "");
        resultIntent.putExtra("policeTwoLat", "");
        resultIntent.putExtra("policeThreeLong", "");
        resultIntent.putExtra("policeThreeLat", "");

        databaseHelper.saveDistress(message, new SimpleDateFormat("yyyy/MM/dd").format(new Date()),
                new SimpleDateFormat("HH:mm:ss").format(new Date()), name, String.valueOf(accuracy),
                senderNum, lon, lat, ""
                , "", "", "", "", "");
        resultIntent.setAction(Intent.ACTION_MAIN);
        resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                resultIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mNotifyBuilder;
        NotificationManager mNotificationManager;

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotifyBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("Distress call")
                .setContentText("You've received a distress call from "+name)
                .setSmallIcon(R.drawable.notification);
        // Set pending intent
        mNotifyBuilder.setContentIntent(resultPendingIntent);

        // Set Vibrate, Sound and Light
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;

        mNotifyBuilder.setDefaults(defaults);
        // Set the content for Notification
        mNotifyBuilder.setContentText("You've received a distress call from "+name);
        // Set autocancel
        mNotifyBuilder.setAutoCancel(true);
        // Post a notification
        mNotificationManager.notify(notifyID, mNotifyBuilder.build());
    }


    private void newsAlert(String greetMsg) {

            Intent resultIntent = new Intent(this, MainActivity.class);
            resultIntent.putExtra("greetjson", greetMsg);

            try {
                credentialsEditor.putString(forNews,"true");
            } catch (Exception e) {
                e.printStackTrace();
            }
            credentialsEditor.apply();

            System.out.println("greetjson "+ greetMsg);
            resultIntent.setAction(Intent.ACTION_MAIN);
            resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);

            NotificationCompat.Builder mNotifyBuilder;
            NotificationManager mNotificationManager;

            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("Security news")
                    .setContentText("You have unread security alerts")
                    .setSmallIcon(R.drawable.notification);
            // Set pending intent
            mNotifyBuilder.setContentIntent(resultPendingIntent);

            // Set Vibrate, Sound and Light
            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;

            mNotifyBuilder.setDefaults(defaults);
            // Set the content for Notification
            mNotifyBuilder.setContentText("You have unread security alerts");
            // Set autocancel
            mNotifyBuilder.setAutoCancel(true);
            // Post a notification
            mNotificationManager.notify(notifyID, mNotifyBuilder.build());

    }


    private void policeResponse(String greetMsg) {

            NotificationCompat.Builder mNotifyBuilder;
            NotificationManager mNotificationManager;

            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("Police response")
                    .setContentText(userEmail+", a police has seen your distress.")
                    .setSmallIcon(R.drawable.notification);
            // Set Vibrate, Sound and Light
            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;

            mNotifyBuilder.setDefaults(defaults);
            // Set the content for Notification
            mNotifyBuilder.setContentText(userEmail+", a police has seen your distress.");
            // Set autocancel
            mNotifyBuilder.setAutoCancel(true);
            // Post a notification
            mNotificationManager.notify(notifyID, mNotifyBuilder.build());

    }


    private void userResponse(String greetMsg) {

        if(msg.contains("accept"))
        {
            Intent resultIntent = new Intent(this, WalkMapsActivity.class);
            resultIntent.putExtra("greetjson", greetMsg);

            try {
                credentialsEditor.putString(savedresponse,jsonObj.getString("msg"));
                credentialsEditor.putString(savedEmail,jsonObj.getString("email"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            credentialsEditor.apply();

            System.out.println("greetjson "+ greetMsg);
            resultIntent.setAction(Intent.ACTION_MAIN);
            resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);

            NotificationCompat.Builder mNotifyBuilder;
            NotificationManager mNotificationManager;

            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("Walk request")
                    .setContentText(userEmail+" has accepted your request to walk.")
                    .setSmallIcon(R.drawable.notification);
            // Set pending intent
            mNotifyBuilder.setContentIntent(resultPendingIntent);

            // Set Vibrate, Sound and Light
            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;

            mNotifyBuilder.setDefaults(defaults);
            // Set the content for Notification
            mNotifyBuilder.setContentText(userEmail+" has accepted your request to walk.");
            // Set autocancel
            mNotifyBuilder.setAutoCancel(true);
            // Post a notification
            mNotificationManager.notify(notifyID, mNotifyBuilder.build());
        }
        else
        {


            NotificationCompat.Builder mNotifyBuilder;
            NotificationManager mNotificationManager;

            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("Walk request")
                    .setContentText(userEmail+" has rejected your request to walk.")
                    .setSmallIcon(R.drawable.notification);
            // Set Vibrate, Sound and Light
            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;

            mNotifyBuilder.setDefaults(defaults);
            // Set the content for Notification
            mNotifyBuilder.setContentText(userEmail+" has rejected your request to walk.");
            // Set autocancel
            mNotifyBuilder.setAutoCancel(true);
            // Post a notification
            mNotificationManager.notify(notifyID, mNotifyBuilder.build());
        }


    }


    private void revokeResponse(String greetMsg) {
        System.out.println("revokeResponse was called");

        Intent resultIntent = new Intent(this, WalkMapsActivity.class);

        credentialsEditor.putString(savedEmail, userEmail);
        credentialsEditor.putString(revoked,"true");
        credentialsEditor.apply();
        resultIntent.setAction(Intent.ACTION_MAIN);
        resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                resultIntent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder mNotifyBuilder;
        NotificationManager mNotificationManager;
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("Access revoked")
                .setContentText(userEmail+" has revoked your access to their location data")
                .setSmallIcon(R.drawable.notification);
        // Set pending intent
        mNotifyBuilder.setContentIntent(resultPendingIntent);
        // Set Vibrate, Sound and Light
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;

        mNotifyBuilder.setDefaults(defaults);
        // Set the content for Notification
        mNotifyBuilder.setContentText(userEmail+" has revoked your access to their location data");
        // Set autocancel
        mNotifyBuilder.setAutoCancel(true);
        // Post a notification
        mNotificationManager.notify(notifyID, mNotifyBuilder.build());
    }


    private void respondWalkRequest(String greetMsg) {

        System.out.println("respondWalkRequest was called");
        Intent resultIntent = new Intent(this, WalkMapsActivity.class);
        System.out.println("greetjson "+ greetMsg);
        resultIntent.putExtra("greetjson", greetMsg);

        credentialsEditor.putString(savedjson, greetMsg);
        credentialsEditor.apply();

        resultIntent.setAction(Intent.ACTION_MAIN);
        resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                resultIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mNotifyBuilder;
        NotificationManager mNotificationManager;

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotifyBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("Walk request")
                .setContentText(userEmail+" would like to walk you")
                .setSmallIcon(R.drawable.notification);
        // Set pending intent
        mNotifyBuilder.setContentIntent(resultPendingIntent);

        // Set Vibrate, Sound and Light
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;

        mNotifyBuilder.setDefaults(defaults);
        // Set the content for Notification
        mNotifyBuilder.setContentText(userEmail+" would like to walk you.");
        // Set autocancel
        mNotifyBuilder.setAutoCancel(true);
        // Post a notification
        mNotificationManager.notify(notifyID, mNotifyBuilder.build());
    }

}