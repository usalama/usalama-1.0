package com.usalamatechnology.application.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.usalamatechnology.application.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class WalkMapsActivity extends AppCompatActivity implements
        LocationListener,
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";

    public static SharedPreferences credentialsSharedPreferences;
    SharedPreferences.Editor credentialsEditor;


    private String KEY_EMAIL = "email";
    private String KEY_RESPONSE = "response";
    private static final String KEY_REQUEST = "requestMail";
    public static final String savedemail = "email";

    private static final String TAG = "LocationActivity";
    private static final long INTERVAL = 1000 * 60 * 1; //1 minute
    private static final long FASTEST_INTERVAL = 1000 * 60 * 1; // 1 minute


    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;


    String mLastUpdateTime;
    GoogleMap googleMap;
    public static final String LOCATIONPREFERENCES = "LocationPrefs";
    public static SharedPreferences locationSharedPreferences;
    SharedPreferences.Editor locationEditor;

    String requestLatitude;
    String requestLongitude;

    public static final String lastKnownLatitude = "lastLatitude";
    public static final String lastKnownLongitude = "lastLongitude";
    public static final String homeLatitude = "homeLatitude";
    public static final String homeLongitude = "homeLongitude";
    public static final String lastKnownAccuracy = "lastAccuracy";
    private Marker myMarker;
    public static final String savedjson = "json";

    ///////////////////////////////////////////////////////////////////////////
    // Internet connection variables
    String response;
    ConnectionDetector cd;
    // flag for Internet connection status
    private InternetConnection internetConnection;
    Boolean isInternetPresent = false;
    Boolean isInternetActive = false;
    private CameraPosition cameraPosition;
    private Context context;
    private String textContent;
    private String address;
    private float distance;
    private ProgressDialog pDialog;
    // Connection detector class
    public static final String savedresponse = "response";
    public static final String savedEmail = "email";
    public static final String revoked = "revoked";

    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;
    String requestinEmail;
    private Marker myMarkerWalker;

    ///////////////////////////////////////////////////////////////////////////



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate ...............................");
        setContentView(R.layout.activity_walk_maps);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        credentialsSharedPreferences = getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
        credentialsEditor = credentialsSharedPreferences.edit();
        context = this.getApplicationContext();
        String json = credentialsSharedPreferences.getString(savedjson, null);
        String locjson = credentialsSharedPreferences.getString(savedresponse, null);
        String email = credentialsSharedPreferences.getString(savedEmail, null);
        //String[] emailaddress=getResources().getStringArray(getNameEmailDetails());

        String isRevoked = credentialsSharedPreferences.getString(revoked, null);

        if (isRevoked != null && isRevoked.equals("true")) {
            credentialsEditor.putString(revoked, "false");
            credentialsEditor.apply();

            Intent i = new Intent(WalkMapsActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }

        if (json != null) {
            try {
                System.out.println("jssssssssssssssssssooooooooooonnnnnn " + json);
                System.out.println("requestinEmailrequestinEmailrequestinEmail " + requestinEmail);
                JSONObject jsonObj = new JSONObject(json);
                requestinEmail = jsonObj.getString("email");
                new MaterialDialog.Builder(this)
                        .title("Response")
                        .titleColorRes(R.color.colorPrimary)
                        .dividerColorRes(R.color.colorPrimaryDark)
                        .content("Do you accept " + requestinEmail + " request?")
                        .positiveText("Accept")
                        .negativeText("Reject")
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                response = "accept";
                                sendResponse();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                response = "reject";
                                sendResponse();
                            }
                        })
                        .show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        cd = new ConnectionDetector(this.getApplicationContext());
        locationSharedPreferences = getSharedPreferences(LOCATIONPREFERENCES, Context.MODE_PRIVATE);
        locationEditor = locationSharedPreferences.edit();


        //When user presses distress call, first thing is to check for internet
        internetConnection = new InternetConnection();
        internetConnection.execute("");
        //show error dialog if GoolglePlayServices not available
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }



        createMapView();
        //addMarker(16.0f);


        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        if (mToolbar != null) {
            mToolbar.setTitle("");

            setSupportActionBar(mToolbar);
        }
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        findViewById(R.id.directions).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                System.out.println("cccccccccccccccccccccccccccccclicked");

                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                System.out.println("Animate");
                if (googleMap != null && cameraPosition != null) {
                    googleMap.clear();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    addMarker(16.0f);
                }

            }
        });


        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                System.out.println("cccccccccccccccccccccccccccccclicked");
                revokeUserAccess();


            }
        });

        //When user presses distress call, first thing is to check for internet
        internetConnection = new InternetConnection();
        internetConnection.execute("");


        //getNearestUsers();
        if (locjson != null) {
            System.out.println("locjsonlocjsonlocjson " + locjson);

            System.out.println("locjsonlocjsonlocjson " + requestLatitude);
            System.out.println("locjsonlocjsonlocjson " + requestLongitude);

            String[] s = locjson.split("#");
            requestLatitude = s[1];
            requestLongitude = s[2];

            requestinEmail = email;
            addMarker(16.0f);

            if (mTimer != null) {
                mTimer.cancel();
            } else {
                // recreate new
                mTimer = new Timer();
            }
            // schedule task
            mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, 1000 * 60);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                this.googleMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            this.googleMap.setMyLocationEnabled(true);
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


    class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA runnnnnnnnnnnnnnnnnnnnnner===========>");
                    getLocUpdates();
                }

            });
        }
    }


    private void revokeUserAccess() {
        System.out.println("Running..................");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.REVOKE_ACCESS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        System.out.println("************************ Did well" + s);
                        Intent i = new Intent(WalkMapsActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        //System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        //Toast.makeText(CreateProfile.this,"Error creating account",Toast.LENGTH_LONG);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Creating parameters
                Map<String, String> params = new Hashtable<>();
                params.put(KEY_REQUEST, requestinEmail);
                params.put(KEY_EMAIL, credentialsSharedPreferences.getString(savedEmail, null));
                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void getLocUpdates() {
        System.out.println("Running..................");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_USER_LOCATIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        System.out.println("************************ Did well" + s);
                        String[] locs = s.split("#");

                        requestLatitude = locs[0];
                        requestLongitude = locs[1];

                        System.out.println("After getLocUpdates " + requestLatitude);
                        if (googleMap != null) {
                            googleMap.clear();
                        }
                        addMarker(16.0f);
                        //Showing toast message of the response
                        //Toast.makeText(CreateProfile.this,"Successfully created",Toast.LENGTH_LONG);
                        //System.out.println("MAIN response "+s);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        //System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        //Toast.makeText(CreateProfile.this,"Error creating account",Toast.LENGTH_LONG);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Creating parameters
                Map<String, String> params = new Hashtable<>();
                params.put(KEY_REQUEST, requestinEmail);
                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void sendResponse() {
        pDialog = new ProgressDialog(this);
        if (response.equals("reject")) {
            pDialog.setMessage("Sending feedback");
        } else {
            pDialog.setMessage("Retrieving user location and sending feedback");
        }
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.RESPONSE_TO_WALK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        pDialog.dismiss();
                        System.out.println("************************ Did well" + s);
                        if (response.equals("reject")) {
                            Intent i = new Intent(WalkMapsActivity.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            // if(cd.isConnectingToInternet())
                            // {
                            String[] locs = s.split("#");
                            requestLatitude = locs[0];
                            requestLongitude = locs[1];

                            System.out.println("After response " + requestLatitude);
                            System.out.println("After response " + requestLongitude);


                            addMarker(16.0f);

                            getLocUpdates();


                            if (mTimer != null) {
                                mTimer.cancel();
                            } else {
                                // recreate new
                                mTimer = new Timer();
                            }
                            // schedule task
                            mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, 1000 * 60);
                            //   }
                            //    else
                            //    {
                            //       System.out.println("Internet is not active");
                            //   }
                        }
                        //Showing toast message of the response
                        //Toast.makeText(CreateProfile.this,"Successfully created",Toast.LENGTH_LONG);
                        //System.out.println("MAIN response "+s);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        //System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        //Toast.makeText(CreateProfile.this,"Error creating account",Toast.LENGTH_LONG);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                String email = credentialsSharedPreferences.getString(savedemail, null);


                //Creating parameters
                Map<String, String> params = new Hashtable<>();


                params.put(KEY_EMAIL, email);
                params.put(KEY_REQUEST, requestinEmail);
                params.put(KEY_RESPONSE, response);


                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent resultIntent = new Intent(this, MainActivity.class);
        startActivity(resultIntent);
    }


    public Boolean isOnline() {

        try {
            Process p1 = Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }


    private class InternetConnection extends AsyncTask<String, Integer, Boolean> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Boolean doInBackground(String... url) {
            //calls helper method to determine if their is an internet connection
            boolean status = checkForActiveConnection();
            return status;

        }


        @Override
        protected void onPostExecute(Boolean result) {

        }
    }

    private boolean checkForActiveConnection() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            if (isOnline()) {
                isInternetActive = true;
                // Toast.makeText(getActivity(), "You are connected to the internet", Toast.LENGTH_LONG).show();
                return true;
            } else {
                isInternetActive = false;
                return false;
            }
        } else {
            //Toast.makeText(getActivity(), "You are NOT connected to the internet", Toast.LENGTH_LONG).show();
            isInternetActive = false;
            return false;
        }
    }

    /**
     * Initialises the mapview
     */
    private void createMapView() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    private void handleNewLocation(Location location) {

        if (location != null) {
            Log.d(TAG, "Firing onLocationChanged..............................................");
            mCurrentLocation = location;
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            addMarker(16.0f);
        }
    }



    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        googleMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        /*if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }*/

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        //mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
        handleNewLocation(mCurrentLocation);
    }

    class GetAddress extends AsyncTask<String, Date, String> {


        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            getMyLocationAddress(1.3080165,36.8131399);
            return "";
        }

        @Override
        protected void onPostExecute(String msg) {
            //pDialog.setMessage("Calling Upload");

        }

    }

    public void getMyLocationAddress(Double latitude, Double longitude) {

        Geocoder geocoder= new Geocoder(this, Locale.ENGLISH);

        try {

            //Place your latitude and longitude
            List<Address> addresses = geocoder.getFromLocation(latitude,longitude, 1);

            if(addresses != null) {

                Address fetchedAddress = addresses.get(0);
                StringBuilder strAddress = new StringBuilder();

                for(int i=0; i<fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(fetchedAddress.getAddressLine(i)).append("\n");
                }

                address= strAddress.toString();

            }

            else
            {
                address="No location found!";
            }


        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //Toast.makeText(getApplicationContext(),"Could not get address..!", Toast.LENGTH_LONG).show();
        }
    }





    private void addMarker(Float zoom) {


        if (mCurrentLocation != null) {
            if(googleMap!=null) {
                googleMap.clear();
                System.out.println("Was executed");
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), zoom));
            }
            /** Make sure that the map has been initialised **/
            LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            cameraPosition = new CameraPosition.Builder()
                    .target(currentLatLng)      // Sets the center of the map to Mountain View
                    .zoom(zoom)
                    .build();                   //
            if (null != googleMap) {

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                System.out.println("user "+mCurrentLocation.getLongitude());
                myMarker = googleMap.addMarker(new MarkerOptions()
                                .position(currentLatLng)
                                .title("Me")
                                .draggable(true)
                                .snippet("My position")
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.markerfinal))
                );
                myMarker.showInfoWindow();
               // drawCircle(mCurrentLocation.getLongitude(), mCurrentLocation.getLatitude(), mCurrentLocation.getAccuracy());
                if(requestLatitude!=null) {
                    myMarkerWalker = googleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(Double.parseDouble(requestLatitude), Double.parseDouble(requestLongitude)))
                                    .title("Your friend")
                                    .draggable(true)
                                    .snippet(requestinEmail)
                                    .icon(BitmapDescriptorFactory
                                            .fromResource(R.drawable.markerfinal))
                    );
                    myMarkerWalker.showInfoWindow();
                }

            }
        } else if (locationSharedPreferences.getString(lastKnownLatitude, null) != null && locationSharedPreferences.getString(lastKnownLongitude, null) != null && locationSharedPreferences.getString(lastKnownAccuracy, null) != null) {
            Toast.makeText(this, "Usalama has used your last known location.Turn on GPS and WIFI for better accuracy", Toast.LENGTH_LONG).show();
            if(googleMap!=null) {
                googleMap.clear();

                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        Double.parseDouble(locationSharedPreferences.getString(lastKnownLatitude, null)), Double.parseDouble(locationSharedPreferences.getString(lastKnownLongitude, null))), 16.0f));
            }

            /** Make sure that the map has been initialised **/
            LatLng currentLatLng = new LatLng(Double.parseDouble(locationSharedPreferences.getString(lastKnownLatitude, null)), Double.parseDouble(locationSharedPreferences.getString(lastKnownLongitude, null)));

            cameraPosition = new CameraPosition.Builder()
                    .target(currentLatLng)      // Sets the center of the map to Mountain View
                    .zoom(zoom)                   // Sets the zoom// Sets the tilt of the camera to 30 degrees
                    .build();                   //
            if (null != googleMap) {

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                myMarker = googleMap.addMarker(new MarkerOptions()
                                .position(currentLatLng)
                                .title("Me")
                                .draggable(true)
                                .snippet("My position")
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.markerfinal))

                );
                myMarker.showInfoWindow();

                //drawCircle(Double.parseDouble(locationSharedPreferences.getString(lastKnownLongitude, null)), Double.parseDouble(locationSharedPreferences.getString(lastKnownLatitude, null)), Float.parseFloat(locationSharedPreferences.getString(lastKnownAccuracy, null)));
                System.out.println("passed through");

                if(requestLatitude!=null) {
                    myMarkerWalker = googleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(Double.parseDouble(requestLatitude), Double.parseDouble(requestLongitude)))
                                    .title("Your friend")
                                    .draggable(true)
                                    .snippet(requestinEmail)
                                    .icon(BitmapDescriptorFactory
                                            .fromResource(R.drawable.markerfinal))
                    );
                    myMarkerWalker.showInfoWindow();
                }
            }
        }
        else
        {

            LatLng currentLatLng = new LatLng(-1.2833, 36.8167);
            Toast.makeText(this, "Usalama was unable to get your location.Please turn on GPS", Toast.LENGTH_LONG).show();
            cameraPosition = new CameraPosition.Builder()
                    .target(currentLatLng)      // Sets the center of the map to Mountain View
                    .zoom(zoom)                   // Sets the zoom// Sets the tilt of the camera to 30 degrees
                    .build();                   //
            if (null != googleMap) {

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                myMarker = googleMap.addMarker(new MarkerOptions()
                                .position(currentLatLng)
                                .title("Me")
                                .draggable(true)
                                .snippet("My position")
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.markerfinal))

                );
                myMarker.showInfoWindow();

               // drawCircle(36.8167, -1.2833, 50);

                if(requestLatitude!=null)
                {
                    myMarkerWalker = googleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(Double.parseDouble(requestLatitude),Double.parseDouble(requestLongitude)))
                                    .title("Your friend")
                                    .draggable(true)
                                    .snippet(requestinEmail)
                                    .icon(BitmapDescriptorFactory
                                            .fromResource(R.drawable.markerfinal))
                    );
                    myMarkerWalker.showInfoWindow();
                }

            }
        }

    }

}

