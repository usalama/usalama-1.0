package com.usalamatechnology.application.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootCompletedReceiver extends BroadcastReceiver {
    public BootCompletedReceiver() {
    }

    final static String TAG = "BootCompletedReceiver";

    @Override
    public void onReceive(Context context, Intent arg1) {
        Log.w(TAG, "starting service...");
        context.startService(new Intent(context, MyService.class));
        context.startService(new Intent(context, MyLocService.class));
        context.startService(new Intent(context, MyTimerService.class));
    }
}
