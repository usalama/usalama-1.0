package com.usalamatechnology.application.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.usalamatechnology.application.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class PickGroupActivity extends AppCompatActivity {

    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";
    public static SharedPreferences credentialsSharedPreferences;
    SharedPreferences.Editor credentialsEditor;

    public static final String savedname = "name";

    public static final String savedid = "id";
    public static final String savedemail = "email";
    public static final String savedphone = "phonenumber";
    private TextView userName;
    private EditText houseNo;


    String  KEY_GET_GROUPS="GETGROUPS";

    String  KEY_GROUPNAME="group_name";
    String  KEY_HOUSENO="house_no";
    String  KEY_USEREMAIL="user_email";
    private MaterialDialog pd; //Varaible for all material dialogs

    private String[] firstName;
    List<String> spinnerArray;
    ArrayAdapter<String> adapter;
    Spinner sItems;
    public static final String isFirstGroup = "isFirstGroup";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_group);

        credentialsSharedPreferences = getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
        credentialsEditor = credentialsSharedPreferences.edit();


        userName = (TextView) findViewById(R.id.textView22);
        houseNo= (EditText) findViewById(R.id.editText);


        String theName=credentialsSharedPreferences.getString(savedname, "Not Available");
        firstName=theName.split(" ");
if(firstName[0]!="Not")
{
    userName.setText(firstName[0]+" :)");
}
        else
{
    userName.setText("there "+" :)");
}

        getAllGroups();


        findViewById(R.id.add_contact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sItems.getSelectedItem().toString()=="1. choose your estate ")
                {
                    Toast.makeText(PickGroupActivity.this,"Please select a valid estate", Toast.LENGTH_LONG).show();
                }
                else
                {
                    updateUserDetails();
                    System.out.println("clicked");
                }



            }
        });

    }

    private void updateUserDetails(){


            pd = new MaterialDialog.Builder(this)
                    .title("Registration")
                    .titleColorRes(R.color.colorPrimary)
                    .dividerColorRes(R.color.colorPrimaryDark)
                    .content("Registering user")
                    .progress(true, 0)
                    .show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.updateUserDetails,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        if(pd!=null)
                        {
                            pd.dismiss();
                        }

                        credentialsEditor.putString(isFirstGroup, "true");
                        credentialsEditor.apply();

                        credentialsEditor.putString(KEY_GROUPNAME, sItems.getSelectedItem().toString());
                        credentialsEditor.apply();

                        credentialsEditor.putString(KEY_HOUSENO, houseNo.getText().toString());
                        credentialsEditor.apply();

                        Intent intent = new Intent(PickGroupActivity.this, MainActivity.class);
                        startActivity(intent);

                        Toast.makeText(PickGroupActivity.this,"Successfully registered!", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {



                //Creating parameters
                Map<String,String> params = new Hashtable<>();

                //Adding parameters
                params.put(KEY_HOUSENO, houseNo.getText().toString());
                params.put(KEY_GROUPNAME, sItems.getSelectedItem().toString());
                params.put(KEY_USEREMAIL, credentialsSharedPreferences.getString(savedemail, "Not Available"));

                System.out.println("1####HOUSE_NO "+houseNo.getText().toString());
                System.out.println("1####KEY_GROUPNAME "+sItems.getSelectedItem().toString());
                System.out.println("1####KEY_USEREMAIL "+credentialsSharedPreferences.getString(savedemail, "Not Available"));

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void getAllGroups(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_ALL_GROUPS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        System.out.println("************************ Did well"+s);

                        spinnerArray =  new ArrayList<String>();
                        spinnerArray.add("1. choose your estate ");

                        JSONArray array = null;
                        try {
                            JSONObject jsonObj = new JSONObject(s);

                            JSONArray posts = jsonObj.getJSONArray("usalama_groups");

                            for (int i = 0; i < posts.length(); i++) {
                                JSONObject row = posts.getJSONObject(i);

                                String group_id = row.getString("group_id");
                                String name = row.getString("name");
                                String location = row.getString("location");
                                String status = row.getString("status");

                                spinnerArray.add(name+", "+location);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        adapter = new ArrayAdapter<String>(
                                PickGroupActivity.this, R.layout.spinner_item, spinnerArray);

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sItems = (Spinner) findViewById(R.id.spinner);
                        sItems.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {



                //Creating parameters
                Map<String,String> params = new Hashtable<>();

                //Adding parameters
                params.put(KEY_GET_GROUPS, "groups");

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }
}
