package com.usalamatechnology.application.activity;

/**
 * Created by Sir Edwin on 2/7/2016.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.usalamatechnology.application.R;

public class MyReceiver extends BroadcastReceiver{

    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context=context;
        System.out.println("broadcast: "+intent.getAction());
        Notification();
    }

    public void Notification() {
        // Set Notification Title
        String strtitle = "Usalama";
        // Set Notification Text
        String strtext = "Usalama has send an automated distress call from the timer,Do you wish to stop it?";

        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, MainTimerActivity.class);
        // Send data to NotificationView Class
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //Create Notification using NotificationCompat.Builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.drawable.notification)
                        // Set Ticker Message
                .setTicker("Usalama")
                        // Set Title
                .setContentTitle(strtitle)
                        // Set Text
                .setContentText(strtext)
                        // Add an Action Button below Notification
                .addAction(R.drawable.ic_pause, "Stop", pIntent)
                        // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                        // Dismiss Notification
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());

    }

}
