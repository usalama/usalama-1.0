package com.usalamatechnology.application.activity;

/**
 * Created by Sir Edwin on 2/7/2016.
 */

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

//import com.google.android.gms.common.GooglePlayServicesUtil;

public class MyLocService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    public static final String USERLOCATIONPREFERENCES = "userlocationPrefs";
    public static SharedPreferences userLocationSharedPreferences;
    SharedPreferences.Editor userLocationEditor;
    private static final String TAG = "LocationService";

    public static final String userLatitude = "userLatitude";
    public static final String userLongitude = "userLongitude";
    public static final String userAccuracy = "userAccuracy";
    public static final String userArea = "area";

    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";
    public static SharedPreferences credentialsSharedPreferences;
    SharedPreferences.Editor credentialsEditor;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    public static final String savedemail = "email";

    String  KEY_CHOICE="choice";
    private String KEY_LATITUDE = "latitude";
    private String KEY_LONGITUDE = "longitude";
    private String KEY_EMAIL = "email";
    private String KEY_AREA = "area";
    String  KEY_GROUPNAME="group_name";
    String  KEY_HOUSENO="house_no";
    private String groupName,houseName;

    // use the websmithing defaultUploadWebsite for testing and then check your
    // location with your browser here: https://www.websmithing.com/gpstracker/displaymap.php
    private String defaultUploadWebsite;

    private boolean currentlyProcessingLocation = false;
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private float distance;
    private String stateName="not available";

    @Override
    public void onCreate() {
        super.onCreate();

        userLocationSharedPreferences = getApplicationContext().getSharedPreferences(USERLOCATIONPREFERENCES, Context.MODE_PRIVATE);
        userLocationEditor = userLocationSharedPreferences.edit();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission(getApplicationContext());
        }

        credentialsSharedPreferences = getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
        credentialsEditor = credentialsSharedPreferences.edit();
        groupName = credentialsSharedPreferences.getString(KEY_GROUPNAME, null);
        houseName = credentialsSharedPreferences.getString(KEY_HOUSENO, null);

        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%% from oncreate");
        latitude = userLocationSharedPreferences.getString(userLatitude, null);
        longitude = userLocationSharedPreferences.getString(userLongitude, null);
        email = credentialsSharedPreferences.getString(savedemail, null);
        if (email != null && latitude != null) {
            uploadLocationDetails();
        }
    }


    public static boolean checkLocationPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // if we are currently trying to get a location and the alarm manager has called this again,
        // no need to start processing a new location.
        if (!currentlyProcessingLocation) {
            currentlyProcessingLocation = true;
            startTracking();
        }

        return START_NOT_STICKY;
    }

    private void startTracking() {
        Log.d(TAG, "startTracking");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(checkLocationPermission(getApplicationContext()))
            {
                if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {

                    googleApiClient = new GoogleApiClient.Builder(this)
                            .addApi(LocationServices.API)
                            .addConnectionCallbacks(this)
                            .addOnConnectionFailedListener(this)
                            .build();

                    if (!googleApiClient.isConnected() || !googleApiClient.isConnecting()) {
                        googleApiClient.connect();
                    }
                } else {
                    Log.e(TAG, "unable to connect to google play services.");
                }
            }
        }
        else
        {
            if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {

                googleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build();

                if (!googleApiClient.isConnected() || !googleApiClient.isConnecting()) {
                    googleApiClient.connect();
                }
            } else {
                Log.e(TAG, "unable to connect to google play services.");
            }
        }


    }



    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {


            Double userCurrentLat = Double.valueOf(userLocationSharedPreferences.getString(userLatitude, "0.0"));
            Double userCurrentLong = Double.valueOf(userLocationSharedPreferences.getString(userLongitude, "0.0"));
            float prevAccuracy = Float.valueOf(userLocationSharedPreferences.getString(userAccuracy, "0.0"));

            System.out.println("------------------------- userCurrentLong" + userCurrentLong);

            Double homeCurrentLat = location.getLatitude();
            Double homeCurrentLong = location.getLongitude();

            Location locationA = new Location("point A");
            locationA.setLatitude(userCurrentLat);
            locationA.setLongitude(userCurrentLong);
            Location locationB = new Location("point B");
            locationB.setLatitude(homeCurrentLat);
            locationB.setLongitude(homeCurrentLong);
            distance = locationA.distanceTo(locationB);
            float curAccuracy = location.getAccuracy();

            //Log.e(TAG, "position: " + location.getLatitude() + ", " + location.getLongitude() + " accuracy: " + location.getAccuracy());
            //System.out.println("===============>A change noted: " + location.getLatitude() + ", " + location.getLongitude() + " accuracy: " + location.getAccuracy());
            System.out.println("===============> distance " + distance + "=======> prevAcc" + prevAccuracy + "=======>curAcc " + curAccuracy);
            userLocationEditor.putString(userLatitude, String.valueOf(location.getLatitude()));
            userLocationEditor.putString(userLongitude, String.valueOf(location.getLongitude()));
            userLocationEditor.putString(userAccuracy, String.valueOf(location.getAccuracy()));
            userLocationEditor.apply();
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
            email = credentialsSharedPreferences.getString(savedemail, null);

            if (email != null && distance > 10 && curAccuracy < prevAccuracy) {
                uploadLocationDetails();
            } else if (userCurrentLong == 0) {
                uploadLocationDetails();
            }
if(credentialsSharedPreferences.getString(KEY_CHOICE, "Not Available")=="Estate")
{
    checkUserStatus();
}
else if(credentialsSharedPreferences.getString(KEY_CHOICE, "Not Available")=="Personal")
{
    checkUserStatus2();

}

            System.out.println("===============> position: " + location.getLatitude() + ", " + location.getLongitude() + " accuracy: " + location.getAccuracy());
            // we have our desired accuracy of 500 meters so lets quit this service,
            // onDestroy will be called and stop our location uodates
            // if (location.getAccuracy() < 500.0f) {
            //      stopLocationUpdates();
            //       sendLocationDataToWebsite(location);
            //  }
        }
    }

    String latitude;
    String longitude;
    String email;

    private void checkUserStatus2() {
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%% we are uploading");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.CHECK_USER3,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        System.out.println("STTTTTTTTTTTTTTTTTTTTTTATUS " + s);
                        if(s.contains("1"))
                        {
                            Intent intent = new Intent(getApplicationContext(), UnpaidActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        System.out.println("volleyError response " + volleyError.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<>();
                params.put(KEY_GROUPNAME, groupName);
                params.put(KEY_HOUSENO, houseName);
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //Adding request to the queue
        requestQueue.add(stringRequest);
    }


    private void checkUserStatus() {
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%% we are uploading");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.CHECK_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        System.out.println("STTTTTTTTTTTTTTTTTTTTTTATUS " + s);
                        if(s.contains("0"))
                        {
                            Intent intent = new Intent(getApplicationContext(), UnpaidActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        System.out.println("volleyError response " + volleyError.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<>();
                params.put(KEY_GROUPNAME, groupName);
                params.put(KEY_HOUSENO, houseName);
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void uploadLocationDetails() {
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%% we are uploading");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.USER_LOCATION_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        System.out.println("UPLOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADED " + s);

                        //Disimissing the progress dialog
                        //loading.dismiss();
                        //Showing toast message of the response
                        //Toast.makeText(CreateProfile.this,"Successfully created",Toast.LENGTH_LONG);
                        //System.out.println("MAIN response "+s);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        //Toast.makeText(CreateProfile.this,"Error creating account",Toast.LENGTH_LONG);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String


                Double userCurrentLat = Double.valueOf(latitude);
                Double userCurrentLong = Double.valueOf(longitude);

                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocation(userCurrentLat, userCurrentLong, 1);
                    String cityName = addresses.get(0).getAddressLine(0);
                    stateName = addresses.get(0).getAddressLine(1);
                    String countryName = addresses.get(0).getAddressLine(2);

                    System.out.println("I am this cityyyyyyyyyyyyyyyyyyyyy "+cityName+stateName+countryName);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                userLocationEditor.putString(userArea, stateName);
                userLocationEditor.apply();

                //Creating parameters
                Map<String, String> params = new Hashtable<>();

                //Adding parameters
                params.put(KEY_LONGITUDE, longitude);
                params.put(KEY_LATITUDE, latitude);
                params.put(KEY_EMAIL, email);
                params.put(KEY_AREA, stateName);


                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void stopLocationUpdates() {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    /**
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected");

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(1000); // milliseconds
        locationRequest.setFastestInterval(10000); // the fastest rate in milliseconds at which your app can handle location updates
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed");

        stopLocationUpdates();
        stopSelf();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "GoogleApiClient connection has been suspend");
    }
}
