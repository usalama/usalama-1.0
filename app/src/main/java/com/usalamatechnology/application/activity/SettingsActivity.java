package com.usalamatechnology.application.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.usalamatechnology.application.R;

import java.util.Hashtable;
import java.util.Map;

public class SettingsActivity extends AppCompatActivity {

    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";
    public static SharedPreferences credentialsSharedPreferences;
    SharedPreferences.Editor credentialsEditor;


    public static final String savedphone = "phonenumber";
    public static final String savedemail = "email";

    private Toolbar mToolbar;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String robberMessages = "robber";
    public static final String burglarMessages = "burglar";
    public static final String muggerMessages = "mugger";
    public static final String carjackerMessages = "carjacker";
    public static final String openOnShake = "openOnShake";
    public static final String sendOnVolume = "sendOnVolume";
    public static SharedPreferences sharedpreferences;
    public static final String broadcast_group = "broadcast_group";
    public static final String broadcast_public = "broadcast_public";
    public static final String broadcast_locale = "broadcast_locale";
    TextView robber;
    TextView burglar;
    TextView mugger;
    TextView carjacker;
    CheckBox group;
    CheckBox allUsers;
    CheckBox locality;
    CheckBox shake;
    CheckBox send;
    TextView number;
    TextView tap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        robber = (TextView)findViewById(R.id.armed_robber_message);
        burglar = (TextView)findViewById(R.id.burglar_message);
        mugger = (TextView)findViewById(R.id.mugger_message);
        carjacker = (TextView)findViewById(R.id.car_jacker_message);
        shake=(CheckBox)findViewById(R.id.radioButton);
        send=(CheckBox)findViewById(R.id.radioButton2);

        group=(CheckBox)findViewById(R.id.radioButton321);
        allUsers=(CheckBox)findViewById(R.id.radioButton3321);
        locality=(CheckBox)findViewById(R.id.radioButton32);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        credentialsSharedPreferences = getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
        credentialsEditor = credentialsSharedPreferences.edit();

        number= (TextView)findViewById(R.id.textView101);
        number .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(SettingsActivity.this);
                View promptsView = li.inflate(R.layout.promts2, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        SettingsActivity.this);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.editTextDialogUserInput);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to result
                                        // edit text
                                        number.setText(userInput.getText());
                                        credentialsEditor.putString(savedphone, userInput.getText().toString());
                                        credentialsEditor.apply();
                                        updatePhoneNumber();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });

        tap= (TextView)findViewById(R.id.textView20);
        tap .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(SettingsActivity.this);
                View promptsView = li.inflate(R.layout.promts2, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        SettingsActivity.this);



                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.editTextDialogUserInput);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to result
                                        // edit text
                                        number.setText(userInput.getText());
                                        credentialsEditor.putString(savedphone, userInput.getText().toString());
                                        credentialsEditor.apply();
                                        updatePhoneNumber();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });

        System.out.println("--->" + credentialsSharedPreferences.getString(savedphone, "Not Available"));
        number.setText(credentialsSharedPreferences.getString(savedphone, "Not Available"));
        robber.setText(sharedpreferences.getString(robberMessages, null));
        burglar.setText(sharedpreferences.getString(burglarMessages, null));
        mugger.setText(sharedpreferences.getString(muggerMessages, null));
        carjacker.setText(sharedpreferences.getString(carjackerMessages, null));

        if(sharedpreferences.getString(openOnShake,"no" ).equals("yes"))
        {
            shake.setChecked(true);
        }
        else
        {
            shake.setChecked(false);
        }

        if(sharedpreferences.getString(sendOnVolume,"no" ).equals("yes"))
        {
            send.setChecked(true);
        }
        else
        {
            send.setChecked(false);
        }

        if(sharedpreferences.getString(broadcast_group,"no" ).equals("yes"))
        {
            group.setChecked(true);
        }
        else
        {
            group.setChecked(false);
        }

        if(sharedpreferences.getString(broadcast_locale,"no" ).equals("yes"))
        {
            locality.setChecked(true);
        }
        else
        {
            locality.setChecked(false);
        }

        if(sharedpreferences.getString(broadcast_public,"no" ).equals("yes"))
        {
            allUsers.setChecked(true);
        }
        else
        {
            allUsers.setChecked(false);
        }



        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ "+sharedpreferences.getString(openOnShake,null ));
        shake.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(openOnShake, "yes");
                    editor.apply();
                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ " + sharedpreferences.getString(openOnShake, null));
                }
                else
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(openOnShake, "no");
                    editor.apply();
                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ " + sharedpreferences.getString(openOnShake, null));
                }
            }
        });

        allUsers.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(broadcast_public, "yes");
                    editor.apply();
                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ " + sharedpreferences.getString(broadcast_public, null));
                    if(locality.isChecked())
                    {
                        locality.setChecked(false);
                        SharedPreferences.Editor editor2 = sharedpreferences.edit();
                        editor.putString(broadcast_locale, "no");
                        editor.apply();
                    }
                    if(group.isChecked())
                    {
                        group.setChecked(false);
                        SharedPreferences.Editor editor3 = sharedpreferences.edit();
                        editor.putString(broadcast_group, "no");
                        editor.apply();
                    }
                }
                else
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(broadcast_public, "no");
                    editor.apply();
                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ " + sharedpreferences.getString(broadcast_public, null));


                }
            }
        });

        locality.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(broadcast_locale, "yes");
                    editor.apply();
                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ " + sharedpreferences.getString(broadcast_locale, null));

                    if(allUsers.isChecked())
                    {
                        allUsers.setChecked(false);
                        SharedPreferences.Editor editor2 = sharedpreferences.edit();
                        editor.putString(broadcast_public, "no");
                        editor.apply();
                    }
                    if(group.isChecked())
                    {
                        group.setChecked(false);
                        SharedPreferences.Editor editor3 = sharedpreferences.edit();
                        editor.putString(broadcast_group, "no");
                        editor.apply();
                    }
                }
                else
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(broadcast_locale, "no");
                    editor.apply();
                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ " + sharedpreferences.getString(broadcast_locale, null));


                }
            }
        });

        group.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(broadcast_group, "yes");
                    editor.apply();
                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ " + sharedpreferences.getString(broadcast_group, null));

                    if(allUsers.isChecked())
                    {
                        allUsers.setChecked(false);
                        SharedPreferences.Editor editor2 = sharedpreferences.edit();
                        editor.putString(broadcast_public, "no");
                        editor.apply();
                    }
                    if(locality.isChecked())
                    {
                        locality.setChecked(false);
                        SharedPreferences.Editor editor3 = sharedpreferences.edit();
                        editor.putString(broadcast_locale, "no");
                        editor.apply();
                    }
                }
                else
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(broadcast_group, "no");
                    editor.apply();
                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ " + sharedpreferences.getString(broadcast_group, null));

                }
            }
        });

        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ "+sharedpreferences.getString(sendOnVolume,null ));
        send.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(sendOnVolume, "yes");
                    editor.apply();
                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ " + sharedpreferences.getString(sendOnVolume, null));
                }
                else
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(sendOnVolume, "no");
                    editor.apply();
                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$ " + sharedpreferences.getString(sendOnVolume, null));
                }
            }
        });


        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if(mToolbar!=null) {
            mToolbar.setTitle("Settings");

            setSupportActionBar(mToolbar);
        }
        if(getSupportActionBar()!=null)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void updatePhoneNumber(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_MOBILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        System.out.println("************************ Did well" + s);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        //Toast.makeText(CreateProfile.this,"Error creating account",Toast.LENGTH_LONG);
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                //Creating parameters
                Map<String,String> params = new Hashtable<>();

                //Adding parameters
                params.put("email", credentialsSharedPreferences.getString(savedemail, null));
                params.put("phone", credentialsSharedPreferences.getString(savedphone, null));
                //System.out.println("Email id = " + email + " Reg Id = " + regId);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
