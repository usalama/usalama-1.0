package com.usalamatechnology.application.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.usalamatechnology.application.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.RequestParams;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener{


    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";
    public static SharedPreferences credentialsSharedPreferences;
    SharedPreferences.Editor credentialsEditor;


    public static final String savedname = "name";

    public static final String savedid = "id";
    public static final String savedemail = "email";
    public static final String savedphone = "phonenumber";
    public static final String picurl = "picurl";
    public static final String isFirstTime = "isFirstTime";
    public static final String isFirstGroup = "isFirstGroup";

    RequestParams params = new RequestParams();
    GoogleCloudMessaging gcmObj;
    Context applicationContext;
    String regId = "";


    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    AsyncTask<Void, Void, String> createRegIdTask;

    public static final String REG_ID = "regId";
    public static final String EMAIL_ID = "eMailId";

    private static final int RC_SIGN_IN = 9001;
    // Logcat tag
    private static final String TAG = "MainActivity";

    private String KEY_IMAGE_PATH = "image_path";
    private String KEY_NAME = "name";
    private String KEY_EMAIL = "email";
    private String KEY_PASSWORD = "password";
    private String KEY_PHONE = "phone";

    // Profile pic image size in pixels
    private static final int PROFILE_PIC_SIZE = 400;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    private boolean mSignInClicked;

    private ConnectionResult mConnectionResult;

    private SignInButton btnSignIn;


    private boolean mIntentInProgress;
    //Defining views
    private EditText editTextPhone;

    private ProgressDialog pDialog;
    private TextView why;


    //boolean variable to check user is logged in or not
    //initially it is false
    private boolean loggedIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent(LoginActivity.this, Marshmallow.class);
            startActivity(intent);
        }

        credentialsSharedPreferences = getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
        credentialsEditor = credentialsSharedPreferences.edit();


        applicationContext = getApplicationContext();

        why=(TextView)findViewById(R.id.linkSignup);

        why.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(LoginActivity.this)
                        .title("Info")
                        .titleColorRes(R.color.colorPrimary)
                        .dividerColorRes(R.color.colorPrimaryDark)
                        .content("Usalama needs your valid phone number so that you may send and receive notifications.")
                        .positiveText("GOT IT")
                        .show();
            }
        });

        //Initializing views
        editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        editTextPhone=(EditText)findViewById(R.id.editTextPhone);
        editTextPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Validation.hasText(editTextPhone);
                Validation.isPhonenumber(editTextPhone);
            }
        });

        btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);


        // Button click listeners
        btnSignIn.setOnClickListener(this);
        //btnSignOut.setOnClickListener(this);
        //btnRevokeAccess.setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }



    private void uploadLogInDetails(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.CREATE_PROFILE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        System.out.println("#####################12"+s);
                        credentialsEditor.putString(isFirstTime, "true");
                        credentialsEditor.apply();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = imagePath;

                //Getting Image Name
                String name = username;
                String email = userEmail;
                String password = userPassword;
                String phone = editTextPhone.getText().toString();

                credentialsEditor.putString(savedphone, phone);
                credentialsEditor.apply();

                //Creating parameters
                Map<String,String> params = new Hashtable<>();

                //Adding parameters
                params.put(KEY_IMAGE_PATH, image);
                params.put(KEY_NAME, name);
                params.put(KEY_EMAIL, email);
                params.put(KEY_PASSWORD, password);
                params.put(KEY_PHONE, phone);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }




    @Override
    public void onConnectionFailed(ConnectionResult result) {


    }
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    GoogleSignInAccount acct;

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            isSignedIn=true;
             acct = result.getSignInAccount();
            getProfileInformation(acct);
            System.out.println("2----------------> "+credentialsSharedPreferences.getString(isFirstGroup,null));
            if(credentialsSharedPreferences.getString(isFirstGroup,null) !="true")
            {
                Intent intent = new Intent(LoginActivity.this, ChooseSide.class);
                startActivity(intent);
            }


        } else {
            // Signed out, show unauthenticated UI.
            // updateUI(false);
        }
    }



    boolean isSignedIn=false;

    String username;
    String userEmail;
    String userPassword;
    String imagePath;

    /**
     * Fetching user's information name, email, profile pic
     * */
    private void getProfileInformation(GoogleSignInAccount acct) {
        try {
                String personName = acct.getDisplayName();
                String personPhotoUrl;
           if(acct.getPhotoUrl()!= null)
            {
                personPhotoUrl = acct.getPhotoUrl().toString();
            }
            else
            {
                personPhotoUrl="http://easypluslife.com/Usalama_project/ic_unknown_user.png";
            }
                String email = acct.getEmail();
                String id="";

                SharedPreferences sharedPreferences = LoginActivity.this.getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);

                //Creating editor to store values to shared preferences
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, true);
                editor.putString(Config.EMAIL_SHARED_PREF, email);
                editor.putString(Config.IMAGE_SHARED_PREF, personPhotoUrl);

                editor.apply();

                credentialsEditor.putString(savedname, personName);
                credentialsEditor.putString(savedemail, email);
                credentialsEditor.putString(savedid, id);
                credentialsEditor.putString(picurl, personPhotoUrl);
                credentialsEditor.putString(isFirstTime, "true");

                credentialsEditor.apply();

                username= personName;
                userEmail=email;
                userPassword="12345678";
                imagePath=personPhotoUrl;

                Log.e(TAG, "Name: " + personName + ", plusProfile: "
                        + userPassword + ", email: " + email
                        + ", Image: " + personPhotoUrl);
                if (personName!=null && credentialsSharedPreferences.getString(isFirstTime,null) =="true")
                {
                    System.out.println("########################### should upload");
                    uploadLogInDetails();
                    registerInBackground(email);
                } else
                {
                    System.out.println("^^^^^^^^^^^^^^^^^^^^^^^ not uploading");
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Button on click listener
     * */


    // AsyncTask to register Device in GCM Server
    private void registerInBackground(final String emailID) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcmObj == null) {
                        gcmObj = GoogleCloudMessaging
                                .getInstance(applicationContext);
                    }
                    regId = gcmObj
                            .register(ApplicationConstants.GOOGLE_PROJ_ID);
                    msg = "Registration ID :" + regId;

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!TextUtils.isEmpty(regId)) {
                    storeRegIdinSharedPref(applicationContext, regId, emailID);

                } else {

                }
            }
        }.execute(null, null, null);
    }

    // Store RegId and Email entered by User in SharedPref
    private void storeRegIdinSharedPref(Context context, String regId,
                                        String emailID) {
        SharedPreferences prefs = getSharedPreferences("UserDetails",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(REG_ID, regId);
        editor.putString(EMAIL_ID, emailID);
        editor.commit();
        storeRegIdinServer();
    }

    private void storeRegIdinServer(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApplicationConstants.APP_SERVER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        System.out.println("************************ Did well" + s);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        //Toast.makeText(CreateProfile.this,"Error creating account",Toast.LENGTH_LONG);
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                //Creating parameters
                Map<String,String> params = new Hashtable<>();

                //Adding parameters
                params.put("emailId", userEmail);
                params.put("regId", regId);
                System.out.println("Email id = " + userEmail + " Reg Id = " + regId);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void updatePhoneNumber(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApplicationConstants.APP_SERVER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        System.out.println("************************ Did well" + s);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        //Toast.makeText(CreateProfile.this,"Error creating account",Toast.LENGTH_LONG);
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                //Creating parameters
                Map<String,String> params = new Hashtable<>();

                //Adding parameters
                params.put("emailId", userEmail);
                params.put("regId", regId);
                System.out.println("Email id = " + userEmail + " Reg Id = " + regId);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }





    @Override
    protected void onResume() {
        super.onResume();
        //In onresume fetching value from sharedpreference
        SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);

        //Fetching the boolean value form sharedpreferences
        loggedIn = sharedPreferences.getBoolean(Config.LOGGEDIN_SHARED_PREF, false);

        //If we will get true
        if(loggedIn){
            //We will start the Profile Activity
            System.out.println("Here----------------> "+credentialsSharedPreferences.getString(isFirstGroup,null));
            if(credentialsSharedPreferences.getString(isFirstGroup, "").equals("true"))
            {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
            else
            {
                Intent intent = new Intent(LoginActivity.this, ChooseSide.class);
                startActivity(intent);
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_in:
                // Signin button clicked
                signIn();
                break;
        }
        //Calling the login function
    }
}
