package com.usalamatechnology.application.activity;

public interface AccelerometerListener {
	 
	 void onAccelerationChanged(float x, float y, float z);
 
	 void onShake(float force);
 
}