package com.usalamatechnology.application.activity;

/**
 * Created by Sir Edwin on 7/17/2015.
 */

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.usalamatechnology.application.R;
import com.usalamatechnology.application.adapter.DistressAdapter;
import com.usalamatechnology.application.adapter.IObserver;
import com.usalamatechnology.application.model.DatabaseHelper;
import com.usalamatechnology.application.model.DistressCall;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp1 on 21-01-2015.
 */
public class DistressFragment extends Fragment implements IObserver {
    private Toolbar mToolbar;
    public  List<DistressCall> distressCalls;
    private RecyclerView rv;
    private DatabaseHelper databaseHelper;

    public static FragmentActivity host ;
    public static int positionOfCard=0;
    DistressAdapter adapter;
    private Cursor mCursor;
    private Cursor mRecord;

    private String selectedId;
    private String senderNum;
    private String message = "";
    private String name;
    private String lon;
    private String lat;
    private String policeOneLong;
    private String policeOneLat;
    private String policeTwoLong;
    private String policeTwoLat;
    private String policeThreeLong;
    private String policeThreeLat;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.distress_fragment,container,false);
        host=getActivity();
        databaseHelper = new DatabaseHelper(getActivity());
        rv = (RecyclerView) v.findViewById(R.id.rv);
        TextView emptyView = (TextView) v.findViewById(R.id.empty_view);
        databaseHelper = new DatabaseHelper(getActivity());

        if (databaseHelper.isDbEmpty()) {
            rv.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
        else {
            rv.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());

            rv.setLayoutManager(llm);
            rv.setHasFixedSize(true);
            registerForContextMenu(rv);

            initializeAdapter();
            initializeData();


        }
        TextView vw = (TextView)v.findViewById(R.id.date);


        return v;
    }

    private void initializeAdapter() {
        distressCalls = new ArrayList<>();
        mCursor= databaseHelper.getAllRecords();

        for(mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
            System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+mCursor.getString(4));
            distressCalls.add(new DistressCall(mCursor.getString(0),mCursor.getString(4),mCursor.getString(1),mCursor.getString(2),
                    R.drawable.logo));
        }

    }

    private void initializeData() {
        System.out.println(" ********************* " + distressCalls.size());
        adapter = new DistressAdapter(distressCalls);
        rv.setAdapter(adapter);
        adapter.setListener(this);
    }

    @Override
    public void onItemClicked(int id) {
        System.out.println("id "+id);
        mRecord= databaseHelper.getRecord(String.valueOf(id));
        for(mRecord.moveToFirst(); !mRecord.isAfterLast(); mRecord.moveToNext()) {
            System.out.println("repeat "+id);
            message=mRecord.getString(1);
            senderNum = mRecord.getString(6)+"'"+mRecord.getString(5); //added this to prevent disrupting people who
            // had already installed app
            name = mRecord.getString(4);
            lon = mRecord.getString(7);
            lat = mRecord.getString(8);

            policeOneLong = mRecord.getString(9);
            policeOneLat = mRecord.getString(10);

            policeTwoLong = mRecord.getString(11);
            policeTwoLat = mRecord.getString(12);

            policeThreeLong = mRecord.getString(13);
            policeThreeLat = mRecord.getString(14);

            Intent resultIntent = new Intent(getActivity(), MapsActivity.class);
            resultIntent.putExtra("Message", message);
            resultIntent.putExtra("Name", name);
            resultIntent.putExtra("Number", senderNum);
            resultIntent.putExtra("long", lon);
            resultIntent.putExtra("lat", lat);
            resultIntent.putExtra("dontsend", "dontsend");
            resultIntent.putExtra("policeOneLong", policeOneLong);
            resultIntent.putExtra("policeOneLat", policeOneLat);
            resultIntent.putExtra("policeTwoLong", policeTwoLong);
            resultIntent.putExtra("policeTwoLat", policeTwoLat);
            resultIntent.putExtra("policeThreeLong", policeThreeLong);
            resultIntent.putExtra("policeThreeLat", policeThreeLat);
            resultIntent.putExtra("send", "send");

            System.out.println("message" + message);
            System.out.println("lon" + lon);
            System.out.println("lat" + lat);
            System.out.println("Number" + senderNum);
            System.out.println("policeOneLong" + policeOneLong);
            System.out.println("policeOneLat" + policeOneLat);
            System.out.println("policeTwoLong" + policeTwoLong);
            System.out.println("policeTwoLat" + policeTwoLat);
            System.out.println("policeThreeLong" + policeThreeLong);
            System.out.println("policeThreeLat" + policeThreeLat);

            startActivity(resultIntent);

        }
    }

    @Override
    public void onItemLongClicked(int id) {

    }

    @Override
    public void onTextClicked(int pos) {

    }
}