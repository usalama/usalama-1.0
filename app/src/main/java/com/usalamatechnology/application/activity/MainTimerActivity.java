/*
 * Copyright (C) 2015 Brent Marriott
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.usalamatechnology.application.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.usalamatechnology.application.R;

public class MainTimerActivity extends AppCompatActivity {
    public static final String LOCATIONPREFERENCES = "LocationPrefs";
    public static SharedPreferences locationSharedPreferences;
    SharedPreferences.Editor locationEditor;


    public static final String homeLatitude = "homeLatitude";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationSharedPreferences = getSharedPreferences(LOCATIONPREFERENCES, Context.MODE_PRIVATE);
        locationEditor = locationSharedPreferences.edit();
        setContentView(R.layout.activity_deco_view_sample);





        if(locationSharedPreferences.getString(homeLatitude, null)==null)
        {
            finish();
            Intent resultIntent = new Intent(MainTimerActivity.this, TimerActivity.class);
            startActivity(resultIntent);
        }

        TimerAdapter samplesAdapter = new TimerAdapter(getSupportFragmentManager());
        ViewPager samplesPager = (ViewPager) findViewById(R.id.samplesPager);
        samplesPager.setAdapter(samplesAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent resultIntent = new Intent(this, MainActivity.class);
        startActivity(resultIntent);
    }
}
