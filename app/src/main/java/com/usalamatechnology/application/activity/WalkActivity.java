package com.usalamatechnology.application.activity;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.usalamatechnology.application.R;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class WalkActivity extends AppCompatActivity {

    private String KEY_EMAIL = "email";
    private String MY_EMAIL = "myemail";
    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";
    public static SharedPreferences credentialsSharedPreferences;
    private ProgressDialog pDialog;

    public static final String savedemail = "email";
    public static final String savedjson = "json";

    ImageButton send;
    LinearLayout sendLayout;
    String selectedEmail;
    EditText userEmail;

    public static final String savedresponse = "response";
    public static final String savedEmail = "email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walk);

        credentialsSharedPreferences = getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor credentialsEditor = credentialsSharedPreferences.edit();

        credentialsEditor.putString(savedjson,null);
        credentialsEditor.putString(savedresponse,null);
        credentialsEditor.apply();

        final AutoCompleteTextView textView=(AutoCompleteTextView) findViewById(R.id.emailEditText);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getNameEmailDetails());
        textView.setAdapter(adapter);



        send=(ImageButton)findViewById(R.id.imageButton);
        sendLayout=(LinearLayout) findViewById(R.id.sendlayout);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedEmail=textView.getText().toString();
                sendRequest();
            }
        });


       /* userEmail=(EditText)findViewById(R.id.emailEditText);
        userEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Validation.hasText(userEmail);
                Validation.isEmailAddress(userEmail, true);
            }
        });*/
        credentialsSharedPreferences = getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);

      //  getUsersDetails();
    }


    private void sendRequest(){

        sendLayout.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SEND_REQUEST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        System.out.println("************************ Did well" + s);

                        if(s.contains("Email does not exist in our server"))
                        {
                            Toast.makeText(WalkActivity.this,"Email does not exist in our server", Toast.LENGTH_LONG).show();
                            sendLayout.setVisibility(View.INVISIBLE);
                        }

                        //Showing toast message of the response
                        //Toast.makeText(CreateProfile.this,"Successfully created",Toast.LENGTH_LONG);
                        //System.out.println("MAIN response "+s);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        //System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        //Toast.makeText(CreateProfile.this,"Error creating account",Toast.LENGTH_LONG);
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                String email = selectedEmail;
                String myEmail = credentialsSharedPreferences.getString(savedemail,null);


                //Creating parameters
                Map<String,String> params = new Hashtable<>();


                params.put(KEY_EMAIL, email);
                params.put(MY_EMAIL, myEmail);


                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void getUsersDetails(){
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading users...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_USALAMA_USERS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        System.out.println("************************ Did well" + s);

                        String [] allUsers = s.split("#");
                        selectedEmail = allUsers[0];
                     //   for (String allUser : allUsers) {
                       //     adapter.add(allUser);
                       // }

                        pDialog.dismiss();
                        //Showing toast message of the response
                        //Toast.makeText(CreateProfile.this,"Successfully created",Toast.LENGTH_LONG);
                        //System.out.println("MAIN response "+s);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        //System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        //Toast.makeText(CreateProfile.this,"Error creating account",Toast.LENGTH_LONG);
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                String email = credentialsSharedPreferences.getString(savedemail,null);


                //Creating parameters
                Map<String,String> params = new Hashtable<>();


                params.put(KEY_EMAIL, email);


                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }
    public ArrayList<String> getNameEmailDetails(){
        ArrayList<String> names = new ArrayList<String>();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                Cursor cur1 = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                while (cur1.moveToNext()) {
                    //to get the contact names
                    String name=cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    Log.e("Name :", name);
                    String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    Log.e("Email", email);
                    if(email!=null){
                        names.add(name);
                    }
                }
                cur1.close();
            }
        }
        return names;
    }
}
