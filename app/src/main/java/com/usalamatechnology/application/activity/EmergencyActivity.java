package com.usalamatechnology.application.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.usalamatechnology.application.R;

public class EmergencyActivity extends AppCompatActivity {

    LinearLayout ambulanceLayout;
    LinearLayout policeLayout;
    LinearLayout hospitalsLayout;

    TextView ambulanceLayoutAc;
    TextView policeLayoutAc;
    TextView hospitalsLayoutAc;

    boolean ambulance = false;
    boolean police = false;
    boolean hospital = false;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbar.setTitle("Hotline numbers");

            setSupportActionBar(mToolbar);
        }
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hospitalsLayout = (LinearLayout) findViewById(R.id.layoutHospital);
        ambulanceLayout = (LinearLayout) findViewById(R.id.layoutAmbulance);
        policeLayout = (LinearLayout) findViewById(R.id.layoutPoliceStation);


        hospitalsLayoutAc = (TextView) findViewById(R.id.textViewHosptitals);
        policeLayoutAc = (TextView) findViewById(R.id.textViewPoliceStation);
        ambulanceLayoutAc = (TextView) findViewById(R.id.textViewAmbulance);


        findViewById(R.id.amb112).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:112"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        findViewById(R.id.amb999).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:999"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        findViewById(R.id.amb2717374).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:020-2717374"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        findViewById(R.id.amb3950000).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:020-3950000"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        findViewById(R.id.amb6005837).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:020-6005837"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        findViewById(R.id.amb2212699).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:020-2212699"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        findViewById(R.id.driviing6345345qw).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:020-3315454"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        findViewById(R.id.amb3315455).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:020-3315455"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        findViewById(R.id.amb3743858).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:020-3743858"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        findViewById(R.id.amb0700395395).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:0700395395"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        findViewById(R.id.amb6004945).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:020-6004945"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        findViewById(R.id.amb2210000).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:020-2210000"));
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        hospitalsLayoutAc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!hospital)
                {
                    hospitalsLayout.setVisibility(View.VISIBLE);
                    ambulanceLayout.setVisibility(View.GONE);
                    policeLayout.setVisibility(View.GONE);
                    hospital=true;
                }
                else
                {
                    hospitalsLayout.setVisibility(View.GONE);
                    hospital=false;
                }

            }
        });

        ambulanceLayoutAc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ambulance)
                {
                    hospitalsLayout.setVisibility(View.GONE);
                    ambulanceLayout.setVisibility(View.VISIBLE);
                    policeLayout.setVisibility(View.GONE);
                }
                else
                {
                    ambulanceLayout.setVisibility(View.GONE);
                    ambulance=false;
                }

            }
        });

        policeLayoutAc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!police)
                {
                    hospitalsLayout.setVisibility(View.GONE);
                    ambulanceLayout.setVisibility(View.GONE);
                    policeLayout.setVisibility(View.VISIBLE);
                }
                else
                {
                    policeLayout.setVisibility(View.GONE);
                    police=false;
                }

            }
        });


    }
}
