package com.usalamatechnology.application.activity;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.usalamatechnology.application.R;

import agency.tango.materialintroscreen.SlideFragment;

public class CustomSlide6 extends SlideFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_custom_slide6, container, false);
        return view;
    }

    @Override
    public int backgroundColor() {
        return R.color.six_slide_background;
    }

    @Override
    public int buttonsColor() {
        return R.color.six_slide_buttons;
    }

    //@Override
    //public boolean canMoveFurther() {
      //  return checkBox.isChecked();
   // }

    @Override
    public String cantMoveFurtherErrorMessage() {
        return getString(R.string.error_message);
    }
}