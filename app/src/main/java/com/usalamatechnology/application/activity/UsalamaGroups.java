package com.usalamatechnology.application.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.usalamatechnology.application.R;
import com.usalamatechnology.application.adapter.GroupsIObserver;
import com.usalamatechnology.application.adapter.GroupsRVAdapter;
import com.usalamatechnology.application.model.DatabaseHelper;
import com.usalamatechnology.application.model.UsalamaGroupMember;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class UsalamaGroups extends AppCompatActivity implements GroupsIObserver {

    private static final String KEY_EMAIL = "email";

    public static final String savedemail = "email";
    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";
    String  KEY_GROUPNAME="group_name";
    String  KEY_CHOICE="choice";

    public static SharedPreferences credentialsSharedPreferences;
    public List<UsalamaGroupMember> usalamaGroupMembers;
    private RecyclerView rv;
    public static FragmentActivity host;
    GroupsRVAdapter adapter;
    private Toolbar mToolbar;

    private Cursor mCursor;
    private DatabaseHelper databaseHelper;
    // Create a hash map
    HashMap hm = new HashMap();
    private String groupName;
    private String email;
    TextView emptyView;
    private MaterialDialog pd; //Varaible for all material dialogs

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usalama_groups);
        databaseHelper = new DatabaseHelper(this);
        rv = (RecyclerView) findViewById(R.id.rv);

        System.out.println("OOOOOOOOOOOOOOOOOOOOOOPENED");

         emptyView = (TextView) findViewById(R.id.empty_view);
        credentialsSharedPreferences = getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);

if(credentialsSharedPreferences.getString(KEY_CHOICE, "Not Available")=="Personal")
{
    rv.setVisibility(View.GONE);
    emptyView.setVisibility(View.VISIBLE);
}
        else
{
    rv.setVisibility(View.VISIBLE);
    emptyView.setVisibility(View.GONE);
}



            LinearLayoutManager llm = new LinearLayoutManager(this);

            rv.setLayoutManager(llm);
            rv.setHasFixedSize(true);

            initializeAdapter();
            initializeData();



        groupName = credentialsSharedPreferences.getString(KEY_GROUPNAME, null);
        email = credentialsSharedPreferences.getString(savedemail, null);

        host=this;

        databaseHelper.clearAllMembers();


                getAllMembersFromOnline();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if(mToolbar!=null) {
            mToolbar.setTitle(credentialsSharedPreferences.getString(KEY_GROUPNAME, null));

            setSupportActionBar(mToolbar);
        }
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);



    }

    private void getAllMembersFromOnline() {

        pd = new MaterialDialog.Builder(this)
                .title("Loading Members...")
                .titleColorRes(R.color.colorPrimary)
                .dividerColorRes(R.color.colorPrimaryDark)
                .content("Give us a sec. Make sure you are connected to the internet")
                .progress(true, 0)
                .show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_ALL_USERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        if(pd!=null)
                        {
                            pd.dismiss();
                        }

                        JSONArray array = null;
                        try {
                            JSONObject jsonObj = new JSONObject(s);

                            JSONArray posts = jsonObj.getJSONArray("usalama_users");

                            for (int i = 0; i < posts.length(); i++) {
                                JSONObject row = posts.getJSONObject(i);

                                String user_name = row.getString("name");
                                String image_path = row.getString("imagepath");

                                System.out.println("++++++++++++++image_path " + image_path);
                                databaseHelper.saveGroups(user_name, image_path);
                            }
                            initializeAdapter();
                            initializeData();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println("MAIN response " + s);

                        if (databaseHelper.isGroupDbEmpty()) {

                            rv.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                            System.out.println("&&&&&&&&&&&&&&&&&&&&&& Empty Db");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        Toast.makeText(UsalamaGroups.this,"Error creating account",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params = new Hashtable<>();


                params.put(KEY_GROUPNAME, groupName);
                params.put(KEY_EMAIL, email);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void initializeData() {
        System.out.println(" ********************* " + usalamaGroupMembers.size());
        adapter = new GroupsRVAdapter(usalamaGroupMembers);
        rv.setAdapter(adapter);
        adapter.setListener(this);
    }

    private void initializeAdapter() {

        usalamaGroupMembers = new ArrayList<>();
        mCursor= databaseHelper.getAllGroupMembers();



        for(mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
            String allMembers= (String) hm.get(mCursor.getString(1));

            usalamaGroupMembers.add(new UsalamaGroupMember(mCursor.getString(1),mCursor.getString(2)));
        }
    }
}
