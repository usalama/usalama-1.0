/*
 * Copyright (C) 2015 Brent Marriott
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.usalamatechnology.application.activity;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.usalamatechnology.application.R;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.melnykov.fab.FloatingActionButton;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimerMainFragment extends TimerFragment {
    private int mSeries1Index;
    private String mProgress;
    private ImageButton setTimer;
    private FloatingActionButton pauseTimer;
    private TextView txtTime,timeDiff,resetHome;
    private int mYear, mMonth, mDay, mHour, mMinute;
    public static final String TIMEPREFERENCES = "timePrefs";
    public static SharedPreferences timeSharedPreferences;
    SharedPreferences.Editor timeEditor;

    public static final String USERLOCATIONPREFERENCES = "userlocationPrefs";
    public static SharedPreferences userLocationSharedPreferences;
    SharedPreferences.Editor userLocationEditor;

    public static final String userLatitude = "userLatitude";
    public static final String userLongitude = "userLongitude";

    public static final String LOCATIONPREFERENCES = "LocationPrefs";
    public static SharedPreferences locationSharedPreferences;
    SharedPreferences.Editor locationEditor;


    public static final String homeLatitude = "homeLatitude";
    public static final String homeLongitude = "homeLongitude";

    Thread myThread;
    Runnable myRunnableThread;
    double setTimeMillis;
    double fixedMills;
    String goodPercent="0";
    float thePercent=0;
     DecoView arcView = null;

     ImageView imgView;
    public static final String timeHour = "timeHour";
    public static final String timeMinute = "timeMinute";
    public static final String setTime = "setTime";
    boolean start=true;
    Intent intent;

    private float distance;

    public TimerMainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v =inflater.inflate(R.layout.fragment_sample_fit2, container, false);

        timeSharedPreferences = getActivity().getSharedPreferences(TIMEPREFERENCES, Context.MODE_PRIVATE);
        timeEditor = timeSharedPreferences.edit();


        userLocationSharedPreferences = getActivity().getSharedPreferences(USERLOCATIONPREFERENCES, Context.MODE_PRIVATE);
        userLocationEditor = userLocationSharedPreferences.edit();

        locationSharedPreferences = getActivity().getSharedPreferences(LOCATIONPREFERENCES, Context.MODE_PRIVATE);
        locationEditor = locationSharedPreferences.edit();

        pauseTimer= (FloatingActionButton)v.findViewById(R.id.pause_timer);
        setTimer = (ImageButton)v.findViewById(R.id.setTimer);
        txtTime = (TextView)v.findViewById(R.id.textView3);
        timeDiff = (TextView)v.findViewById(R.id.textView15);
        resetHome = (TextView)v.findViewById(R.id.textView18);

        resetHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent(getActivity(), TimerActivity.class);
                startActivity(resultIntent);
            }
        });

        pauseTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread.currentThread().interrupt();
                myThread.interrupt();
                start=false;
                addAnimation(arcView, mSeries1Index, 0, 2000, imgView, R.drawable.gridbg, "%.1f", COLOR_BLUE, false);
                timeEditor.putString(timeHour, null);
                timeEditor.putString(timeMinute, null);
                timeEditor.putString(setTime, null);
                timeEditor.apply();
                String diff = "0" + "H " + "0" + "m " + "0" + "s "; // updated value every1 second

                timeDiff.setText(diff);
                txtTime.setText("00" + ":" + "00");
            }
        });
        setTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Time
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);


                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                start=true;
                                String hour= String.valueOf(hourOfDay);
                                String minuteOfDay= String.valueOf(minute);
                                thePercent=0;
                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

                                if(hour.length()==1)
                                {
                                    hour="0"+hour;
                                }
                                if(minuteOfDay.length()==1)
                                {
                                    minuteOfDay="0"+minuteOfDay;
                                }
                                txtTime.setText(hour + ":" + minuteOfDay);
                                timeEditor.putString(timeHour, String.valueOf(hourOfDay));
                                timeEditor.putString(timeMinute, String.valueOf(minute));
                                setTimeMillis= System.currentTimeMillis();
                                timeEditor.putString(setTime, String.valueOf(setTimeMillis));
                                timeEditor.apply();

                                Date systemDate = Calendar.getInstance().getTime();
                                String myDate = sdf.format(systemDate);
                                double millse=0.0;
                                Date Date1 = null;
                                try {
                                    Date1 = sdf.parse(myDate);
                                    Date Date2 = sdf.parse(timeSharedPreferences.getString(timeHour, null) + ":" + timeSharedPreferences.getString(timeMinute, null) + ":" + "00");
                                     millse = Date2.getTime()-Date1.getTime();
                                    fixedMills = Math.abs(millse);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                 intent = new Intent(getActivity(), MyTimerService.class);
                                getActivity().startService(intent);
                                System.out.println("My timer shoul have started");
                                startTimer();

                            }
                        }, mHour, mMinute,false);

                        timePickerDialog.show();
            }
        });
        startTimer();

        return v;
    }

    private void startTimer()
    {
        if(timeSharedPreferences.getString(timeHour, null)!=null && timeSharedPreferences.getString(setTime, null)!=null)
        {
            String hour="";
            String minute="";
            if(timeSharedPreferences.getString(timeHour, null).length()==1)
            {
                hour="0"+timeSharedPreferences.getString(timeHour, null);
            }
            else
            {
                hour=timeSharedPreferences.getString(timeHour, null);
            }
            if(timeSharedPreferences.getString(timeMinute, null).length()==1)
            {
                minute="0"+timeSharedPreferences.getString(timeMinute, null);
            }else
            {
                minute=timeSharedPreferences.getString(timeMinute, null);
            }
            System.out.println("minute ^^^^^^^^^^^^ " + minute);
            txtTime.setText(hour + ":" + minute);
            setTimeMillis= Double.parseDouble(timeSharedPreferences.getString(setTime, null));
            myThread = null;
            myRunnableThread = new CountDownRunner();
            myThread= new Thread(myRunnableThread);
            myThread.start();

        }
    }


    public void doWork()
    {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (start && fixedMills!=0) {
                    try {
                        if (!myThread.isInterrupted()) {

                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

                            Date systemDate = Calendar.getInstance().getTime();
                            String myDate = sdf.format(systemDate);

                            Date Date1 = sdf.parse(myDate);
                            double millse = 0.0;
                            if (timeSharedPreferences.getString(timeHour, null) != null) {
                                Date Date2 = sdf.parse(timeSharedPreferences.getString(timeHour, null) + ":" + timeSharedPreferences.getString(timeMinute, null) + ":" + "00");

                                millse = Date2.getTime() - Date1.getTime();
                                System.out.println("--------------------------------------------millse " + millse);
                            }

                            if (millse >= 0) {
                                double currentTimeMillis = System.currentTimeMillis();
                                double diffMills = Math.abs(currentTimeMillis - setTimeMillis);
                                double mills = Math.abs(millse);
                                double percent = (diffMills / fixedMills) * 100;


                                DecimalFormat df = new DecimalFormat("#.#");
                                df.setRoundingMode(RoundingMode.CEILING);
                                goodPercent = df.format(percent);
                                System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%setTimeMillis " + setTimeMillis);
                                System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%currentTimeMillis " + currentTimeMillis);
                                System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%diffMills " + diffMills);
                                System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%fixedMills " + fixedMills);
                                System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%percent " + goodPercent);
                                thePercent = Float.parseFloat(goodPercent);
                                if (thePercent >= 100) {
                                    Thread.currentThread().interrupt();
                                    myThread.interrupt();
                                    thePercent = 100;
                                    start = false;
                                }
                                addAnimation(arcView, mSeries1Index, thePercent, 2000, imgView, R.drawable.gridbg, "%.1f", COLOR_BLUE, false);

                                int Hours = (int) (mills / (1000 * 60 * 60));
                                int Mins = (int) (mills / (1000 * 60)) % 60;
                                long Secs = (int) (mills / 1000) % 60;

                                String diff = Hours + "H " + Mins + "m " + Secs + "s "; // updated value every1 second

                                timeDiff.setText(diff);
                            } else {

                                Thread.currentThread().interrupt();
                                myThread.interrupt();
                                timeEditor.putString(timeHour, null);
                                timeEditor.putString(timeMinute, null);
                                timeEditor.putString(setTime, null);
                                timeEditor.apply();
                                getActivity().stopService(intent);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Thread.currentThread().interrupt();
                }

            }
        });
    }


    class CountDownRunner implements Runnable
    {
        // @Override
        public void run()
        {
            while(!Thread.currentThread().isInterrupted())
            {
                try
                {
                    doWork();
                    Thread.sleep(1000); // Pause of 1 Second
                }
                catch (InterruptedException e)
                {
                    Thread.currentThread().interrupt();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }



    @Override
    protected void createTracks() {
        setDemoFinished(false);
        final View view = getView();
        final DecoView decoView = getDecoView();
        if (view == null || decoView == null) {
            return;
        }

        view.setBackgroundColor(Color.argb(255, 255, 255, 255));

        decoView.executeReset();
        decoView.deleteAll();

        final float mSeriesMax = 100f;

        SeriesItem seriesBack1Item = new SeriesItem.Builder(COLOR_BACK)
                .setRange(0, mSeriesMax, mSeriesMax)
                .setLineWidth(getDimension(26))
                .build();

        decoView.addSeries(seriesBack1Item);

        SeriesItem series1Item = new SeriesItem.Builder(COLOR_NEUTRAL)
                .setRange(0, mSeriesMax, 0)
                .setInitialVisibility(false)
                .setLineWidth(getDimension(26))
                .setCapRounded(true)
                .setShowPointWhenEmpty(true)
                .build();

        mSeries1Index = decoView.addSeries(series1Item);

        TextView textListener = (TextView) view.findViewById(R.id.textProgress);
        addFitListener(series1Item, textListener);
    }

    @Override
    protected void setupEvents() {
        arcView = getDecoView();
        final View view = getView();
        if (view == null || arcView == null || arcView.isEmpty()) {
            return;
        }

        imgView = (ImageView) view.findViewById(R.id.imageViewAvatar);

        addAnimation(arcView, mSeries1Index, thePercent, 2000, imgView, R.drawable.gridbg, "%.1f", COLOR_BLUE, false);
       // addAnimation(arcView, mSeries1Index, 16.4f, 9000, imgView, R.drawable.ic_fit_run, "Run %.1f Km", COLOR_YELLOW, false);
      //  addAnimation(arcView, mSeries1Index, 58f, 16000, imgView, R.drawable.ic_fit_gym, "Gym %.0f min", COLOR_PINK, false);
     //  addAnimation(arcView, mSeries1Index, 3.38f, 23000, imgView, R.drawable.ic_fit_swim, "Swim %.2f Km", COLOR_BLUE, true);
    }

    private void addAnimation(final DecoView arcView,
                              int series, float moveTo, int delay,
                              final ImageView imageView, final int imageId,
                              final String format, final int color, final boolean restart) {

        DecoEvent.ExecuteEventListener listener = new DecoEvent.ExecuteEventListener() {
            @Override
            public void onEventStart(DecoEvent event) {
                imageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), imageId));
                showAvatar(true, imageView);
                mProgress = format;
            }

            @Override
            public void onEventEnd(DecoEvent event) {
                showAvatar(false, imageView);
                if (restart) {
                    setupEvents();
                }
            }
        };

        arcView.addEvent(new DecoEvent.Builder(moveTo)
                .setIndex(series)
                .setDelay(delay)
                .setDuration(5000)
                .setListener(listener)
                .setColor(color)
                .build());
    }

    private void showAvatar(boolean show, View view) {
        AlphaAnimation animation = new AlphaAnimation(show ? 0.0f : 1.0f, show ? 1.0f : 0.0f);
        animation.setDuration(2000);
        animation.setFillAfter(true);
        view.startAnimation(animation);
    }

    private void addFitListener(@NonNull final SeriesItem seriesItem, @NonNull final TextView view) {
        seriesItem.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {

            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                if (mProgress != null) {
                    if (mProgress.contains("%%")) {
                        view.setText(String.format(mProgress, (1.0f - (currentPosition / seriesItem.getMaxValue())) * 100f));
                    } else {
                        view.setText(String.format(mProgress, currentPosition));
                    }
                }
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });
    }
}