package com.usalamatechnology.application.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.usalamatechnology.application.R;

public class ProgressActivity extends AppCompatActivity {

    Button b1;
    private ProgressDialog progress;
    boolean isCanceled=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);



    }


    public void download(View view){
        progress=new ProgressDialog(this);
        progress.setMessage("This distress will be sent in 5 seconds. Press the cancel button to stop.");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(false);
        progress.setProgress(0);
        progress.setMax(5);
        // Put a cancel button in progress dialog
        progress.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener(){
            // Set a click listener for progress dialog cancel button
            @Override
            public void onClick(DialogInterface dialog, int which){
                // dismiss the progress dialog
                progress.dismiss();
                // Tell the system about cancellation
                isCanceled = true;
            }
        });
        progress.show();

        final int totalProgressTime = 5;
        final Thread t = new Thread() {
            @Override
            public void run() {
                int jumpTime = 0;

                while(jumpTime <= totalProgressTime) {
                    try {
                        sleep(1000);
                        jumpTime += 1;
                        progress.setProgress(jumpTime);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                progress.dismiss();
            }
        };
        t.start();
    }
}
