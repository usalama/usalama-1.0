package com.usalamatechnology.application.activity;

/**
 * Created by Belal on 11/14/2015.
 */
public class Config {
    //URL to our login.php file
    public static final String LOGIN_URL = "http://easypluslife.com/Usalama_project/admin/applogin.php";
    public static String KEY_PHONE="phone";

    // Php Application URL to store Reg ID created
    String APP_SERVER_URL = "http://easypluslife.com/gcmwebapp/insertuser.php";

    // Google Project Number
    String GOOGLE_PROJ_ID = "1057207929831";
    String MSG_KEY = "m";

    //URL to our login.php file
    public static final String LOCATION_URL = "http://easypluslife.com/Usalama_project/admin/locupload.php";
    public static final String UPLOAD_POST = "http://easypluslife.com/Usalama_project/admin/uploadpost.php";
    public static final String CHECK_USER = "http://easypluslife.com/Usalama_project/admin/checkUser2.php";
    public static final String CHECK_USER3 = "http://easypluslife.com/Usalama_project/admin/checkUser3.php";

    public static final String GET_ALL_APPROVALS = "http://easypluslife.com/Usalama_project/admin/getAllApprovals.php";
    public static final String GET_ALL_POSTS = "http://easypluslife.com/Usalama_project/admin/getAllPosts.php";
    public static final String GET_ALL_USERS = "http://easypluslife.com/Usalama_project/admin/getAllUsers.php";


    public static final String GET_NEW_POSTS = "http://easypluslife.com/Usalama_project/admin/getNewPosts.php";
    public static final String UPLOAD_FLAG = "http://easypluslife.com/Usalama_project/admin/uploadFlag.php";

    public static final String UPLOAD_APPROVAL = "http://easypluslife.com/Usalama_project/admin/uploadApproval.php";

    public static final String GET_USER_LOCATIONS = "http://easypluslife.com/Usalama_project/admin/getuserlocations.php";

    //URL to our login.php file
    public static final String GET_NEARBY_USERS = "http://easypluslife.com/Usalama_project/admin/getnearbyusers.php";

    //URL to our login.php file
    public static final String REVOKE_ACCESS = "http://easypluslife.com/Usalama_project/admin/revokeaccess.php";

    //URL to our login.php file
    public static final String CREATE_PROFILE_URL = "http://easypluslife.com/Usalama_project/admin/upload.php";

    //URL to our login.php file
    public static final String GET_ALL_GROUPS = "http://easypluslife.com/Usalama_project/admin/getAllGroups.php";
    public static final String updateUserDetails = "http://easypluslife.com/Usalama_project/admin/updateUserDetails.php";
    public static final String updateUserDetails2 = "http://easypluslife.com/Usalama_project/admin/updateUserDetails2.php";

    //URL to our login.php file
    public static final String RESPONSE_TO_WALK = "http://easypluslife.com/Usalama_project/admin/walkrequest.php";
    //URL to our login.php file
    public static final String USER_LOCATION_URL = "http://easypluslife.com/Usalama_project/admin/userLocation.php";

    public static final String GET_USALAMA_USERS_URL = "http://easypluslife.com/Usalama_project/admin/getusers.php";
    public static final String SEND_REQUEST = "http://easypluslife.com/Usalama_project/admin/sendrequest.php";
    public static final String UPDATE_MOBILE = "http://easypluslife.com/Usalama_project/admin/updatePhone.php";


    //Keys for email and password as defined in our $_POST['key'] in login.php
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";

    public static final String KEY_LAT = "latitude";
    public static final String KEY_LON = "longitude";
    public static final String KEY_MES = "message";
    public static final String KEY_UEMAIL = "email";

    //If server response is equal to this that means login is successful
    public static final String LOGIN_SUCCESS = "success";

    //If server response is equal to this that means login is successful
    public static final String UPLOAD_SUCCESS = "success";

    //Keys for Sharedpreferences
    //This would be the name of our shared preferences
    public static final String SHARED_PREF_NAME = "myloginapp";

    //This would be used to store the email of current logged in user
    public static final String EMAIL_SHARED_PREF = "email";

    //This would be used to store the email of current logged in user
    public static final String IMAGE_SHARED_PREF = "path";

    //We will use this to store the boolean in sharedpreference to track user is loggedin or not
    public static final String LOGGEDIN_SHARED_PREF = "loggedin";
}
