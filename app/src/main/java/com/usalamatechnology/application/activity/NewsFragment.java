package com.usalamatechnology.application.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.usalamatechnology.application.R;
import com.usalamatechnology.application.adapter.NewsIObserver;
import com.usalamatechnology.application.adapter.NewsRVAdapter;
import com.usalamatechnology.application.model.DatabaseHelper;
import com.usalamatechnology.application.model.UsalamaPost;
import com.melnykov.fab.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by hp1 on 21-01-2015.
 */
public class NewsFragment extends Fragment implements NewsIObserver {

    private static final String KEY_POST = "userpost";
    private static final String KEY_AREA = "area";

    private static final String KEY_POST_ID = "postid";
    private static final String KEY_BROADCAST = "broadcast";
    private static final String KEY_USER_ID = "userid";
    private static final String KEY_USER_COMMENT = "comment";
    public static final String USERLOCATIONPREFERENCES = "userlocationPrefs";
    public static SharedPreferences userLocationSharedPreferences;

    public static SharedPreferences sharedpreferences;
    public static final String broadcast_group = "broadcast_group";
    public static final String broadcast_public = "broadcast_public";
    public static final String broadcast_locale = "broadcast_locale";

    public static final String userLatitude = "userLatitude";
    public static final String userLongitude = "userLongitude";
    public static final String userAccuracy = "userAccuracy";
    public static final String MyPREFERENCES = "MyPrefs" ;

    private static final String KEY_MAX_ID = "maxid";
    String area="Madaraka";
    private Toolbar mToolbar;
    FloatingActionButton send;
    public static final String savedEmail = "email";
    public static final String CREDENTIALSPREFERENCES = "CredentialsPrefs";

    public static SharedPreferences credentialsSharedPreferences;

    EditText post;
    public List<UsalamaPost> usalamaPosts;
    private RecyclerView rv;
    private DatabaseHelper databaseHelper;

    public static FragmentActivity host ;
    public static int positionOfCard=0;
    NewsRVAdapter adapter;
    private Cursor mCursor;
    private Cursor mRecord;

    private String email;
    private String KEY_EMAIL = "email";
    private String maxPostId;
    private Cursor mCursorApprove;
    private boolean isSending=false;
    String stateName="not available";
    String broadcast="";


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.news_fragment,container,false);

        credentialsSharedPreferences = getActivity().getSharedPreferences(CREDENTIALSPREFERENCES, Context.MODE_PRIVATE);
        userLocationSharedPreferences = getActivity().getSharedPreferences(USERLOCATIONPREFERENCES, Context.MODE_PRIVATE);
        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        host=getActivity();
        databaseHelper = new DatabaseHelper(getActivity());
        rv = (RecyclerView) v.findViewById(R.id.rv);
        TextView emptyView = (TextView) v.findViewById(R.id.empty_view);
        databaseHelper = new DatabaseHelper(getActivity());

        if (databaseHelper.isDbEmpty()) {
            rv.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
        else {
            rv.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());

            rv.setLayoutManager(llm);
            rv.setHasFixedSize(true);

            initializeAdapter();
            initializeData();
        }

        Double userCurrentLat = Double.valueOf(userLocationSharedPreferences.getString(userLatitude, "0.0"));
        Double userCurrentLong = Double.valueOf(userLocationSharedPreferences.getString(userLongitude, "0.0"));

        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(userCurrentLat, userCurrentLong, 1);
            if(addresses!=null)
            {
                String cityName = addresses.get(0).getAddressLine(0);
                stateName = addresses.get(0).getAddressLine(1);
                String countryName = addresses.get(0).getAddressLine(2);
            }
            else
            {
                stateName="Not available";
            }


        } catch (IOException e) {
            e.printStackTrace();
        }


        send=(FloatingActionButton)v.findViewById(R.id.btnSend);
        post=(EditText)v.findViewById(R.id.editTextPost);

        email = credentialsSharedPreferences.getString(savedEmail, null);

        post.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Validation.isLimit(post);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if(databaseHelper.isPostDbEmpty())
        {
            if(CheckNetwork.isInternetAvailable(getActivity()))
            {
                //getAllApprovalsFromOnline();
                getAllPostFromOnline();
            }
            else
            {
                Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
            }

        }
        else
        {
            rv.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());

            rv.setLayoutManager(llm);
            rv.setHasFixedSize(true);

            initializeAdapter();
            initializeData();
            getMaxId();

            if(CheckNetwork.isInternetAvailable(getActivity()))
            {
                //getAllApprovalsFromOnline();
                getNewOnlinePosts();
            }
            else
            {
                Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
            }


            System.out.println("Am not empty");
            //getLocalPosts();

        }

        if(sharedpreferences.getString(broadcast_group,"no" ).equals("yes"))
        {
            broadcast="group";
        }


        if(sharedpreferences.getString(broadcast_locale,"no" ).equals("yes"))
        {
            broadcast="locale";
        }

        if(sharedpreferences.getString(broadcast_public,"no" ).equals("yes"))
        {
            broadcast="public";
        }

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        if(CheckNetwork.isInternetAvailable(getActivity()))
        {
            if(!isSending)
            {
                isSending=true;
                if(post.getText().toString().trim().length()>0)
                {
                    uploadPost();
                }
                else
                {
                    Toast.makeText(getActivity(), "You cannot create an empty post.", Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(getActivity(), "We are still uploading your post.", Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
        }
            }
        });

        return v;
    }


    private void initializeAdapter() {

        initializeApprovalAdapter();

        usalamaPosts = new ArrayList<>();
        mCursor= databaseHelper.getAllPosts();



        for(mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
            String allApproval= (String) hm.get(mCursor.getString(1));

            if(allApproval!=null && !allApproval.equals(""))
            {}
            else
            {
                 allApproval="0";
            }

            usalamaPosts.add(new UsalamaPost(mCursor.getString(1),mCursor.getString(2),"Anonymous",
                    mCursor.getString(4),mCursor.getString(5),mCursor.getString(6)+" "+mCursor.getString(7),allApproval));
        }
    }


    // Create a hash map
    HashMap hm = new HashMap();

    private void initializeApprovalAdapter() {
        hm.clear();
        mCursorApprove= databaseHelper.getAllApprovals();

        for(mCursorApprove.moveToFirst(); !mCursorApprove.isAfterLast(); mCursorApprove.moveToNext()) {
            hm.put(mCursorApprove.getString(1), mCursorApprove.getString(2));
        }
    }

    private void initializeData() {
        System.out.println(" ********************* " + usalamaPosts.size());
        adapter = new NewsRVAdapter(usalamaPosts);
        rv.setAdapter(adapter);
        adapter.setListener(this);
    }

    private String getMaxId()
    {
        mCursor = databaseHelper.getAllPosts();
        int max = 0;

        if(mCursor!=null)
        {
            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                if (Integer.parseInt(mCursor.getString(1)) > max) {
                    max = Integer.parseInt(mCursor.getString(1));
                }

            }
            maxPostId = String.valueOf(max);
        }
        System.out.println("MAAAAAAAAAAAAAAAAAAAAAAAAAAAXXXXXXXXXXXXXXXXXXX " + maxPostId);
        return maxPostId;
    }


    private void getNewOnlinePosts() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_NEW_POSTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        System.out.println("SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS "+s);
                    if(s!=null && !s.equals(""))
                    {

                        JSONArray array = null;
                        try {
                            JSONObject jsonObj = new JSONObject(s);

                            JSONArray posts = jsonObj.getJSONArray("usalama_posts");

                            for (int i = 0; i < posts.length(); i++) {
                                JSONObject row = posts.getJSONObject(i);

                                String postid = row.getString("id");
                                String user_id = row.getString("user_id");
                                String user_type = row.getString("user_type");
                                String content = row.getString("content");
                                String area = row.getString("area");
                                String date = row.getString("date");
                                String time = row.getString("time");
                                String status = row.getString("status");

                                System.out.println("++++++++++++++Content " + content);
                                databaseHelper.savePost(postid, user_id, user_type, content, area, date, time, status);
                            }
                            initializeAdapter();
                            initializeData();
                            refreshFragment();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //Disimissing the progress dialog
                        //loading.dismiss();
                        //Showing toast message of the response
                        //Toast.makeText(getActivity(), "Successfully created", Toast.LENGTH_LONG).show();
                        System.out.println("MAIN response " + s);
                    }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        Toast.makeText(getActivity(),"Error creating account",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                getMaxId();

                //Creating parameters
                Map<String,String> params = new Hashtable<>();

                //Adding parameters
                params.put(KEY_AREA, area);
                params.put(KEY_MAX_ID, maxPostId);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }



    private void getAllPostFromOnline() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_ALL_POSTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        JSONArray array = null;
                        try {
                            JSONObject jsonObj = new JSONObject(s);

                            JSONArray posts = jsonObj.getJSONArray("usalama_posts");

                            for (int i = 0; i < posts.length(); i++) {
                                JSONObject row = posts.getJSONObject(i);

                                String postid = row.getString("id");
                                String user_id = row.getString("user_id");
                                String user_type = row.getString("user_type");
                                String content = row.getString("content");
                                String area = row.getString("area");
                                String date = row.getString("date");
                                String time = row.getString("time");
                                String status = row.getString("status");

                                System.out.println("++++++++++++++Content " + content);
                                databaseHelper.savePost(postid, user_id, user_type, content, area, date, time, status);
                            }
                            initializeAdapter();
                            initializeData();
                            refreshFragment();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //Disimissing the progress dialog
                        //loading.dismiss();
                        //Showing toast message of the response
                        //Toast.makeText(getActivity(), "Successfully created", Toast.LENGTH_LONG).show();
                        System.out.println("MAIN response " + s);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        Toast.makeText(getActivity(),"Error creating account",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params = new Hashtable<>();


                params.put(KEY_AREA, area);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }


    private void getAllApprovalsFromOnline() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_ALL_APPROVALS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        JSONArray array = null;
                        try {
                            JSONObject jsonObj = new JSONObject(s);

                            JSONArray posts = jsonObj.getJSONArray("usalama_approvals");

                            for (int i = 0; i < posts.length(); i++) {
                                JSONObject row = posts.getJSONObject(i);

                                String postid = row.getString("post_id");
                                String approvals = row.getString("approvals");

                                System.out.println("++++++++++++++approvals " + approvals);
                                databaseHelper.saveApprovals(postid, approvals);
                            }
                            initializeApprovalAdapter();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //Disimissing the progress dialog
                        //loading.dismiss();
                        //Showing toast message of the response
                       // Toast.makeText(getActivity(), "Successfully created", Toast.LENGTH_LONG).show();
                        System.out.println("MAIN response " + s);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        Toast.makeText(getActivity(),"Error creating account",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params = new Hashtable<>();


                params.put(KEY_AREA, area);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }



    public  void refreshFragment()
    {
        if(getActivity()!=null)
        {
            try{
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .detach(this)
                        .attach(this)
                        .commit();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }

        }

    }


    private void uploadApproval(final String postid, final String userid){
        //Showing the progress dialog
        //final ProgressDialog loading = ProgressDialog.show(getActivity(),"Uploading...","Please wait...",false,false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPLOAD_APPROVAL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        //loading.dismiss();
                        //Showing toast message of the response
                        //Toast.makeText(getActivity(), "Successfully created", Toast.LENGTH_LONG).show();
                        System.out.println("MAIN response " + s);
                        //getAllApprovalsFromOnline();
                        initializeAdapter();
                        initializeData();
                        //refreshFragment();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        Toast.makeText(getActivity(),"Error creating account",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String



                //Creating parameters
                Map<String,String> params = new Hashtable<>();

                //Adding parameters
                params.put(KEY_POST_ID, postid);
                params.put(KEY_USER_ID,userid);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }


    private void uploadFlag(final String postid, final String userid, final String comment){
        //Showing the progress dialog
        //final ProgressDialog loading = ProgressDialog.show(getActivity(),"Uploading...","Please wait...",false,false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPLOAD_FLAG,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Toast.makeText(getActivity(),"Your flag has been submitted",Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        Toast.makeText(getActivity(),"Error creating account",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String



                //Creating parameters
                Map<String,String> params = new Hashtable<>();

                //Adding parameters
                params.put(KEY_POST_ID, postid);
                params.put(KEY_USER_ID,userid);
                params.put(KEY_USER_COMMENT,comment);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void uploadPost(){
        //Showing the progress dialog
        //final ProgressDialog loading = ProgressDialog.show(getActivity(),"Uploading...","Please wait...",false,false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPLOAD_POST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        //loading.dismiss();
                        //Showing toast message of the response
                        Toast.makeText(getActivity(), "Successfully created", Toast.LENGTH_LONG).show();
                        System.out.println("MAIN response " + s);

                        post.setText("");
                        System.out.println("getMaxID " + getMaxId());

                        if(CheckNetwork.isInternetAvailable(getActivity()))
                        {
                           // getAllApprovalsFromOnline();
                            getNewOnlinePosts();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
                        }
                        isSending=false;

                        initializeAdapter();
                        initializeData();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        System.out.println("volleyError response " + volleyError.getMessage());
                        //Showing toast
                        Toast.makeText(getActivity(),"Error creating account",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                //Getting Image Name
                String postText = post.getText().toString().trim();


                //Creating parameters
                Map<String,String> params = new Hashtable<>();

                //Adding parameters
                params.put(KEY_POST, postText);
                params.put(KEY_AREA,stateName);
                params.put(KEY_EMAIL, email);
                params.put(KEY_BROADCAST, broadcast);
                System.out.println("++++++++++++++++++++++gfg "+broadcast+" -- "+stateName);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    @Override
    public void onAprroveClicked(int pos,String userid) {
        System.out.println("onAprroveClicked********************************* "+pos);
        uploadApproval(String.valueOf(pos), userid);
    }

    String comment="";

    @Override
    public void onFlagClicked(final int pos,final String userid) {
        System.out.println("onFlagClicked********************************* "+pos);
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.promts, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                                comment=userInput.getText().toString();
                                System.out.println("onFlagcomment********************************* "+comment);
                                uploadFlag(String.valueOf(pos), userid,comment);
                            }
                        })
                .setNegativeButton("Skip",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                                System.out.println("onFlagcomment********************************* " + comment);
                                uploadFlag(String.valueOf(pos), userid, "");
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


    }
}