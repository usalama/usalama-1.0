package com.usalamatechnology.application.activity;

/**
 * Created by Ravi on 29/07/15.
 */
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.usalamatechnology.application.R;

import java.util.Locale;


public class PoliceFragmentTwo extends Fragment {
    public static final String MyPREFERENCES = "detailsprefs" ;
    public static final String myLatitude = "myLatitude";
    public static final String myLongitude = "myLatitude";
    public static final String name = "name";
    public static final String distancetovic = "distancetovic";
    public static final String vicLongitude = "vicLongitude";
    public static final String vicLatitude = "vicLatitude";
    public static final String policeOneLong = "policeOneLong";
    public static final String policeOneLat = "policeOneLat";
    public static final String policeTwoLong = "policeTwoLong";
    public static final String policeTwoLat = "policeTwoLat";
    public static final String policeThreeLong = "policeThreeLong";
    public static final String policeThreeLat = "policeThreeLat";

    public static SharedPreferences detailsSharedpreferences;
    public PoliceFragmentTwo() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_policetwo, container, false);

        detailsSharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);



        ImageButton walk = (ImageButton) rootView.findViewById(R.id.imageButtonWalk);
        ImageButton bus = (ImageButton) rootView.findViewById(R.id.imageButtonBus);
        ImageButton drive = (ImageButton) rootView.findViewById(R.id.imageButtonDrive);

        System.out.println("******** " + detailsSharedpreferences.getString(name, null));

        walk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double vicLong=Double.parseDouble(detailsSharedpreferences.getString(policeTwoLong, null));
                Double vicLat=Double.parseDouble(detailsSharedpreferences.getString(policeTwoLat, null));

                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)&mode=walking", vicLat, vicLong, "Victim Location");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");

                try
                {
                    startActivity(intent);
                }
                catch(ActivityNotFoundException ex)
                {
                    try
                    {
                        Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        startActivity(unrestrictedIntent);
                    }
                    catch(ActivityNotFoundException innerEx)
                    {
                        Toast.makeText(getActivity(), "Please install a maps com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.edu.siredwin.application", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        bus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double vicLong=Double.parseDouble(detailsSharedpreferences.getString(policeTwoLong, null));
                Double vicLat=Double.parseDouble(detailsSharedpreferences.getString(policeTwoLat, null));

                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)&mode=transit", vicLat, vicLong, "Victim Location");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");

                try
                {
                    startActivity(intent);
                }
                catch(ActivityNotFoundException ex)
                {
                    try
                    {
                        Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        startActivity(unrestrictedIntent);
                    }
                    catch(ActivityNotFoundException innerEx)
                    {
                        Toast.makeText(getActivity(), "Please install a maps com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.edu.siredwin.application", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        drive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double vicLong=Double.parseDouble(detailsSharedpreferences.getString(policeTwoLong, null));
                Double vicLat=Double.parseDouble(detailsSharedpreferences.getString(policeTwoLat, null));

                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)&mode=driving", vicLat, vicLong, "Victim Location");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");

                try
                {
                    startActivity(intent);
                }
                catch(ActivityNotFoundException ex)
                {
                    try
                    {
                        Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        startActivity(unrestrictedIntent);
                    }
                    catch(ActivityNotFoundException innerEx)
                    {
                        Toast.makeText(getActivity(), "Please install a maps com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.edu.siredwin.application", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
