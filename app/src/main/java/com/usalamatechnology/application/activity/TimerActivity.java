package com.usalamatechnology.application.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.usalamatechnology.application.R;

public class TimerActivity extends AppCompatActivity {
Button setHome;
    ImageView setHomeImage;

    public static final String LOCATIONPREFERENCES = "LocationPrefs";
    public static SharedPreferences locationSharedPreferences;
    SharedPreferences.Editor locationEditor;


    public static final String homeLatitude = "homeLatitude";
    public static final String homeLongitude = "homeLongitude";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        locationSharedPreferences = getSharedPreferences(LOCATIONPREFERENCES, Context.MODE_PRIVATE);
        locationEditor = locationSharedPreferences.edit();



        setHome=(Button)findViewById(R.id.button);
        setHomeImage= (ImageView) findViewById(R.id.imageView3);

        setHomeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent(TimerActivity.this, MapsTimerActivity.class);
                startActivity(resultIntent);
            }
        });

        setHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent(TimerActivity.this, MapsTimerActivity.class);
                startActivity(resultIntent);
            }
        });
    }
}
