package com.usalamatechnology.application.activity;

/**
 * Created by Sir Edwin on 3/2/2016.
 */

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.usalamatechnology.application.R;

import java.util.ArrayList;

public class CallBroadcastReceiver extends BroadcastReceiver {
    public static SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String volumeCounter = "volumeCounter";
    SharedPreferences.Editor editor;

    public static final String USERLOCATIONPREFERENCES = "userlocationPrefs";
    public static SharedPreferences userLocationSharedPreferences;
    SharedPreferences.Editor userLocationEditor;

    //emergency contacts
    public static final String phoneKey = "phoneKey";
    public static final String phoneOneKey = "phoneKey1";
    public static final String phoneTwoKey = "phoneKey2";



    public static final String userLatitude = "userLatitude";
    public static final String userLongitude = "userLongitude";
    public static final String userAccuracy = "userAccuracy";

    public static final String LOCATIONPREFERENCES = "LocationPrefs";
    public static SharedPreferences locationSharedPreferences;
    SharedPreferences.Editor locationEditor;

    final String sendOnVolume = "sendOnVolume";

    private ArrayList<String> allPhoneNumbers = new ArrayList<>();
    String message;
    boolean sent = false;
    boolean stop=false;
    private int mMessageSentCount;
    private String txtBody;
    private int mMessageSentTotalParts;
    private int mMessageSentParts;
    private int count;
    private BroadcastReceiver receiver;
    private BroadcastReceiver receiverTwo;

    String phoneNumber;
    String phoneNumberOne ;
    String phoneNumberTwo;
    private Context context;

    private void setAllPhoneNumbers() {
        allPhoneNumbers.clear();
        if (phoneNumber != null && !"".equals(phoneNumber)) {
            allPhoneNumbers.add(phoneNumber);
        }

        if (phoneNumberOne != null && !"".equals(phoneNumberOne)) {
            allPhoneNumbers.add(phoneNumberOne);
        }

        if (phoneNumberTwo != null && !"".equals(phoneNumberTwo)) {
            allPhoneNumbers.add(phoneNumberTwo);
        }
    }

    private boolean thereAreSmsToSend() {

        return mMessageSentCount < allPhoneNumbers.size();
    }

    private void sendNextMessage() {
        if (thereAreSmsToSend()) {
            if (message != null)
                sendSMS(allPhoneNumbers.get(mMessageSentCount), txtBody);
        } else {
            if(context!=null) {
                // Get instance of Vibrator from current Context
                Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

// Vibrate for 300 milliseconds
                v.vibrate(1000);
                Toast.makeText(context, "All distress have been sent",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendSMS(final String phoneNumber, String message) {
        if(!stop)
        {
            String SENT = "SMS_SENT";
            String DELIVERED = "SMS_DELIVERED";

            SmsManager sms = SmsManager.getDefault();
            ArrayList<String> parts = sms.divideMessage(message);
            mMessageSentTotalParts = parts.size();

            Log.i("Message Count", "Message Count: " + mMessageSentTotalParts);

            ArrayList<PendingIntent> deliveryIntents = new ArrayList<>();
            ArrayList<PendingIntent> sentIntents = new ArrayList<>();

            if(context!=null) {
                PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
                PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);

                for (int j = 0; j < mMessageSentTotalParts; j++) {
                    sentIntents.add(sentPI);
                    deliveryIntents.add(deliveredPI);
                }
            }
            mMessageSentParts = 0;
            if (!phoneNumber.equals("") && count <= allPhoneNumbers.size()) {
                System.out.println("phoneNumber "+phoneNumber);
                sms.sendMultipartTextMessage(phoneNumber, null, parts, sentIntents, deliveryIntents);
                ++count;
            }

        }


    }

    private void registerBroadCastReceivers() {

        String SENT = "SMS_SENT";

        class myReceiverTwo extends BroadcastReceiver
        {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                if (count <= allPhoneNumbers.size()) {
                    switch (getResultCode()) {

                        case Activity.RESULT_OK:

                            mMessageSentParts++;
                            if (mMessageSentParts == mMessageSentTotalParts) {

                                mMessageSentCount++;
                                sendNextMessage();
                            }

                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:

                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:

                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:

                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:

                            break;
                    }
                }
            }
        }
        if(receiverTwo!=null)
            if(context!=null) {
                //context.unregisterReceiver(receiverTwo);
            }
        receiverTwo=null;
        receiverTwo=new myReceiverTwo();
        if(context!=null)
        {
//            context.registerReceiver(receiverTwo, new IntentFilter(SENT));
        }


        String DELIVERED = "SMS_DELIVERED";


        class myReceiver extends BroadcastReceiver
        {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {

                    case Activity.RESULT_OK:

                        break;
                    case Activity.RESULT_CANCELED:

                        break;
                }
            }
        }
        if(receiver!=null)
            if(context!=null) {
                //context.unregisterReceiver(receiver);
                receiver = null;
            }

        receiver=new myReceiver();
        if(context!=null) {
            //Intent intent = context.registerReceiver(receiver, new IntentFilter(DELIVERED));
        }
    }


    private void startSendMessages() {
        registerBroadCastReceivers();
        message="I need your help";
        if (locationSharedPreferences.getString(userLatitude, null) != null) {

            txtBody = "Usalama text: " + message + " Location:http://maps.google.com/?q=" + locationSharedPreferences.getString(userLatitude, null) + "," + locationSharedPreferences.getString(userLongitude, null) + "&z=14 .Accuracy level is " + locationSharedPreferences.getString(userAccuracy, null)+" meters ";

        } else {
            txtBody = "Usalama text: " + message;

        }
        mMessageSentCount = 0;


        if (message != null) {
            sendSMS(allPhoneNumbers.get(mMessageSentCount), txtBody);
        } else
        {
            if(context!=null) {
                Toast.makeText(context, "Distress not sent. Message field is not available", Toast.LENGTH_LONG).show();
            }
        }
    }



    @Override
    public void onReceive(Context context, Intent intent) {
        this.context=context;
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();



        userLocationSharedPreferences = context.getSharedPreferences(USERLOCATIONPREFERENCES, Context.MODE_PRIVATE);
        userLocationEditor = userLocationSharedPreferences.edit();

        locationSharedPreferences = context.getSharedPreferences(LOCATIONPREFERENCES, Context.MODE_PRIVATE);
        locationEditor = locationSharedPreferences.edit();

        int volume = (Integer)intent.getExtras().get("android.media.EXTRA_VOLUME_STREAM_VALUE");
        Log.i("Tag", "Action : "+ intent.getAction() + " / volume : "+volume);
        if(volume==0)
        {
            System.out.println("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII " + sharedpreferences.getString(volumeCounter, null));
            int i= Integer.parseInt(sharedpreferences.getString(volumeCounter, null))+1;
            editor.putString(volumeCounter, String.valueOf(i));
            editor.apply();
            if(Integer.parseInt(sharedpreferences.getString(volumeCounter, null))>7 && sharedpreferences.getString(sendOnVolume, "no").equals("yes"))
            {
                phoneNumber = sharedpreferences.getString(phoneKey, null);
                phoneNumberOne = sharedpreferences.getString(phoneOneKey, null);
                phoneNumberTwo = sharedpreferences.getString(phoneTwoKey, null);
                System.out.println("phoneNumberMy Timer Service " + phoneNumber);
                setAllPhoneNumbers();
                if(phoneNumber!=null)
                {
                    startSendMessages();
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.notification)
                                    .setContentTitle("Usalama has sent a default distress message.")
                                    .setContentText("Default distress sent.");
// Sets an ID for the notification
                    int mNotificationId = 001;
// Gets an instance of the NotificationManager service
                    NotificationManager mNotifyMgr =
                            (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
// Builds the notification and issues it.
                    mNotifyMgr.notify(mNotificationId, mBuilder.build());
                }
                //Intent n = context.getPackageManager().getLaunchIntentForPackage("com.edu.siredwin.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.usalamatechnology.com.edu.siredwin.application");
                //context.startActivity(n);
                System.out.println("Done sending " + phoneNumber);
                System.out.println("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii "+Integer.parseInt(sharedpreferences.getString(volumeCounter, null)));
                System.out.println("sttttttttttttttttttaaaaaaaaaaaaaaaaarts");
                editor.putString(volumeCounter, "0");
                editor.apply();
            }
        }

    }

}
