package com.usalamatechnology.application.adapter;

/**
 * Created by Sir Edwin on 8/30/2015.
 */

public interface NewsIObserver {
    // change signature of method as per your need
    void onAprroveClicked(int pos, String userid);
    void onFlagClicked(int pos, String userid);
}
