package com.usalamatechnology.application.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.usalamatechnology.application.R;
import com.usalamatechnology.application.model.UsalamaGroupMember;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GroupsRVAdapter extends RecyclerView.Adapter<GroupsRVAdapter.PersonViewHolder> {

    public GroupsIObserver mObserver;


    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView personName;
        public static ImageView userImage;

        PersonViewHolder(View itemView) {
            super(itemView);
            personName = (TextView)itemView.findViewById(R.id.person_name);
            userImage = (ImageView)itemView.findViewById(R.id.person_photo);

        }

    }

    public void setListener(GroupsIObserver obs) { mObserver = obs; }

    List<UsalamaGroupMember> post;

    public GroupsRVAdapter(List<UsalamaGroupMember> post){
        this.post = post;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.groups_cardview, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.personName.setText(post.get(i).name);

        Picasso.with(personViewHolder.itemView.getContext()).load(post.get(i).imagePath)
                .error(R.drawable.ic_account)
                .placeholder(R.drawable.ic_account).into(PersonViewHolder.userImage);
    }



    @Override
    public int getItemCount() {
        return post.size();
    }
}