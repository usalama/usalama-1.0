package com.usalamatechnology.application.adapter;

/**
 * Created by Ravi on 29/07/15.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.usalamatechnology.application.R;
import com.usalamatechnology.application.model.NavDrawerItem;

import java.util.Collections;
import java.util.List;

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());
        //System.out.println("######################## "+current.getTitle());
        Drawable myDrawable;

        if(position==0)
        {
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                myDrawable = context.getDrawable(R.drawable.ic_home_drawer);
            } else {
                myDrawable = context.getResources().getDrawable(R.drawable.ic_home_drawer);
            }
            holder.icon.setImageDrawable(myDrawable);
        }

        if(position==1)
        {
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                myDrawable = context.getDrawable(R.drawable.ic_usalama_around);
            } else {
                myDrawable = context.getResources().getDrawable(R.drawable.ic_usalama_around);
            }

            holder.icon.setImageDrawable(myDrawable);
        }


        if(position==2)
        {
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                myDrawable = context.getDrawable(R.drawable.ic_timer);
            } else {
                myDrawable = context.getResources().getDrawable(R.drawable.ic_timer);
            }

            holder.icon.setImageDrawable(myDrawable);
        }

        if(position==3)
        {
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                myDrawable = context.getDrawable(R.drawable.ic_walk);
            } else {
                myDrawable = context.getResources().getDrawable(R.drawable.ic_walk);
            }

            holder.icon.setImageDrawable(myDrawable);
        }

        if(position==4)
        {
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                myDrawable = context.getDrawable(R.drawable.ic_groups);
            } else {
                myDrawable = context.getResources().getDrawable(R.drawable.ic_groups);
            }

            holder.icon.setImageDrawable(myDrawable);
        }



        if(position==5)
        {
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                myDrawable = context.getDrawable(R.drawable.ic_emergency);
            } else {
                myDrawable = context.getResources().getDrawable(R.drawable.ic_emergency);
            }

            holder.icon.setImageDrawable(myDrawable);
        }

        if(position==6)
        {
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                myDrawable = context.getDrawable(R.drawable.ic_contact_us);
            } else {
                myDrawable = context.getResources().getDrawable(R.drawable.ic_contact_us);
            }

            holder.icon.setImageDrawable(myDrawable);
        }

        if(position==7)
        {
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                myDrawable = context.getDrawable(R.drawable.ic_settings);
            } else {
                myDrawable = context.getResources().getDrawable(R.drawable.ic_settings);
            }

            holder.icon.setImageDrawable(myDrawable);
        }


        if(position==8)
        {
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                myDrawable = context.getDrawable(R.drawable.ic_user);
            } else {
                myDrawable = context.getResources().getDrawable(R.drawable.ic_user);
            }

            holder.icon.setImageDrawable(myDrawable);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView icon;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            icon = (ImageView) itemView.findViewById(R.id.navicon);
        }
    }
}
