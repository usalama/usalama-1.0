package com.usalamatechnology.application.adapter;

/**
 * Created by Sir Edwin on 8/30/2015.
 */
public interface IObserver {

    // change signature of method as per your need
    void onItemClicked(int pos);
    void onItemLongClicked(int pos);
    void onTextClicked(int pos);

}
