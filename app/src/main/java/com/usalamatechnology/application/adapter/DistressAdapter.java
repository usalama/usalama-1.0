package com.usalamatechnology.application.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.usalamatechnology.application.R;
import com.usalamatechnology.application.model.DistressCall;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DistressAdapter extends RecyclerView.Adapter<DistressAdapter.DistressViewHolder> {

    public IObserver mObserver;



    public static class DistressViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView personName;
        TextView message;
        TextView date;
        TextView id;
        ImageView personPhoto;


        DistressViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            personName = (TextView)itemView.findViewById(R.id.person_name);
            message = (TextView)itemView.findViewById(R.id.person_message);
            date = (TextView)itemView.findViewById(R.id.date);
            id = (TextView)itemView.findViewById(R.id.distressId);
            personPhoto = (ImageView)itemView.findViewById(R.id.person_photo);

        }

    }

    public void setListener(IObserver obs) { mObserver = obs; }

    List<DistressCall> distressCall;

    public DistressAdapter(List<DistressCall> distressCall){
        this.distressCall = distressCall;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

    }

    @Override
    public DistressViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.distress_cardview, viewGroup, false);
        DistressViewHolder dvh = new DistressViewHolder(v);
        return dvh;
    }

    @Override
    public void onBindViewHolder(DistressViewHolder distressViewHolder, int i) {
        distressViewHolder.personName.setText(distressCall.get(i).name);

        Date Date1 = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());
        Date Date2 = null;
        try {
            Date1 = sdf.parse(currentDateandTime);
            Date2 = sdf.parse(distressCall.get(i).date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long mills = 0;
        if (Date1 != null && Date2 != null) {
            mills = Date1.getTime() - Date2.getTime();
        }
        System.out.println();

        long seconds = mills / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        String time ;
        if(seconds<=5 && minutes==0 && hours==0 && days==0)
        {
            time="just now";
        }
        else if(seconds>5 && minutes==0 && hours==0 && days==0)
        {
            if(seconds==1)
            {
                time= seconds+" sec ago";
            }
            else
            {
                time= seconds+" secs ago";
            }

        }
        else if(minutes>0 && hours==0 && days==0)
        {
            if(minutes==1)
            {
                time= minutes+" min ago";
            }else
            {
                time= minutes+" mins ago";
            }
        }
        else if(hours>0 && days==0)
        {
            if(hours==1)
            {
                time= hours+" hr ago";
            }
            else
            {
                time= hours+" hrs ago";
            }

        }
        else if(days>0 && days<=4)
        {
            if(days==1)
            {
                time= days+" day ago";
            }
            else
            {
                time= days+" days ago";
            }

        }
        else
        {
            time="08 Nov";
            try{
            SimpleDateFormat fd = new SimpleDateFormat("dd MMM");
                Date dateCur = fd.parse(distressCall.get(i).date);
            time=dateCur.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        }


        distressViewHolder.date.setText(time);
        distressViewHolder.id.setText(distressCall.get(i).id);
        distressViewHolder.message.setText(distressCall.get(i).message);
        distressViewHolder.personPhoto.setImageResource(distressCall.get(i).photoId);

        final int id= Integer.parseInt(distressCall.get(i).id);

        distressViewHolder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // It will call method from activity class where you can do startActivityForResult()
                mObserver.onItemClicked(id);
            }
        });

        distressViewHolder.cv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mObserver.onItemLongClicked(id);
                return true;
            }
        });

    }



    @Override
    public int getItemCount() {
        return distressCall.size();
    }
}