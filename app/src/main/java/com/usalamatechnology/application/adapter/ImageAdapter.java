package com.usalamatechnology.application.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.usalamatechnology.application.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sir Edwin on 8/4/2014.
 */
public class ImageAdapter extends BaseAdapter {
    private final List<Item> items = new ArrayList<>();
    private final LayoutInflater inflater;

    public ImageAdapter(Context context) {
        inflater = LayoutInflater.from(context);

        items.add(new Item("Armed robber", R.drawable.armed_robber));
        items.add(new Item("Burglar", R.drawable.burglar));
        items.add(new Item("Mugger", R.drawable.mugger));
        items.add(new Item("Carjacker", R.drawable.carjacking));
        items.add(new Item("Emergency", R.drawable.emergency));
        items.add(new Item("Gender", R.drawable.gender));
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return items.get(i).drawableId;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView picture;
        TextView name;

        if (v == null) {
            v = inflater.inflate(R.layout.gridview_item, viewGroup, false);
            v.setTag(R.id.picture, v.findViewById(R.id.picture));
            v.setTag(R.id.text, v.findViewById(R.id.text));
        }

        picture = (ImageView) v.getTag(R.id.picture);
        name = (TextView) v.getTag(R.id.text);

        Item item = (Item) getItem(i);

        picture.setImageResource(item.drawableId);
        name.setText(item.name);

        return v;
    }

    private class Item {
        final String name;
        final int drawableId;

        Item(String name, int drawableId) {
            this.name = name;
            this.drawableId = drawableId;
        }
    }
}
