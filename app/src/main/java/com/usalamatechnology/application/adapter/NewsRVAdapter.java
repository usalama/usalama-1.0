package com.usalamatechnology.application.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.usalamatechnology.application.R;
import com.usalamatechnology.application.model.UsalamaPost;

import java.util.List;

public class NewsRVAdapter extends RecyclerView.Adapter<NewsRVAdapter.PersonViewHolder> {

    public NewsIObserver mObserver;

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView personName;
        TextView postdatetime;
        TextView postApprove;
        TextView postFlag;
        ImageView imgpostApprove;
        ImageView imgpostFlag;
        TextView postContent;
        LinearLayout linearLayout;

        TextView postId;


        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.newscv);
            personName = (TextView)itemView.findViewById(R.id.postname);
            postdatetime = (TextView)itemView.findViewById(R.id.datetime);
            postApprove = (TextView)itemView.findViewById(R.id.postapproval);
            postContent = (TextView)itemView.findViewById(R.id.postcontent);
            postId = (TextView)itemView.findViewById(R.id.postid);
            linearLayout=(LinearLayout)itemView.findViewById(R.id.layoutId);

            postFlag = (TextView)itemView.findViewById(R.id.txtpostflag);
            imgpostApprove = (ImageView)itemView.findViewById(R.id.imgpostapprove);
            imgpostFlag = (ImageView)itemView.findViewById(R.id.imgpostflag);
        }

    }

    public void setListener(NewsIObserver obs) { mObserver = obs; }

    List<UsalamaPost> post;

    public NewsRVAdapter(List<UsalamaPost> post){
        this.post = post;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_cardview, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.personName.setText(post.get(i).name);
        personViewHolder.postApprove.setText(post.get(i).approvals);
        personViewHolder.postContent.setText(post.get(i).content);
        personViewHolder.postdatetime.setText(post.get(i).datetime);

        //personViewHolder.linearLayout.setGravity(Gravity.CENTER | Gravity.RIGHT);


        final int pos=i;
        final int id= Integer.parseInt(post.get(i).postid);
        final String posterid= post.get(i).userid;

        personViewHolder.postApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mObserver.onAprroveClicked(id,posterid);
            }
        });

        personViewHolder.imgpostApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mObserver.onAprroveClicked(id,posterid);
            }
        });

        personViewHolder.postFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mObserver.onFlagClicked(id,posterid);
            }
        });

        personViewHolder.imgpostFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mObserver.onFlagClicked(id,posterid);
            }
        });

    }



    @Override
    public int getItemCount() {
        return post.size();
    }
}