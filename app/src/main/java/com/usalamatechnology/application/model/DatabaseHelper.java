package com.usalamatechnology.application.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Inganji on 5/25/14.
 */
public class DatabaseHelper {

    private static final String COLUMN_USERTYPE = "usertype";
    private static final String COLUMN_CONTENT = "content";
    private static final String COLUMN_POSTID = "postid";
    private static final String COLUMN_APPROVAL = "approvals";
    private static final String COLUMN_USERID = "userid";
    private static final String COLUMN_AREA = "area";
    private static final String COLUMN_POSTDATE = "postdate";
    private static final String COLUMN_POSTTIME = "posttime";
    private static final String COLUMN_STATUS = "status";

    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_MESSAGE = "message";
    private static final String COLUMN_DATE = "date";
    private static final String COLUMN_TIME = "time";
    private static final String COLUMN_ACCURACY = "accuracy";
    private static final String COLUMN_VNAME = "vname";
    private static final String COLUMN_PNUMBER = "pnumber";
    private static final String COLUMN_VLONG = "vlong";
    private static final String COLUMN_VLAT = "vlat";
    private static final String COLUMN_PONELONG = "ponelong";
    private static final String COLUMN_PONELAT = "ponelat";
    private static final String COLUMN_PTWOLONG = "ptwolong";
    private static final String COLUMN_PTWOLAT = "ptwolat";
    private static final String COLUMN_PTHREELONG = "pthreelong";
    private static final String COLUMN_PTHREELAT = "pthreelat";

    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_IMAGEPATH = "imagepath";

    private static final int DATABASE_VERSION = 11;
    private static final String DATABASE_NAME = "usalama.db";

    private static final String TABLE_NAME_PASSWORD = "password";
    private static final String TABLE_NAME_DISTRESS = "distress";
    private static final String TABLE_NAME_POSTS = "usalama_posts";
    private static final String TABLE_NAME_GROUPS = "usalama_groups";
    private static final String TABLE_NAME_APPROVALS = "approvals";

    private final SQLiteDatabase database;

    public DatabaseHelper(Context context) {
        DatabaseOpenHelper openHelper = new DatabaseOpenHelper(context);
        database = openHelper.getWritableDatabase();

    }

    public void savePassword(String password) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_PASSWORD, password);
        database.insert(TABLE_NAME_PASSWORD, null, contentValues);
    }

    public void savePost(String postid, String userid, String usertype, String content, String area, String postdate,
                             String posttime, String status) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_POSTID, postid);
        contentValues.put(COLUMN_USERID, userid);
        contentValues.put(COLUMN_USERTYPE, usertype);
        contentValues.put(COLUMN_CONTENT, content);
        contentValues.put(COLUMN_AREA, area);
        contentValues.put(COLUMN_POSTDATE, postdate);
        contentValues.put(COLUMN_POSTTIME, posttime);
        contentValues.put(COLUMN_STATUS, status);


        database.insert(TABLE_NAME_POSTS, null, contentValues);
    }


    public void saveApprovals(String postid, String approvals) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_POSTID, postid);
        contentValues.put(COLUMN_APPROVAL, approvals);

        database.insert(TABLE_NAME_APPROVALS, null, contentValues);
    }

    public void saveGroups(String username, String imagePath) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_USERNAME, username);
        contentValues.put(COLUMN_IMAGEPATH, imagePath);
        database.insert(TABLE_NAME_GROUPS, null, contentValues);
    }

    public void saveDistress(String message, String date, String time, String vname, String accuracy, String pnumber,
                             String vlong, String vlat, String ponelong,
                             String ponelat, String ptwolong, String ptwolat,
                             String pthreelong, String pthreelat) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_MESSAGE, message);
        contentValues.put(COLUMN_DATE, date);
        contentValues.put(COLUMN_TIME, time);
        contentValues.put(COLUMN_VNAME, vname);
        contentValues.put(COLUMN_ACCURACY, accuracy);
        contentValues.put(COLUMN_PNUMBER, pnumber);
        contentValues.put(COLUMN_VLONG, vlong);
        contentValues.put(COLUMN_VLAT, vlat);
        contentValues.put(COLUMN_PONELONG, ponelong);
        contentValues.put(COLUMN_PONELAT, ponelat);
        contentValues.put(COLUMN_PTWOLONG, ptwolong);
        contentValues.put(COLUMN_PTWOLAT, ptwolat);
        contentValues.put(COLUMN_PTHREELONG, pthreelong);
        contentValues.put(COLUMN_PTHREELAT, pthreelat);

        database.insert(TABLE_NAME_DISTRESS, null, contentValues);
    }


    public void deleteRecord(String id) {
        String sql = "DELETE FROM " + TABLE_NAME_DISTRESS + " WHERE `_id` =" + id;
        database.execSQL(sql);
    }

    public void clearAllRecords() {
        try {
            database.delete(TABLE_NAME_DISTRESS, null, null);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void clearAllMembers() {
        try {
            database.delete(TABLE_NAME_GROUPS, null, null);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public Cursor getAllRecords() {

        return database.rawQuery(
                "select * from " + TABLE_NAME_DISTRESS + " ORDER BY `_id` DESC",
                null
        );
    }

    public Cursor getAllApprovals() {

        return database.rawQuery(
                "select * from " + TABLE_NAME_APPROVALS + " ORDER BY `_id` DESC",
                null
        );
    }

    public Cursor getRecord(String id) {

        return database.rawQuery(
                "select * from " + TABLE_NAME_DISTRESS + " WHERE _id = "+id+" ORDER BY `_id` DESC",
                null
        );
    }

    public Cursor changeStatus(String id) {

        return database.rawQuery(
                "UPDATE " + TABLE_NAME_POSTS + " SET status = 0 WHERE _id = "+id,
                null
        );
    }

    public Cursor getAllPosts() {

        return database.rawQuery(
                "select * from " + TABLE_NAME_POSTS +" ORDER BY `_id` DESC",
                null
        );
    }

    public Cursor getAllGroupMembers() {

        return database.rawQuery(
                "select * from " + TABLE_NAME_GROUPS + " ORDER BY `_id` DESC",
                null
        );
    }

    public boolean isPasswordSet() {
        Cursor cursor = database.rawQuery(
                "select `password` from " + TABLE_NAME_PASSWORD + " WHERE `_id` = 1",
                null
        );
        return cursor.getCount() != 0;
    }

    public boolean isDbEmpty() {
        Cursor cursor = database.rawQuery(
                "select * from " + TABLE_NAME_DISTRESS,
                null
        );
        return cursor.getCount() == 0;
    }


    public boolean isPostDbEmpty() {
        Cursor cursor = database.rawQuery(
                "select * from " + TABLE_NAME_POSTS,
                null
        );
        return cursor.getCount() == 0;
    }

    public boolean isGroupDbEmpty() {
        Cursor cursor = database.rawQuery(
                "select * from " + TABLE_NAME_GROUPS,
                null
        );
        return cursor.getCount() == 0;
    }


    public boolean authenticateUser(String userPassword) {
       // Cursor cursor = database.rawQuery(
    //            "select `password` from " + TABLE_NAME_PASSWORD+" WHERE _id = 1",
    //            null
   //     );
   //    ;
       // System.out.println("******************************** "+cursor.getCount()+cursor.getColumnCount()+cursor.getString(0));
     //   String password = cursor.getString(cursor.getColumnIndex("password"));
        return true;
    }

    private class DatabaseOpenHelper extends SQLiteOpenHelper {
        DatabaseOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {


            db.execSQL(
                    "CREATE TABLE " + TABLE_NAME_DISTRESS +
                            "( _id INTEGER PRIMARY KEY , "
                            + COLUMN_MESSAGE + " TEXT , "
                            + COLUMN_DATE + " TEXT , "
                            + COLUMN_TIME + " TEXT , "
                            + COLUMN_VNAME + " TEXT , "
                            + COLUMN_ACCURACY + " TEXT , "
                            + COLUMN_PNUMBER + " TEXT , "
                            + COLUMN_VLONG + " TEXT , "
                            + COLUMN_VLAT + " TEXT , "
                            + COLUMN_PONELONG + " TEXT , "
                            + COLUMN_PONELAT + " TEXT , "
                            + COLUMN_PTWOLONG + " TEXT , "
                            + COLUMN_PTWOLAT + " TEXT , "
                            + COLUMN_PTHREELONG + " TEXT , "
                            + COLUMN_PTHREELAT + " TEXT )"
            );

            db.execSQL(
                    "CREATE TABLE " + TABLE_NAME_POSTS +
                            "( _id INTEGER PRIMARY KEY , "
                            + COLUMN_POSTID + " TEXT , "
                            + COLUMN_USERID + " TEXT , "
                            + COLUMN_USERTYPE + " TEXT , "
                            + COLUMN_CONTENT + " TEXT , "
                            + COLUMN_AREA + " TEXT , "
                            + COLUMN_POSTDATE + " TEXT , "
                            + COLUMN_POSTTIME + " TEXT , "
                            + COLUMN_STATUS  + " TEXT )"
            );

            db.execSQL(
                    "CREATE TABLE " + TABLE_NAME_APPROVALS +
                            "( _id INTEGER PRIMARY KEY , "
                            + COLUMN_POSTID + " TEXT , "
                            + COLUMN_APPROVAL  + " TEXT )"
            );

            db.execSQL(
                    "CREATE TABLE " + TABLE_NAME_GROUPS +
                            "( _id INTEGER PRIMARY KEY , "
                            + COLUMN_USERNAME + " TEXT , "
                            + COLUMN_IMAGEPATH  + " TEXT )"
            );

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_PASSWORD);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_DISTRESS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_POSTS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_APPROVALS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_GROUPS);
            onCreate(db);
        }

    }

}
