package com.usalamatechnology.application.model;

/**
 * Created by Ravi on 29/07/15.
 */
public class NavDrawerItemM {
    private boolean showNotify;
    private String title;


    public NavDrawerItemM() {

    }

    public NavDrawerItemM(boolean showNotify, String title) {
        this.showNotify = showNotify;
        this.title = title;
    }

    public boolean isShowNotify() {
        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
