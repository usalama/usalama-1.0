package com.usalamatechnology.application.model;

/**
 * Created by Sir Edwin on 8/29/2015.
 */
public class Person {
    public String name;
    public String age;
    public int photoId;

    public Person(String name, String age, int photoId) {
        this.name = name;
        this.age = age;
        this.photoId = photoId;
    }

}