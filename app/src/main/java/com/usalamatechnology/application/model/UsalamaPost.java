package com.usalamatechnology.application.model;

/**
 * Created by Sir Edwin on 8/29/2015.
 */
public class UsalamaPost {
    public String name;
    public String postid;
    public String userid;
    public String datetime;
    public String approvals;
    public String content;
    public String area;


    public UsalamaPost(String postid, String userid,String name, String content, String area, String datetime,String approvals
                       ) {
        this.name = name;
        this.postid = postid;
        this.userid = userid;
        this.content = content;
        this.datetime = datetime;
        this.approvals = approvals;
        this.area = area;

    }

}