package com.usalamatechnology.application.model;

/**
 * Created by Sir Edwin on 9/23/2015.
 */
public class DistressCall {
    public String id;
    public String name;
    public String message;
    public String date;
    public int photoId;


    public DistressCall(String id, String name, String message,String date,int photoId) {
        this.id=id;
        this.name = name;
        this.message = message;
        this.date = date;
        this.photoId = photoId;

    }
}
