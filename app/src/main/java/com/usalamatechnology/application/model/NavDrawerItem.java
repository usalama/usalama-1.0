package com.usalamatechnology.application.model;

/**
 * Created by Ravi on 29/07/15.
 */
public class NavDrawerItem {
    private boolean showNotify;
    private String title;
    public int photoId;

    public NavDrawerItem() {

    }

    public NavDrawerItem( String title,int photoId) {
        this.title = title;
        this.photoId = photoId;
    }

    public boolean isShowNotify() {
        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return photoId;
    }

    public void setIcon(int photoId) {
        this.photoId = photoId;
    }
}
